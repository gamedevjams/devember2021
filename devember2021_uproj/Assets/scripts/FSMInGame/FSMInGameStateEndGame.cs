using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Mathematics;
using static Unity.Mathematics.math;
using Sirenix.OdinInspector;
using DG.Tweening;
using tg;
using static tg.tg;

namespace devember2021
{
	public class FSMInGameStateEndGame : MonoBehaviour, FSM.IStateMB
	{
		public class State : FSM.State
		{
			public override string name => "EndGame";
			public Data data;

			public override void WillEnter( FSM<string>.State From, Action done )
			{
				data.canvasGroup = data.gameState.endGameUI?.canvasGroup;

				#if UNITY_EDITOR
				if ( data.canvasGroup == null )
				{
					log_err( $"data.canvasGroup null. GameState.endGameUI has to be assigned before calling GoTo( \"{data.state.name}\" )", data.fsm_state_go );
				}
				#endif

				data.canvasGroup.interactable   = false;
				data.canvasGroup.blocksRaycasts = false;
				data.canvasGroup.alpha          = 0;

				data.canvasGroup.DOFade( endValue: 1, FSMApp.fadesDuration ).OnComplete( ()=>done?.Invoke() );
			}
			public override void WillExit( FSM<string>.State From, Action done )
			{
				data.canvasGroup.interactable   = false;
				data.canvasGroup.blocksRaycasts = false;
				data.canvasGroup.DOFade( endValue: 0, FSMApp.fadesDuration ).OnComplete( ()=>done?.Invoke() );

				data.gameState.endGameUI?.enable( false );
			}
			public override void Enter( FSM<string>.State from, Action done )
			{
				data.canvasGroup.interactable   = true;
				data.canvasGroup.blocksRaycasts = true;

				data.gameState.endGameUI?.enable( true );

				done?.Invoke();
			}
			public override void Exit( FSM<string>.State from, Action done )
			{
				done?.Invoke();
			}
		}

		public State state = new State();

		#if UNITY_EDITOR
		[ReadOnly,ShowInInspector]
		private string stateName => state.name;
		#endif

		[Serializable]
		public class Data
		{
			public CanvasGroup   canvasGroup;
			public Game_State    gameState;

			[NonSerialized] public State      state;
			[NonSerialized] public GameObject fsm_state_go;
		}
		public Data data;

		//[ShowInInspector,ValueDropdown("editor_states_names_list")] public string pauseState;

		#region Unity
		private void Awake()
		{
			state.data = data;

			data.gameState.onEndGame += handle_end_game;

			data.state        = state;
			data.fsm_state_go = gameObject;
		}
		#endregion Unity

		FSM.State FSM.IStateMB.state => state;

		void
		handle_end_game()
		{
			state.M.GoTo( state );
		}

		#if UNITY_EDITOR
		private List<string> editor_states_names_list() => GetComponentInParent<FSMInGame>().editor_states_names_list();
		#endif // UNITY_EDITOR
	}
}
