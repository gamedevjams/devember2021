using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Mathematics;
using static Unity.Mathematics.math;
using Sirenix.OdinInspector;
using DG.Tweening;
using tg;
using static tg.tg;

namespace devember2021
{
	[DisallowMultipleComponent]
	public class Destructible : MonoBehaviour
	{
		public GameObject intact;
		public GameObject destructed;
		public Damageable damageable;
		public float      intactAppearanceDelay     = 0;
		public float      destructedAppearanceDelay = 0;

		public bool is_destructed => _is_destructed;
		[SerializeField, LabelText("Is Destructed")]
		private bool _is_destructed;

		public Action on_destructed;

		public
		void
		destruct()
		{
			log( $"{name} destruct() is_destructed: {_is_destructed} isActiveAndEnabled: {isActiveAndEnabled}" );
			if ( ! _is_destructed )
			{
				_is_destructed = true;
				apply();
				on_destructed?.Invoke();
			}
		}

		public
		void
		make_intact()
		{
			_is_destructed = false;
			apply();
		}

		public
		void
		apply()
		{
			if ( intact != null )
			{
				bool active = ! is_destructed;
				stop_coroutine( intact_coroutine );
				if ( intactAppearanceDelay == 0 || ! active )
				{
					tg.tg.log( $"setting {intact.name} {active}", this );
					intact.SetActive( active );
				} else
				{
					intact_coroutine = StartCoroutine( set_active( is_destructed, active, intactAppearanceDelay ) );
				}
			}

			if ( destructed != null )
			{
				bool active = is_destructed;
				stop_coroutine( destructed_coroutine );
				if ( destructedAppearanceDelay == 0 || ! active )
				{
					tg.tg.log( $"setting {destructed.name} {active}", this );
					destructed.SetActive( active );
				} else
				{
					destructed_coroutine = StartCoroutine( set_active( is_destructed, active, destructedAppearanceDelay ) );
				}
			}
		}

		#region Unity
		private
		void
		Awake()
		{
			apply();

			if ( damageable != null )
			{
				damageable.onDeath += (d,dd) => destruct();
			}
		}
		#endregion Unity

		#region private
		private Coroutine intact_coroutine;
		private Coroutine destructed_coroutine;

		private
		IEnumerator
		set_active( bool destructed, bool active, float delay )
		{
			yield return new WaitForSeconds( delay );
			( destructed ? this.destructed : intact ).SetActive( active );

			if ( destructed )
			{
				destructed_coroutine = null;
			} else
			{
				intact_coroutine = null;
			}
		}
		private
		void
		stop_coroutine( Coroutine coroutine )
		{
			if ( coroutine != null )
			{
				StopCoroutine( coroutine );
			}
		}
		#endregion private
	}
}
