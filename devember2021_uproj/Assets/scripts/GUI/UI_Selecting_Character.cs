using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Unity.Mathematics;
using static Unity.Mathematics.math;
using Sirenix.OdinInspector;
using DG.Tweening;
using tg;
using static tg.tg;

namespace devember2021
{
	public class UI_Selecting_Character : MonoBehaviour
	{
		public TMP_Text characterName;
		public TMP_Text teamName;
		public RawImage characterRawImage;
		public Image    leftChangeCharacter;
		public Image    rightChangeCharacter;
		public Image    leftChangeTeam;
		public Image    rightChangeTeam;
		public CanvasGroup activatedGUICanvasGroup;
		public CanvasGroup deactivatedGUICanvasGroup;

		[Space]
		public Game_Input.Gamepad_Button leftChangeCharacterGamepadButton;
		public Game_Input.Gamepad_Button rightChangeCharacterGamepadButton;
		public Game_Input.Gamepad_Button leftChangeTeamGamepadButton;
		public Game_Input.Gamepad_Button rightChangeTeamGamepadButton;

		[PropertySpace,ShowInInspector]
		public int gamepadDeviceID { get => _gamepadDeviceID; set { if ( _gamepadDeviceID != value ) { _gamepadDeviceID = value; update_gamepad(); } } }
		private int _gamepadDeviceID = -1;
		public Game_State.Team     team;
		public Game_State.Team[]   availableTeams;
		public Game_Character_SO   character => instanced_characters[selected_character_index];
		public Game_Character_SO[] characters;

		public void update_gamepad()
		{
			Game_Input.instance.gamepads.TryGetValue( _gamepadDeviceID, out gamepad );
		}

		public bool isActivated => _gamepadDeviceID >= 0;

		public
		void
		deactivate()
		{
			gamepadDeviceID = -1;

			activatedGUICanvasGroup.alpha            = 0;
			activatedGUICanvasGroup.interactable     = false;
			activatedGUICanvasGroup.blocksRaycasts   = false;

			deactivatedGUICanvasGroup.alpha          = 1;
			deactivatedGUICanvasGroup.interactable   = true;
			deactivatedGUICanvasGroup.blocksRaycasts = true;

			reset_characters();
		}
		public
		void
		activate( int gamepadID )
		{
			gamepadDeviceID = gamepadID;

			activatedGUICanvasGroup.alpha            = 1;
			activatedGUICanvasGroup.interactable     = true;
			activatedGUICanvasGroup.blocksRaycasts   = true;

			deactivatedGUICanvasGroup.alpha          = 0;
			deactivatedGUICanvasGroup.interactable   = false;
			deactivatedGUICanvasGroup.blocksRaycasts = false;

			new_character( character );
			new_team( team = Game_State.Team.A );
		}

		#region Unity
		private void Awake()
		{
			instanced_characters = new Game_Character_SO[characters.Length];
			reset_characters();
		}
		private
		void
		Update()
		{
			if ( gamepad != null )
			{
				bool char_left  = 1 == gamepad.buttons_just_activated[(int)leftChangeCharacterGamepadButton];
				bool char_right = 1 == gamepad.buttons_just_activated[(int)rightChangeCharacterGamepadButton];
				bool team_left  = 1 == gamepad.buttons_just_activated[(int)leftChangeTeamGamepadButton];
				bool team_right = 1 == gamepad.buttons_just_activated[(int)rightChangeTeamGamepadButton];

				if ( char_left != char_right )
				{
					int char_change = char_left ? -1 : 1;
					int ccount = characters.Length;
					selected_character_index = ( selected_character_index + char_change + ccount ) % ccount;
					new_character( character );
				}

				if ( team_left != team_right )
				{
					int team_change = team_left ? -1 : 1;
					int teams_count = availableTeams.Length;
					team = availableTeams[ ( (int)team + team_change + teams_count ) % teams_count ];
					new_team( team );
				}
			}
		}
		#endregion Unity

		#region private
		private Game_Input.Gamepad_Status gamepad;
		//private readonly int teams_count = Enum.GetValues(typeof(Game_State.Team)).Length;
		private int selected_character_index = 0;
		private Game_Character_SO[] instanced_characters;

		private
		void
		new_character( Game_Character_SO character )
		{
			characterName    .text    = character.name;
			characterRawImage.texture = character.thumbnail;
		}
		private
		void
		new_team( Game_State.Team team )
		{
			teamName.text = team.ToString();
		}
		private
		void
		reset_characters()
		{
			for ( int i = 0; i != instanced_characters.Length; ++i )
			{
				instanced_characters[i] = Instantiate( characters[i] );
			}
		}
		#endregion private
	}
}
