using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Mathematics;
using static Unity.Mathematics.math;
using Sirenix.OdinInspector;
using tg;
using static tg.tg;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.InputSystem;

namespace devember2021
{
	public class GamepadButtons : MonoBehaviour
	{
		public bool autoGatherButtons;
		public List<GamepadButton> buttons = new List<GamepadButton>();
		[Serializable]
		public class SelectionChanged : UnityEvent<GamepadButton,GamepadButton> { }

		public SelectionChanged onSelectionChanged;
		[Range(0,1)]
		public float changeDurationSecs;

		public
		void
		reset_selection()
		{
			selected_index = 0;
			if ( buttons.Count > 0 )
			{
				buttons[0].Select( request_animation: false );
			}
			for ( int i = 1; i < buttons.Count; ++i )
			{
				buttons[i].Deselect( request_animation: false );
			}
		}

		#region unity
		private
		void
		Start()
		{
			timer = new Timer( changeDurationSecs );

			if ( autoGatherButtons )
			{
				gather_buttons();
			}

			reset_selection();
		}
		private
		void
		Update()
		{
			if ( timer.active && timer.passed_percentage() < 1 )
			{
				return;
			}
			timer.active = false;

			int old_selected_index = selected_index;
			bool up = false, down = false, A = false;

			up   = up   || Keyboard.current.upArrowKey  .isPressed;
			down = down || Keyboard.current.downArrowKey.isPressed;
			A    = A    || Keyboard.current.enterKey    .isPressed;

			#if false
			up   = up   || Gamepad.current.dpad.up  .isPressed;
			down = down || Gamepad.current.dpad.down.isPressed;
			A    = A    || Gamepad.current.aButton  .isPressed;
			#endif

			foreach ( var g in Game_Input.instance.gamepads.Values )
			{
				up   = up   || g.buttons_active[(int)Game_Input.Gamepad_Button.L_STICK_UP]   == 1;
				down = down || g.buttons_active[(int)Game_Input.Gamepad_Button.L_STICK_DOWN] == 1;
				up   = up   || g.buttons_active[(int)Game_Input.Gamepad_Button.DPAD_UP]      == 1;
				down = down || g.buttons_active[(int)Game_Input.Gamepad_Button.DPAD_DOWN]    == 1;
				A    = A    || g.buttons_active[(int)Game_Input.Gamepad_Button.A]            == 1;
			}

			if ( down && ! up)
			{
				selected_index = selected_index == buttons.Count - 1 ? 0 : selected_index + 1;
				//log( "down" );
			}

			if ( up && ! down )
			{
				selected_index = selected_index == 0 ? buttons.Count - 1 : selected_index - 1;
				//log( "up" );
			}

			if ( up != down )
			{
				//log( $"change {old_selected_index} => {selected_index}" );
				buttons[old_selected_index].Deselect();
				buttons[    selected_index].Select();
				timer.restart( changeDurationSecs );
			}

			if ( A )
			{
				buttons[selected_index].Activate();
				timer.restart( changeDurationSecs );
			}
		}
		#endregion unity

		#region private
		[ShowInInspector,DisableInEditorMode]
		private bool timer_active() => timer.active;
		[ShowInInspector,DisableInEditorMode]
		private float timer_passed_percentage() => timer.passed_percentage();
		[ShowInInspector,ReadOnly]
		private int   selected_index = 0;
		private Timer timer;

		[Button("Gather Buttons")]
		private
		void
		gather_buttons()
		{
			buttons.Add( GetComponentsInChildren<GamepadButton>( includeInactive: false ) );
		}
		[Button("Clear Buttons")]
		private
		void
		clear_buttons()
		{
			buttons.Clear();
		}
		#endregion private
	}
}
