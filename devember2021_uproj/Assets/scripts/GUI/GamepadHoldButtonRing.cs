using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Unity.Mathematics;
using static Unity.Mathematics.math;
using Sirenix.OdinInspector;
using DG.Tweening;
using tg;
using static tg.tg;

namespace devember2021
{
	public class GamepadHoldButtonRing : MonoBehaviour
	{
		public Image             ring;
		public GamepadHoldButton gamepadHoldButton;

		#region Unity
		private
		void
		Awake()
		{
			ring_material = new Material( ring.material );
			ring.material = ring_material;
			percentage_shader_name_id = Shader.PropertyToID( "_Percentage" );
		}
		public
		void
		Update()
		{
			ring_material.SetFloat( percentage_shader_name_id, gamepadHoldButton.progress );
		}
		#endregion Unity

		#region private
		private Material ring_material;
		private int percentage_shader_name_id;
		[ShowInInspector, HideInEditorMode]
		private float pc => Application.isPlaying ? gamepadHoldButton.progress : -1;
		#endregion private
	}
}
