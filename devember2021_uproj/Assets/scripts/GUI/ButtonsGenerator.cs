using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Mathematics;
using static Unity.Mathematics.math;
using Sirenix.OdinInspector;
using DG.Tweening;
using tg;
using static tg.tg;
using UnityEngine.Events;
using TMPro;

namespace devember2021
{
	public class ButtonsGenerator : MonoBehaviour
	{
		public GameObject buttonPrefab;

		public
		void
		instantiate_button( string text, UnityAction<bool> on_activate )
		{
			check_init();

			var go = Instantiate( buttonPrefab, tr );
			var gb = go.GetComponent<GamepadButton>();
			gb.onActivated.AddListener( on_activate );
			gb.GetComponentInChildren<TMP_Text>().text = text;
		}

		public
		void
		clear_buttons()
		{
			check_init();
			tr.destroy_all_children();
		}

		#region Unity
		#endregion Unity

		#region private
		private Transform tr;
		private bool initialized;

		private
		void
		check_init()
		{
			if ( ! initialized )
			{
				tr = transform;
				initialized = true;
			}
		}
		#endregion private
	}
}
