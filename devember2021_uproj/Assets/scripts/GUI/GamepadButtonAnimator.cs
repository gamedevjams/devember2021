using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Unity.Mathematics;
using static Unity.Mathematics.math;
using Sirenix.OdinInspector;
using DG.Tweening;
using tg;
using static tg.tg;

namespace devember2021
{
	public class GamepadButtonAnimator : MonoBehaviour
	{
		public GamepadButton gamepadButton;
		public Image         bgImage;
		public Game_Button_Animator_Settings settings;

		#region Unity
		private
		void
		Awake()
		{
			gamepadButton.onSelected  .AddListener( handle_selected   );
			gamepadButton.onDeselected.AddListener( handle_deselected );
			gamepadButton.onActivated .AddListener( handle_activated   );

			button_tr = gamepadButton.transform;
		}
		#endregion Unity

		#region private
		private Transform button_tr;

		private Tween current_tween;

		private
		void
		animate( Game_Button_Animator_Settings.StateConfig config )
		{
			if ( current_tween != null && current_tween.active )//&& current_tween.IsPlaying() )
			{
				//current_tween.Complete();
				current_tween.Kill( complete: false );
			}

			Sequence scale_seq = DOTween.Sequence();
			scale_seq.Append( button_tr.DOScale( new Vector3( config.overshootScale.x, config.overshootScale.y, 1 ), config.overshootDuration      ).SetEase( config.overshootScaleEase      ) );
			scale_seq.Append( button_tr.DOScale( new Vector3( config.scale.x,          config.scale.y,          1 ), config.afterOvershootDuration ).SetEase( config.afterOvershootScaleEase ) );

			Sequence color_seq = DOTween.Sequence();
			color_seq.Append( bgImage.DOColor( config.overshootColor, config.overshootDuration      ).SetEase( config.overshootColorEase      ) );
			color_seq.Append( bgImage.DOColor( config.color,          config.afterOvershootDuration ).SetEase( config.afterOvershootColorEase ) );

			Sequence seq = DOTween.Sequence();
			seq.Join( scale_seq );
			seq.Join( color_seq );
			seq.SetAutoKill( false );
			seq.Play();

			current_tween = seq;
		}

		private
		void
		handle_selected( bool request_animation = true )
		{
			//log( $"selected {name}" );
			animate( settings.selected );

			if ( ! request_animation )
			{
				current_tween.Complete();
			}
		}
		private
		void
		handle_deselected( bool request_animation = true )
		{
			//log( $"deselected {name}" );
			animate( settings.deselected );

			if ( ! request_animation )
			{
				current_tween.Complete();
			}
		}
		private
		void
		handle_activated( bool request_animation = true )
		{
			//log( $"activated {name}" );
			animate( settings.activated );

			if ( ! request_animation )
			{
				current_tween.Complete();
			}
		}
		#endregion private
	}
}
