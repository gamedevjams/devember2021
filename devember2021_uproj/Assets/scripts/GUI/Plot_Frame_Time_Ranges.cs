﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using static tg.tg;

namespace tg
{
	public class Plot_Frame_Time_Ranges : MonoBehaviour
	{
		public Color[] fg_colors     = new Color[Plot_CRT_Ranges.MAX_RANGES_COUNT];
		public float[] ranges_max_ms = new float[Plot_CRT_Ranges.MAX_RANGES_COUNT];

		public Plot_CRT_Ranges plot;
		[Range(0,10000)]
		public float min_ms;
		[Range(0,10000)]
		public float max_ms;
		[Range(0,1024)]
		public float pow_base = 1024f;

		#region private
		private float max_exp = 1.0f;
		private float pow_span;// = Mathf.Pow( pow_base, max_exp ) - 1.0f;
		private
		float
		lin01_to_log01( float v )
		{
			float ret = v * pow_span + 1;
			ret = Mathf.Log( ret, pow_base ) / max_exp;
			return ret;
		}
		private
		float
		ms_to_lin01( float ms )
		{
			float ret = ( Mathf.Clamp( ms, min_ms, max_ms ) - min_ms ) / ( max_ms - min_ms );
			return ret;
		}
		private
		float
		ms_to_log01( float ms )
		{
			float ret = lin01_to_log01( ms_to_lin01( ms ) );
			return ret;
		}
		#endregion

		#region Unity
		private void Awake()
		{
			pow_span = Mathf.Pow( pow_base, max_exp ) - 1.0f;
			float last_value = -1;
			for ( int i = 0; i != Plot_CRT_Ranges.MAX_RANGES_COUNT; ++i )
			{
				float ms = ranges_max_ms[i];
				float val01 = 1;
				if ( ms <= last_value )
				{
					plot.ranges_max[i-1] = 1;
					break;
				} else
				if ( i == Plot_CRT_Ranges.MAX_RANGES_COUNT - 1 )
				{
					val01 = 1;
				} else
				{
					val01 = ms_to_log01( ms );
				}
				plot.ranges_max[i] = val01;
				plot.fg_colors[i]  = fg_colors[i];
				last_value = ms;
			}
		}
		private void Start()
		{
		}
		private void Update()
		{
			float val01 = ms_to_log01( Time.deltaTime * 1000f );
			plot.plot01( val01 );
		}
		#endregion
	}
}
