using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Mathematics;
using static Unity.Mathematics.math;
using Sirenix.OdinInspector;
using DG.Tweening;
using tg;
using static tg.tg;

namespace devember2021
{
	[CreateAssetMenu(fileName = "Game_Button_Animator_Settings", menuName = "ScriptableObjects/tg/Game Button Animator Settings", order = 1)]
	public class Game_Button_Animator_Settings : ScriptableObject
	{
		[Serializable]
		public class StateConfig
		{
			public Color   color;
			public Color   overshootColor;
			public Vector2 scale;
			public Vector2 overshootScale;
			public float   overshootDuration;
			public float   afterOvershootDuration;
			public Ease    overshootScaleEase;
			public Ease    afterOvershootScaleEase;
			public Ease    overshootColorEase;
			public Ease    afterOvershootColorEase;
		}
		public StateConfig selected;
		public StateConfig deselected;
		public StateConfig activated;
	}
}
