using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Mathematics;
using static Unity.Mathematics.math;
using Sirenix.OdinInspector;
using DG.Tweening;
using tg;
using static tg.tg;

namespace devember2021
{
	public class UIAnimFloating : MonoBehaviour
	{
		[MinValue(0)]
		public float period_duration = 1;
		public float amplitude       = 10;
		[Tooltip("Exponent used for the cosine. Values < 1 converge towards a square wave. Values > 1 converge to a Dirac's delta.")]
		public float stepness        = 1;
		#region Unity
		private
		void
		Awake()
		{
			tr = transform;
			rtr = GetComponent<RectTransform>();
		}
		private
		void
		Update()
		{
			float3 lp = tr.localPosition;
			float t = cos( Time.time / period_duration * PI * 2.0f );
			lp.y = sign( t ) * pow( abs( t ), stepness ) * amplitude;
			tr.localPosition = lp;
		}
		#endregion Unity

		#region private
		private Transform tr;
		private RectTransform rtr;
		#endregion private
	}
}
