using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Mathematics;
using static Unity.Mathematics.math;
using Sirenix.OdinInspector;
using DG.Tweening;
using tg;
using static tg.tg;

namespace devember2021
{
	public class UI_Character_Selection : MonoBehaviour
	{
		public UI_Selecting_Character[] selectingCharacters;

		public
		void
		reset()
		{
			activated_count = 0;
			for ( int i = 0; i != selectingCharacters.Length; ++i )
			{
				selectingCharacters[i].deactivate();
			}
		}

		public
		Game_State.Character[]
		selected_characters()
		{
			Game_State.Character[] ret = new Game_State.Character[activated_count];

			int i = 0;
			for ( int si = 0; si != selectingCharacters.Length; ++si )
			{
				var sc = selectingCharacters[si];
				if ( sc.isActivated )
				{
					ret[i++] = new Game_State.Character(){
						character_so      = sc.character,
						team              = sc.team,
						gamepad_device_id = sc.gamepadDeviceID
					};
				}
			}

			return ret;
		}

		public int activated_count { get; private set; }
		public bool all_activated => activated_count == selectingCharacters.Length;

		#region Unity
		private
		void
		Awake()
		{
			selectingCharacters = GetComponentsInChildren<UI_Selecting_Character>( includeInactive: false );
		}
		private
		void
		Start()
		{
			reset();
		}
		private
		void
		Update()
		{
			if ( ! all_activated )
			{
				foreach ( var g in Game_Input.instance.gamepads.Values )
				{
					if ( g.buttons_active.any() && ! is_gamepad_assigned( g.gamepad_device_id ) )
					{
						first_unselected().activate( g.gamepad_device_id );
						++activated_count;

						if ( all_activated )
						{
							break;
						}
					}
				}
			}
		}
		#endregion Unity

		#region private
		private
		bool
		is_gamepad_assigned( int gamepadID )
		{
			bool ret = false;
			for ( int i = 0; i != selectingCharacters.Length; ++i )
			{
				var c = selectingCharacters[i];
				if ( c.gamepadDeviceID == gamepadID )
				{
					ret = true;
					break;
				}
			}
			return ret;
		}
		private
		UI_Selecting_Character
		first_unselected()
		{
			UI_Selecting_Character ret = null;
			for ( int i = 0; i != selectingCharacters.Length; ++i )
			{
				var c = selectingCharacters[i];
				if ( ! c.isActivated )
				{
					ret = c;
					break;
				}
			}
			return ret;
		}
		#endregion private
	}
}
