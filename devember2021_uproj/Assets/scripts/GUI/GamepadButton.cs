using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Mathematics;
using static Unity.Mathematics.math;
using Sirenix.OdinInspector;
using tg;
using static tg.tg;
using UnityEngine.Events;

namespace devember2021
{
	public class GamepadButton : MonoBehaviour, ISelectable, IActivatable
	{
		// NOTE(theGiallo): bool request_animation
		[Serializable] public class ButtonEvent : UnityEvent<bool> {}
		public ButtonEvent onSelected;
		public ButtonEvent onDeselected;
		public ButtonEvent onActivated;
		public bool isSelected { get; private set; }
		public
		void
		Select( bool request_animation = true )
		{
			isSelected = true;
			onSelected?.Invoke( request_animation );
		}
		public
		void
		Deselect( bool request_animation = true )
		{
			isSelected = false;
			onDeselected?.Invoke( request_animation );
		}

		public
		void
		Activate( bool request_animation = true )
		{
			onActivated?.Invoke( request_animation );
		}

		#region Unity
		#endregion Unity

		#region private
		#endregion private
	}

	public interface IActivatable
	{
		public void Activate( bool request_animation = true );
	}
	public interface ISelectable
	{
		public bool isSelected { get; }
		public void Select( bool request_animation = true );
		public void Deselect( bool request_animation = true );
	}
}
