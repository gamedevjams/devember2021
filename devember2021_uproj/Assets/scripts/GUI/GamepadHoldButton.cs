using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Mathematics;
using static Unity.Mathematics.math;
using Sirenix.OdinInspector;
using DG.Tweening;
using tg;
using static tg.tg;
using UnityEngine.Events;

namespace devember2021
{
	public class GamepadHoldButton : MonoBehaviour
	{
		[Serializable] public class HoldEvent : UnityEvent<int> {}

		public float durationSecs = 1;
		[Tooltip("-1 means any pad")]
		public int gamepadId = -1;
		public Game_Input.Gamepad_Button button;
		public bool enableContinuousHoldings;
		public bool enableCompletionResetAfterRelease;

		public HoldEvent onStarted;
		public HoldEvent onInterrupted;
		public HoldEvent onComplete;

		[ShowInInspector,HideInEditorMode]
		public float progress => timer.active ? Mathf.Clamp01( timer.passed_percentage() ) : 0;
		[ShowInInspector,HideInEditorMode]
		public bool holdComplete    { get; private set; }
		[ShowInInspector,HideInEditorMode]
		public int  holdCompletions { get; private set; }
		[ShowInInspector,HideInEditorMode]
		public bool holding => timer.active;

		public
		void
		ResetHold()
		{
			if ( enableCompletionResetAfterRelease )
			{
				holdComplete = false;
				holdCompletions = 0;
			}

			timer.active = false;
			current_any_gamepad_id = -1;
		}

		#region Unity
		private
		void
		Awake()
		{
			timer = new Timer( durationSecs );
		}
		private
		void
		Update()
		{
			if ( holding && ! holdComplete && timer.passed_percentage() >= 1 )
			{
				holdComplete = true;
				++holdCompletions;
				onComplete?.Invoke( holdCompletions );
				if ( enableContinuousHoldings )
				{
					holdComplete = false;
					timer.loopback();
					started();
				}
			}

			if ( gamepadId == -1 && current_any_gamepad_id == -1 )
			{
				foreach ( var g in Game_Input.instance.gamepads.Values )
				{
					if ( g.buttons_active[(int)button] == 1 )
					{
						current_any_gamepad_id = g.gamepad_device_id;
						break;
					}
				}
			}

			{
				int gamepad_id = gamepadId == -1 ? current_any_gamepad_id : gamepadId;
				if ( Game_Input.instance.gamepads.TryGetValue( gamepad_id, out Game_Input.Gamepad_Status g ) )
				{
					if ( g.buttons_active[(int)button] == 1 )
					{
						if ( ! holding && ! holdComplete )
						{
							timer.restart( durationSecs );
							started();
						}
					} else
					{
						interrupted();
					}
				} else
				{
					interrupted();
				}
			}
		}
		#endregion Unity

		#region private
		private Timer timer = new Timer(0);
		private int current_any_gamepad_id = -1;

		private
		void
		started()
		{
			onStarted?.Invoke( holdCompletions );
		}
		private
		void
		interrupted()
		{
			if ( ! holding )
			{
				return;
			}

			int _holdCompletions = holdCompletions;
			ResetHold();

			onInterrupted?.Invoke( _holdCompletions );
		}
		#endregion private
	}
}
