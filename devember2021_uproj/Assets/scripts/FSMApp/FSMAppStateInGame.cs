using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Mathematics;
using static Unity.Mathematics.math;
using Sirenix.OdinInspector;
using DG.Tweening;
using tg;
using static tg.tg;

namespace devember2021
{
	public class FSMAppStateInGame : MonoBehaviour, FSM.IStateMB
	{
		public class State : FSM.State
		{
			public override string name => "InGame";
			public Data data;

			public override void WillEnter( FSM<string>.State From, Action done )
			{
				data.curtainCanvasGroup.DOFade( endValue: 0, FSMApp.fadesDuration ).OnComplete( ()=>done?.Invoke() );
				data.gameLoader.Load( data.gameState );
				//data.menuCamera.gameObject.SetActive( false );
				//data.gameState.currentMap.gameCamera.gameObject.SetActive( true );
			}
			public override void WillExit( FSM<string>.State From, Action done )
			{
				data.curtainCanvasGroup.interactable   = false;
				data.curtainCanvasGroup.blocksRaycasts = true;
				data.curtainCanvasGroup.DOFade( endValue: 1, FSMApp.fadesDuration ).OnComplete( ()=>done?.Invoke() );

				data.gameLoader.Unload( data.gameState );
				//data.menuCamera.gameObject.SetActive( true );

				data.fsmInGame.fsm.GoTo( (FSM.State) null );
			}
			public override void Enter( FSM<string>.State from, Action done )
			{
				data.curtainCanvasGroup.interactable   = false;
				data.curtainCanvasGroup.blocksRaycasts = false;
				data.fsmInGame.Enter();
				done?.Invoke();
			}
			public override void Exit( FSM<string>.State To, Action done )
			{
				done?.Invoke();
			}
		}

		public State state = new State();

		#if UNITY_EDITOR
		[ReadOnly,ShowInInspector]
		private string stateName => state.name;
		#endif

		[Serializable]
		public class Data
		{
			public CanvasGroup curtainCanvasGroup;
			public Game_State  gameState;
			public Game_Loader gameLoader;
			public Camera      menuCamera;
			public FSMInGame   fsmInGame;
		}
		public Data data;

		[ShowInInspector,ValueDropdown("editor_states_names_list")] public string exitState;

		#region Unity
		private void Awake()
		{
			state.data = data;

			data.curtainCanvasGroup.interactable   = false;
			data.curtainCanvasGroup.blocksRaycasts = true;
			data.curtainCanvasGroup.alpha          = 1;

			data.gameState.onExit += exit;
			data.gameState.onRestart += handle_restart;
		}
		#endregion Unity

		FSM.State FSM.IStateMB.state => state;

		#if UNITY_EDITOR
		private List<string> editor_states_names_list() => GetComponentInParent<FSMApp>().editor_states_names_list();
		#endif // UNITY_EDITOR

		private
		void
		exit( Action done )
		{
			state.M.GoTo( exitState, done );
		}
		private
		void
		handle_restart( Action done )
		{
			state.M.GoTo( state, done );
		}
	}
}
