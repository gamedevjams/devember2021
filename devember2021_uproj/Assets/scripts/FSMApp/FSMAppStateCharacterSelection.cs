using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Mathematics;
using static Unity.Mathematics.math;
using Sirenix.OdinInspector;
using DG.Tweening;
using tg;
using static tg.tg;

namespace devember2021
{
	public class FSMAppStateCharacterSelection : MonoBehaviour, FSM.IStateMB
	{
		public class State : FSM.State
		{
			public override string name => "CharacterSelection";
			public Data data;

			public override void WillEnter( FSM<string>.State From, Action done )
			{
				data.gamepadHoldButtonBack    .ResetHold();
				data.gamepadHoldButtonContinue.ResetHold();
				data.canvasGroup.DOFade( endValue: 1, FSMApp.fadesDuration ).OnComplete( ()=>done?.Invoke() );
				data.uiCharacterSelection.reset();
				data.gamepadHoldButtonContinue.gameObject.SetActive( false );
				some_activated = false;
			}
			public override void WillExit( FSM<string>.State To, Action done )
			{
				//gamepadButtons.enabled = false;
				data.canvasGroup.interactable   = false;
				data.canvasGroup.blocksRaycasts = false;
				data.gamepadHoldButtonBack    .enabled = false;
				data.gamepadHoldButtonContinue.enabled = false;
				data.canvasGroup.DOFade( endValue: 0, FSMApp.fadesDuration ).OnComplete( ()=>done?.Invoke() );
				data.uiCharacterSelection.enabled = false;

				if ( To.name == data.nextState )
				{
					data.game_state.set_characters( data.uiCharacterSelection.selected_characters() );
				}
			}
			public override void Enter( FSM<string>.State from, Action done )
			{
				data.canvasGroup.interactable   = true;
				data.canvasGroup.blocksRaycasts = true;
				data.gamepadHoldButtonBack    .enabled = true;
				data.gamepadHoldButtonContinue.enabled = true;
				data.uiCharacterSelection.enabled = true;
				//gamepadButtons.enabled = true;
				done?.Invoke();
			}
			public override void Exit( FSM<string>.State To, Action done )
			{
				done?.Invoke();
			}
			public override void Update()
			{
				if ( data.uiCharacterSelection.activated_count != 0 && ! some_activated )
				{
					some_activated = true;
					data.gamepadHoldButtonContinue.gameObject.SetActive( true );
				}
			}

			private bool some_activated;
		}

		public State state = new State();

		#if UNITY_EDITOR
		[ReadOnly,ShowInInspector]
		private string stateName => state.name;
		#endif

		[Serializable]
		public class Data
		{
			public CanvasGroup            canvasGroup;
			public GamepadHoldButton      gamepadHoldButtonBack;
			public GamepadHoldButton      gamepadHoldButtonContinue;
			public UI_Character_Selection uiCharacterSelection;
			public Game_State             game_state;


			[NonSerialized] public string nextState;
			[NonSerialized] public string prevState;
		}
		public Data data;

		[ShowInInspector,ValueDropdown("editor_states_names_list")] public string nextState;
		[ShowInInspector,ValueDropdown("editor_states_names_list")] public string prevState;

		#region Unity
		private void Awake()
		{
			state.data = data;
			//state.gamepadButtons = gamepadButtons;

			data.canvasGroup.interactable   = false;
			data.canvasGroup.blocksRaycasts = false;
			data.canvasGroup.alpha          = 0;
			data.gamepadHoldButtonBack    .enabled = false;
			data.gamepadHoldButtonContinue.enabled = false;

			data.gamepadHoldButtonBack    .onComplete.AddListener( (_)=>back() );
			data.gamepadHoldButtonContinue.onComplete.AddListener( (_)=>next() );

			data.nextState = nextState;
			data.prevState = prevState;

			data.uiCharacterSelection.enabled = false;

			//gamepadButtons.enabled = false;
		}
		#endregion Unity

		FSM.State FSM.IStateMB.state => state;

		#if UNITY_EDITOR
		private List<string> editor_states_names_list() => GetComponentInParent<FSMApp>().editor_states_names_list();
		#endif // UNITY_EDITOR

		private
		void
		next()
		{
			state.M.GoTo( nextState );
		}
		private
		void
		back()
		{
			state.M.GoTo( prevState );
		}
	}
}
