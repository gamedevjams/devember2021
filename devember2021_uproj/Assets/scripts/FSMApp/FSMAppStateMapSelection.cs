using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Mathematics;
using static Unity.Mathematics.math;
using Sirenix.OdinInspector;
using DG.Tweening;
using tg;
using static tg.tg;

namespace devember2021
{
	public class FSMAppStateMapSelection : MonoBehaviour, FSM.IStateMB
	{
		public class State : FSM.State
		{
			public override string name => "MapSelection";
			public Data data;

			public override void WillEnter( FSM<string>.State From, Action done )
			{
				data.gamepadButtons.reset_selection();
				select_map( 0 );
				data.canvasGroup.DOFade( endValue: 1, FSMApp.fadesDuration ).OnComplete( ()=>done?.Invoke() );
			}
			public override void WillExit( FSM<string>.State From, Action done )
			{
				data.gamepadButtons.enabled = false;
				data.canvasGroup.interactable   = false;
				data.canvasGroup.blocksRaycasts = false;
				data.gamepadHoldButtonBack    .enabled = false;
				data.gamepadHoldButtonContinue.enabled = false;
				data.canvasGroup.DOFade( endValue: 0, FSMApp.fadesDuration ).OnComplete( ()=>done?.Invoke() );
			}
			public override void Enter( FSM<string>.State from, Action done )
			{
				data.canvasGroup.interactable   = true;
				data.canvasGroup.blocksRaycasts = true;
				data.gamepadButtons.enabled = true;
				data.gamepadHoldButtonBack    .enabled = true;
				data.gamepadHoldButtonContinue.enabled = true;
				done?.Invoke();
			}
			public override void Exit( FSM<string>.State To, Action done )
			{
				data.gamepadHoldButtonBack    .ResetHold();
				data.gamepadHoldButtonContinue.ResetHold();
				done?.Invoke();
			}

			private Tween selectedMapTween;
			private int selected_map_index = 0;

			public
			void
			select_map( int index )
			{
				selected_map_index = index;

				var m = data.maps[index];
				Transform button_tr = data.buttonsGenerator.transform.GetChild( index );
				float y = data.selectedMapMarker.parent.InverseTransformPoint( button_tr.position ).y;
				selectedMapTween?.Kill();
				selectedMapTween = data.selectedMapMarker.DOLocalMoveY( y, data.selectedMapMarkerAnimDurationSecs ).SetEase( data.selectedMapMarkerAnimEase ).SetAutoKill( false );

				data.gameLoader.mapToLoad = m;
			}
		}

		public State state = new State();

		#if UNITY_EDITOR
		[ReadOnly,ShowInInspector]
		private string stateName => state.name;
		#endif

		[Serializable]
		public class Data
		{
			public CanvasGroup       canvasGroup;
			public GamepadButtons    gamepadButtons;
			public ButtonsGenerator  buttonsGenerator;
			public GamepadHoldButton gamepadHoldButtonBack;
			public GamepadHoldButton gamepadHoldButtonContinue;
			public Transform         selectedMapMarker;
			public float             selectedMapMarkerAnimDurationSecs = 0.3f;
			public Ease              selectedMapMarkerAnimEase;
			public Game_Loader       gameLoader;
			public List<Map>         maps;
		}
		public Data data;

		[ShowInInspector,ValueDropdown("editor_states_names_list")] public string nextState;
		[ShowInInspector,ValueDropdown("editor_states_names_list")] public string prevState;

		#region Unity
		private void Awake()
		{
			state.data = data;

			data.canvasGroup.interactable   = false;
			data.canvasGroup.blocksRaycasts = false;
			data.canvasGroup.alpha          = 0;

			data.gamepadHoldButtonBack    .enabled = false;
			data.gamepadHoldButtonContinue.enabled = false;

			data.gamepadButtons.enabled = false;

			data.gamepadHoldButtonBack    .onComplete.AddListener( (_)=>back() );
			data.gamepadHoldButtonContinue.onComplete.AddListener( (_)=>next() );

			generate_maps_buttons();
		}
		#endregion Unity

		FSM.State FSM.IStateMB.state => state;

		#if UNITY_EDITOR
		private List<string> editor_states_names_list() => GetComponentInParent<FSMApp>().editor_states_names_list();
		#endif // UNITY_EDITOR

		#region private

		private
		void
		next()
		{
			state.M.GoTo( nextState );
		}
		private
		void
		back()
		{
			state.M.GoTo( prevState );
		}

		[Button("Generate Maps Buttons")]
		private
		void
		generate_maps_buttons()
		{
			data.buttonsGenerator.clear_buttons();
			for ( int i = 0; i != data.maps.Count; ++i )
			{
				var m = data.maps[i];
				int index = i;
				data.buttonsGenerator.instantiate_button( m.mapName, (_)=> state.select_map( index ) );
			}
		}
		#endregion private
	}
}
