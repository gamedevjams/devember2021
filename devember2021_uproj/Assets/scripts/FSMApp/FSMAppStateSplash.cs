using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Mathematics;
using UnityEngine.InputSystem;
using static Unity.Mathematics.math;
using Sirenix.OdinInspector;
using DG.Tweening;
using tg;
using static tg.tg;

namespace devember2021
{
	public class FSMAppStateSplash : MonoBehaviour, FSM.IStateMB
	{
		public class State : FSM.State
		{
			public override string name => "Splash";

			public string      nextState;
			public CanvasGroup canvasGroup;

			public override void WillEnter( FSM<string>.State From, Action done )
			{
				canvasGroup.DOFade( endValue: 1, FSMApp.fadesDuration ).OnComplete( ()=>done?.Invoke() );
			}
			public override void WillExit( FSM<string>.State From, Action done )
			{
				canvasGroup.interactable   = false;
				canvasGroup.blocksRaycasts = false;
				canvasGroup.DOFade( endValue: 0, FSMApp.fadesDuration ).OnComplete( ()=>done?.Invoke() );
			}
			public override void Enter( FSM<string>.State from, Action done )
			{
				canvasGroup.interactable   = true;
				canvasGroup.blocksRaycasts = true;
				done?.Invoke();
			}

			public override void Update()
			{
				if ( isTransitioning )
				{
					return;
				}

				bool something_pressed = false;
				foreach ( var kv in Game_Input.instance.gamepads )
				{
					var g = kv.Value;

					if ( g.buttons_active.any() )
					{
						something_pressed = true;
						break;
					}
				}
				something_pressed = something_pressed || Keyboard.current.anyKey.isPressed;
				if ( something_pressed )
				{
					log( "splash: something pressed, going to next state" );
					M.GoTo( nextState );
				}
			}
		}

		#if UNITY_EDITOR
		[ShowInInspector]
		private string stateName => state.name;
		#endif // UNITY_EDITOR

		public State state = new State();

		[ShowInInspector,ValueDropdown("editor_states_names_list")]
		public string nextState;
		public CanvasGroup canvasGroup;

		#region Unity
		private void Awake()
		{
			state.nextState   = nextState;
			state.canvasGroup = canvasGroup;

			canvasGroup.interactable   = false;
			canvasGroup.blocksRaycasts = false;
			canvasGroup.alpha          = 0;
		}
		#endregion Unity

		#if UNITY_EDITOR
		private List<string> editor_states_names_list() => GetComponentInParent<FSMApp>().editor_states_names_list();
		#endif // UNITY_EDITOR

		FSM.State FSM.IStateMB.state => state;
	}
}
