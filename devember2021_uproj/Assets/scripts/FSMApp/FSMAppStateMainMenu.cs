using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Mathematics;
using static Unity.Mathematics.math;
using Sirenix.OdinInspector;
using DG.Tweening;
using tg;
using static tg.tg;

namespace devember2021
{
	public class FSMAppStateMainMenu : MonoBehaviour, FSM.IStateMB
	{
		public class State : FSM.State
		{
			public override string name => "MainMenu";
			public Data data;

			public override void WillEnter( FSM<string>.State From, Action done )
			{
				data.gamepadButtons.reset_selection();
				data.canvasGroup.DOFade( endValue: 1, FSMApp.fadesDuration ).OnComplete( ()=>done?.Invoke() );
			}
			public override void WillExit( FSM<string>.State From, Action done )
			{
				data.gamepadButtons.enabled = false;
				data.canvasGroup.interactable   = false;
				data.canvasGroup.blocksRaycasts = false;
				data.canvasGroup.DOFade( endValue: 0, FSMApp.fadesDuration ).OnComplete( ()=>done?.Invoke() );
			}
			public override void Enter( FSM<string>.State from, Action done )
			{
				data.canvasGroup.interactable   = true;
				data.canvasGroup.blocksRaycasts = true;
				data.gamepadButtons.enabled = true;
				done?.Invoke();
			}
		}

		public State state = new State();

		#if UNITY_EDITOR
		[ReadOnly,ShowInInspector]
		private string stateName => state.name;
		#endif

		[Serializable]
		public class Data
		{
			public CanvasGroup      canvasGroup;
			public GamepadButtons   gamepadButtons;
			public ButtonsGenerator buttonsGenerator;
		}
		public Data data;

		[ShowInInspector,ValueDropdown("editor_states_names_list")] public string newGameState;
		[ShowInInspector,ValueDropdown("editor_states_names_list")] public string settingsState;
		public string newGameButtonText;
		public string settingsButtonText;
		public string quitButtonText;

		#region Unity
		private void Awake()
		{
			state.data = data;

			data.canvasGroup.interactable   = false;
			data.canvasGroup.blocksRaycasts = false;
			data.canvasGroup.alpha          = 0;

			data.gamepadButtons.enabled = false;

			generate_buttons();
		}
		#endregion Unity

		FSM.State FSM.IStateMB.state => state;

		#if UNITY_EDITOR
		private List<string> editor_states_names_list() => GetComponentInParent<FSMApp>().editor_states_names_list();
		#endif // UNITY_EDITOR

		private
		void
		new_game( bool request_animation )
		{
			state.M.GoTo( newGameState );
		}
		private
		void
		settings( bool request_animation )
		{
			state.M.GoTo( settingsState );
		}
		private
		void
		quit( bool request_animation )
		{
			Application.Quit();
		}
		[Button("Generate Buttons")]
		private
		void
		generate_buttons()
		{
			data.buttonsGenerator.clear_buttons();
			data.buttonsGenerator.instantiate_button( newGameButtonText,  new_game );
			data.buttonsGenerator.instantiate_button( settingsButtonText, settings );
			data.buttonsGenerator.instantiate_button( quitButtonText,     quit     );
		}
	}
}
