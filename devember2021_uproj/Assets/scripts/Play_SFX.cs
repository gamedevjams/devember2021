using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Mathematics;
using static Unity.Mathematics.math;
using Sirenix.OdinInspector;
using DG.Tweening;
using tg;
using static tg.tg;

namespace devember2021
{
	public class Play_SFX : MonoBehaviour
	{
		public string sfx;
		public Sound_System_Provider soundSystemProvider;
		public Sound_System sound_system;

		public
		void
		play()
		{
			if ( sound_system != null )
			{
				sound_system.playSFX( sfx, tr.position );
			}
		}

		#region Unity
		private
		void
		Awake()
		{
			tr = transform;
			soundSystemProvider.onSet += handle_sound_system_set;
		}
		#endregion Unity

		#region private
		private Transform tr;
		private void handle_sound_system_set( Sound_System ss )
		{
			this.sound_system = ss;
		}
		#endregion private
	}
}
