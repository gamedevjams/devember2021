using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Mathematics;
using static Unity.Mathematics.math;
using Sirenix.OdinInspector;
using DG.Tweening;
using tg;
using static tg.tg;

namespace devember2021
{
	public class Provider<T> : MonoBehaviour, IProvider<T>
	{
		[ValidateInput("editor_validate_chained_provider")]
		public Provider<T> chainedProvider;

		[SerializeField] private T value;
		public event Action<T> onSet;
		public void set( T v )
		{
			value = v;
			ever_set = true;
			onSet?.Invoke( value );
		}
		public T get() => value;

		#region Unity
		private
		void
		Awake()
		{
			if ( chainedProvider != null )
			{
				chainedProvider.onSet += set;
			}

			if ( ! ever_set )
			{
				onSet?.Invoke( value );
			}
		}
		#endregion Unity

		#region private
		private bool ever_set;

		#if UNITY_EDITOR
		private
		bool
		editor_validate_chained_provider( Provider<T> v ) => v != this;
		#endif
		#endregion private
	}
	public interface IProvider<T>
	{
		T get();
	}
}
