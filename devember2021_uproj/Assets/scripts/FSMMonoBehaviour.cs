using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Mathematics;
using static Unity.Mathematics.math;
using Sirenix.OdinInspector;
using tg;
using static tg.tg;

namespace devember2021
{
	public class FSMMonoBehaviour : MonoBehaviour
	{
		public static float fadesDuration = 0.5f;

		[ShowInInspector,ValueDropdown("editor_states_names_list")]
		public string startingState = null;
		[ShowInInspector,ValueDropdown("editor_states_names_list")]
		public string entranceState = null;

		public FSM fsm { get; private set; }
		public
		void
		Enter()
		{
			fsm.GoTo( entranceState );
		}

		#region Unity
		private
		void
		Awake()
		{
			fsm = new FSM();
		}

		private
		void
		Start()
		{
			gather_states();

			for ( int i = 0; i != states.Length; ++i )
			{
				fsm.Add( states[i] );
			}

			fsm.GoTo( startingState );
		}
		private void Update()
		{
			fsm.Update();
		}
		#endregion Unity

		#if UNITY_EDITOR
		private void OnValidate()
		{
			gather_states();
		}
		#endif

		private void gather_states()
		{
			this.GetComponentsListInDirectChildren( istates, includeInactive: false );
			if ( states == null || states.Length != istates.Count )
			{
				states = new FSM.State[istates.Count];
			}
			for ( int i = 0; i != states.Length; ++i )
			{
				states[i] = istates[i].state;
			}
		}

		#region private
		private FSM.State[] states;
		private List<FSM.IStateMB> istates = new List<FSM<string>.IStateMB>();

		#if UNITY_EDITOR
		[ShowInInspector,LabelText("Transitioning "),HideInEditorMode]
		private string in_editor_transitioning => ( fsm?.transitioning ?? false ) ? $"{fsm.transitioning_from?.name} => {fsm.transitioning_to?.name}" : "---";
		[ShowInInspector,LabelText("Current State"),HideInEditorMode]
		private string in_editor_curr_state => fsm?.curr_state?.name;
		[ShowInInspector,LabelText("State To Go To"),ValueDropdown("editor_states_names_list"),DisableInEditorMode]
		private string in_editor_go_to_state;
		[Button("Go To"),DisableInEditorMode]
		private void editor_go_to()
		{
			fsm.GoTo( in_editor_go_to_state );
		}
		private List<string> _editor_states_names_list = new List<string>();

		public List<string> editor_states_names_list()
		{
			if ( _editor_states_names_list == null )
			{
				_editor_states_names_list = new List<string>();
			}
			_editor_states_names_list.Clear();
			_editor_states_names_list.Add( "null" );

			if ( Application.isPlaying )
			{
				fsm.StatesList( _editor_states_names_list );
			} else
			{
				gather_states();

				if ( _editor_states_names_list == null )
				{
					_editor_states_names_list = new List<string>();
				}

				_editor_states_names_list.Reserve( states.Length );

				for ( int i = 0; i != states.Length; ++i )
				{
					if ( states[i] == null )
					{
						log_err( $"states[{i}] is null", gameObject );
					} else
					{
						_editor_states_names_list.Add( states[i].name );
					}
				}
			}

			return _editor_states_names_list;
		}
		#endif // UNITY_EDITOR

		#endregion private
	}
}
