using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using tg;
using static tg.tg;
using Sirenix.OdinInspector;
using Unity.Mathematics;
using static Unity.Mathematics.math;

namespace devember2021
{
	public class Physic_Materials_SFXs : MonoBehaviour
	{
		public Physic_Materials_SFXs_Projectiles_SO physicMaterialsSFXsProjectilesSO;

		public
		string
		SFX( PhysicMaterial pm ) => physicMaterialsSFXsProjectilesSO.SFX( pm );

		#region Unity
		private
		void
		Awake()
		{
			physicMaterialsSFXsProjectilesSO.init();
		}
		#endregion Unity

		#region private
		#endregion private
	}
}
