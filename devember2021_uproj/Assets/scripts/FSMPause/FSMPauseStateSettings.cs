using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Mathematics;
using static Unity.Mathematics.math;
using Sirenix.OdinInspector;
using DG.Tweening;
using tg;
using static tg.tg;

namespace devember2021
{
	public class FSMPauseStateSettings : MonoBehaviour, FSM.IStateMB
	{
		public class State : FSM.State
		{
			public override string name => "Settings";
			public Data data;

			public override void WillEnter( FSM<string>.State From, Action done )
			{
				data.canvasGroup.DOFade( endValue: 1, FSMApp.fadesDuration ).OnComplete( ()=>done?.Invoke() );
			}
			public override void WillExit( FSM<string>.State From, Action done )
			{
				data.canvasGroup.interactable   = false;
				data.canvasGroup.blocksRaycasts = false;
				data.canvasGroup.DOFade( endValue: 0, FSMApp.fadesDuration ).OnComplete( ()=>done?.Invoke() );

				data.backHoldButton.enabled = false;
			}
			public override void Enter( FSM<string>.State from, Action done )
			{
				data.canvasGroup.interactable   = true;
				data.canvasGroup.blocksRaycasts = true;

				data.backHoldButton.enabled = true;

				done?.Invoke();
			}
		}

		public State state = new State();

		#if UNITY_EDITOR
		[ReadOnly,ShowInInspector]
		private string stateName => state.name;
		#endif

		[Serializable]
		public class Data
		{
			public CanvasGroup       canvasGroup;
			public Game_State        gameState;
			public GamepadHoldButton backHoldButton;
		}
		public Data data;

		[ShowInInspector,ValueDropdown("editor_states_names_list")] public string pauseState;

		#region Unity
		private void Awake()
		{
			state.data = data;

			data.canvasGroup.interactable   = false;
			data.canvasGroup.blocksRaycasts = false;
			data.canvasGroup.alpha          = 0;

			data.backHoldButton.onComplete.AddListener( back );
			data.backHoldButton.enabled = false;
		}
		#endregion Unity

		FSM.State FSM.IStateMB.state => state;

		#if UNITY_EDITOR
		private List<string> editor_states_names_list() => GetComponentInParent<FSMPause>().editor_states_names_list();
		#endif // UNITY_EDITOR

		private
		void
		back( int _ )
		{
			state.M.GoTo( pauseState );
		}
	}
}
