using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Mathematics;
using static Unity.Mathematics.math;
using Sirenix.OdinInspector;

#if UNITY_EDITOR
using UnityEditor;
using Sirenix.OdinInspector.Editor;
using Sirenix.Utilities;
using Sirenix.Utilities.Editor;
#endif

using DG.Tweening;
using tg;
using static tg.tg;

namespace devember2021
{
	[CreateAssetMenu(fileName = "New_Match_Settings_SO", menuName = "ScriptableObjects/tg/Match Settings", order = 1)]
	public class Match_Settings_SO : SerializedScriptableObject
	{
		[Serializable]
		public class Duration
		{
			public bool  infinite;
			public float totalSeconds;

			public void Set( int h, int m, int s, int ms )
			{
				totalSeconds = Mathf.Max( 0, ms / 1000.0f + s + 60 * ( m + 60 * h ) );
			}

			public int   hours => Mathf.FloorToInt( totalSeconds / 3600 );
			public int   mins  => Mathf.FloorToInt( totalSeconds / 60 ) % 60;
			public int   secs  => Mathf.FloorToInt( totalSeconds % 60 );
			public int   ms    => Mathf.FloorToInt( 1000 * ( totalSeconds - Mathf.Floor( totalSeconds ) ) );
		}

		public enum Score_Direction
		{
			DOWN,
			UP
		}
		public enum Loss_Victory
		{
			None,
			Loss,
			Victory
		}

		public I_In_Game_UI  inGameUIPrefab;
		public I_End_Game_UI endGameUIPrefab;

		public Duration duration = new Duration() { infinite = true };
		public bool     respawns;
		public bool     teamBased;
		public bool     killsScore;
		public bool     deathsSubtractScore;
		public bool     allowNegativeScores;
		public bool     alliesKillsSubtractScore;
		public bool     suicidesSubtractScore;

		public bool     victoryScoreEnabled;
		[ShowIf("victoryScoreEnabled")]
		public int      victoryScore;
		[ShowIf("victoryScoreEnabled")]
		public Score_Direction victoryScoreDirection;

		public bool     lossScoreEnabled;
		[ShowIf("lossScoreEnabled")]
		public int      lossScore;
		[ShowIf("lossScoreEnabled")]
		public Score_Direction lossScoreDirection;

		public
		void
		setup( Game_State game_state )
		{
			this.game_state = game_state;

			game_state.onExitDone    += desetup;
			game_state.onRestartDone += desetup;

			var cs = game_state.characters;
			characters_scores = new int[cs.Count];
			characters_loss_victory = new Loss_Victory[cs.Count];

			for ( int i = 0; i != teams_scores.Length; ++i )
			{
				teams_scores[i] = 0;
				teams_loss_victory[i] = Loss_Victory.None;
			}

			for ( int i = 0; i != cs.Count; ++i )
			{
				var c = cs[i];
				Action<Damageable,Damageable.Damage> on_death = ( damageable, damage ) => handle_death( damageable, damage, c );
				on_deaths.Add( on_death );
				c.damageable.onDeath += on_death;
			}

			if ( inGameUI_instance != null )
			{
				Destroy( inGameUI_instance.gameObject );
			}

			inGameUI_instance = Instantiate( inGameUIPrefab.gameObject ).GetComponent<I_In_Game_UI>();
			inGameUI_instance.set_match_settings( this );
			game_state.inGameUI  = inGameUI_instance;

			if ( endGameUI_instance != null )
			{
				Destroy( endGameUI_instance.gameObject );
			}

			endGameUI_instance = Instantiate( endGameUIPrefab.gameObject ).GetComponent<I_End_Game_UI>();
			endGameUI_instance.set_match_settings( this );
			game_state.endGameUI = endGameUI_instance;

			game_state.onStart   += handle_start_game;
			game_state.onEndGame += handle_end_game;
		}

		public
		void
		desetup()
		{
			game_state.onExitDone    -= desetup;
			game_state.onRestartDone -= desetup;
			game_state.onStart    -= handle_start_game;
			game_state.onEndGame  -= handle_end_game;

			var cs = game_state.characters;
			for ( int i = 0; i != cs.Count; ++i )
			{
				var c = cs[i];
				if ( c.damageable != null )
				{
					c.damageable.onDeath -= on_deaths[i];
				}
			}
			on_deaths.Clear();

			if ( inGameUI_instance != null )
			{
				if ( game_state.inGameUI == inGameUI_instance )
				{
					game_state.inGameUI = null;
				}
				Destroy( inGameUI_instance.gameObject );
				inGameUI_instance = null;
			}

			if ( endGameUI_instance != null )
			{
				if ( game_state.endGameUI == endGameUI_instance )
				{
					game_state.endGameUI = null;
				}
				Destroy( endGameUI_instance.gameObject );
				endGameUI_instance = null;
			}
		}

		public
		void
		update()
		{
			if ( ! duration.infinite && game_state.gameTime >= duration.totalSeconds )
			{
				// TODO(theGiallo, 2021-12-29): handle time overflow for UI timers
				end_game();
			}
		}

		#region Unity
		#endregion Unity

		#region private
		private List<Action<Damageable,Damageable.Damage>> on_deaths = new List<Action<Damageable, Damageable.Damage>>();

		protected I_In_Game_UI  inGameUI_instance;
		protected I_End_Game_UI endGameUI_instance;

		[NonSerialized                 , HideInEditorMode] public Game_State     game_state;
		[NonSerialized, ShowInInspector, HideInEditorMode] public int[]          characters_scores;
		[NonSerialized, ShowInInspector, HideInEditorMode] public int[]          teams_scores       = new int[2];
		[NonSerialized, ShowInInspector, HideInEditorMode] public Loss_Victory[] characters_loss_victory;
		[NonSerialized, ShowInInspector, HideInEditorMode] public Loss_Victory[] teams_loss_victory = new Loss_Victory[2];

		private void handle_death( Damageable damageable, Damageable.Damage damage, Game_State.Character character )
		{
			var killer_character = game_state.character( damage.character_id );

			if ( killsScore )
			{
				if ( killer_character != null )
				{
					if ( teamBased )
					{
						if ( killer_character.team != character.team )
						{
							++teams_scores[team_index( killer_character.team )];
						} else
						if ( alliesKillsSubtractScore )
						{
							--teams_scores[team_index( killer_character.team )];

							if ( ! allowNegativeScores )
							{
								teams_scores[team_index( killer_character.team )] = Mathf.Max( 0, teams_scores[team_index( killer_character.team )] );
							}
						}
					} else
					{
						if ( killer_character.character_id != character.character_id )
						{
							++characters_scores[killer_character.character_id.id];
						} else
						if ( alliesKillsSubtractScore || suicidesSubtractScore )
						{
							--characters_scores[killer_character.character_id.id];

							if ( ! allowNegativeScores )
							{
								characters_scores[killer_character.character_id.id] = Mathf.Max( 0, characters_scores[killer_character.character_id.id] );
							}
						}
					}
				}
			}

			if ( deathsSubtractScore )
			{
				--characters_scores[character.character_id.id];

				if ( ! allowNegativeScores )
				{
					characters_scores[character.character_id.id] = Mathf.Max( 0, characters_scores[character.character_id.id] );
				}

				if ( teamBased )
				{
					--teams_scores[team_index( character.team )];

					if ( ! allowNegativeScores )
					{
						teams_scores[team_index( character.team )] = Mathf.Max( 0, teams_scores[team_index( character.team )] );
					}
				}
			}

			if ( teamBased )
			{
				bool one_team_won     = false;
				int  teams_loss_count = 0;
				for ( int i = 0; i != teams_loss_victory.Length; ++i )
				{
					if ( teams_loss_victory[i] == Loss_Victory.None )
					{
						Loss_Victory victory = check_victory_score( teams_scores[i] );
						Loss_Victory loss    = check_loss_score   ( teams_scores[i] );
						if ( victory == Loss_Victory.Victory )
						{
							teams_loss_victory[i] = victory;
						} else
						if ( loss == Loss_Victory.Loss )
						{
							teams_loss_victory[i] = loss;
						}
					}

					if ( teams_loss_victory[i] == Loss_Victory.Victory )
					{
						one_team_won = true;
					}

					if ( teams_loss_victory[i] == Loss_Victory.Loss )
					{
						++teams_loss_count;
					}
				}
				if ( one_team_won || teams_loss_count == teams_loss_victory.Length - 1 )
				{
					end_game();
				}
			} else
			{
				for ( int i = 0; i != characters_loss_victory.Length; ++i )
				{
					bool one_ch_won    = false;
					int  ch_loss_count = 0;
					if ( characters_loss_victory[i] == Loss_Victory.None )
					{
						Loss_Victory victory = check_victory_score( characters_scores[i] );
						Loss_Victory loss    = check_loss_score   ( characters_scores[i] );
						if ( victory == Loss_Victory.Victory )
						{
							characters_loss_victory[i] = victory;
						} else
						if ( loss == Loss_Victory.Loss )
						{
							characters_loss_victory[i] = loss;
						}

						if ( characters_loss_victory[i] == Loss_Victory.Victory )
						{
							one_ch_won = true;
						}

						if ( characters_loss_victory[i] == Loss_Victory.Loss )
						{
							++ch_loss_count;
						}
					}

					if ( one_ch_won
					  || ( ch_loss_count > 0
					    && ch_loss_count == characters_loss_victory.Length - 1 ) )
					{
						end_game();
					}
				}
			}

			if ( respawns )
			{
				character.respawn();
			} else
			{
				character.disable();
			}
		}
		private
		Loss_Victory
		check_victory_score( int score )
		{
			Loss_Victory ret = Loss_Victory.None;
			if ( victoryScoreEnabled )
			{
				switch ( victoryScoreDirection )
				{
					case Score_Direction.DOWN:
						if ( score <= victoryScore )
						{
							ret = Loss_Victory.Victory;
						}
						break;
					case Score_Direction.UP:
						if ( score >= victoryScore )
						{
							ret = Loss_Victory.Victory;
						}
						break;
				}
			}
			return ret;
		}

		private
		Loss_Victory
		check_loss_score( int score )
		{
			Loss_Victory ret = Loss_Victory.None;
			if ( lossScoreEnabled )
			{
				switch ( lossScoreDirection )
				{
					case Score_Direction.DOWN:
						if ( score <= lossScore )
						{
							ret = Loss_Victory.Loss;
						}
						break;
					case Score_Direction.UP:
						if ( score >= lossScore )
						{
							ret = Loss_Victory.Loss;
						}
						break;
				}
			}
			return ret;
		}

		private
		int
		team_index( Game_State.Team team ) => team == Game_State.Team.A ? 0 : ( team == Game_State.Team.B ? 1 : -1 );

		private
		void
		end_game()
		{
			game_state.end_game();
		}
		private
		void
		handle_start_game()
		{
			if ( inGameUI_instance == null ) log_err( "inGameUI_instance == null" );
			inGameUI_instance.canvasGroup.alpha          = 1;
			inGameUI_instance.canvasGroup.interactable   = true;
			inGameUI_instance.canvasGroup.blocksRaycasts = false;

			if ( endGameUI_instance == null ) log_err( "endGameUI_instance == null" );
			endGameUI_instance.canvasGroup.alpha          = 0;
			endGameUI_instance.canvasGroup.interactable   = false;
			endGameUI_instance.canvasGroup.blocksRaycasts = false;
		}
		private
		void
		handle_end_game()
		{
			inGameUI_instance.canvasGroup.alpha          = 0;
			inGameUI_instance.canvasGroup.interactable   = false;
			inGameUI_instance.canvasGroup.blocksRaycasts = false;
		}
		#endregion private
	}

#if UNITY_EDITOR
	public class DurationDrawer : OdinValueDrawer<Match_Settings_SO.Duration>
	{
		protected override void DrawPropertyLayout( GUIContent label )
		{
			Rect rect = EditorGUILayout.GetControlRect();

			if ( label != null )
			{
				rect = EditorGUI.PrefixLabel( rect, label );
			}

			Match_Settings_SO.Duration value = this.ValueEntry.SmartValue;
			GUIHelper.PushLabelWidth( 20 );

			int inf_width = 50;
			rect = rect.SubXMin( inf_width );
			bool new_infinite = EditorGUI.Toggle( rect.AlignLeft( inf_width ), "Inf", value.infinite );
			rect = rect.AddXMin( inf_width );

			if ( new_infinite != value.infinite )
			{
				this.Property.RecordForUndo( $"toggle infinite duration in {this.Property.Name}" );
				value.infinite = new_infinite;
			}

			if ( ! value.infinite )
			{
				var labels =  new GUIContent[]{ new GUIContent( "h" ),
				                                new GUIContent( "m"  ),
				                                new GUIContent( "s"  ),
				                                new GUIContent( "ms"  ),
				};
				int[] hmsms = new int[]{ value.hours, value.mins, value.secs, value.ms };

				EditorGUI.BeginChangeCheck();

				EditorGUI.MultiIntField( rect.AlignLeft( rect.width ), labels, hmsms );

				if ( EditorGUI.EndChangeCheck() )
				{
					float new_value = Mathf.Max( 0, hmsms[3] / 1000.0f + hmsms[2] + 60 * ( hmsms[1] + 60 * hmsms[0] ) );
					if ( value.totalSeconds != new_value )
					{
						this.Property.RecordForUndo( $"change duration of {this.Property.Name}" );
					}

					value.totalSeconds = new_value;
				}
			}

			GUIHelper.PopLabelWidth();

			this.ValueEntry.SmartValue = value;
		}
	}
#endif
}
