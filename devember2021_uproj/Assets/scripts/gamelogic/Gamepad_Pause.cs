using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Mathematics;
using static Unity.Mathematics.math;
using Sirenix.OdinInspector;
using DG.Tweening;
using tg;
using static tg.tg;

namespace devember2021
{
	public class Gamepad_Pause : MonoBehaviour
	{
		public Game_Input.Gamepad_Button pauseButton = Game_Input.Gamepad_Button.START;
		public Game_State gameState;

		#region Unity
		private
		void
		Update()
		{
			if ( ! gameState.can_update )
			{
				return;
			}

			foreach ( var g in Game_Input.instance.gamepads.Values )
			{
				if ( g.buttons_just_activated[(int)pauseButton] == 1 )
				{
					gameState.pause();
				}
			}
		}
		#endregion Unity

		#region private
		#endregion private
	}
}
