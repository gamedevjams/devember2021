using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using Unity.Mathematics;
using static Unity.Mathematics.math;
using Sirenix.OdinInspector;
using DG.Tweening;
using tg;
using static tg.tg;

namespace devember2021
{
	public class Keyboard_Pause : MonoBehaviour
	{
		public Key pauseButton = Key.Escape;
		public Game_State gameState;

		#region Unity
		private
		void
		Update()
		{
			if ( ! gameState.can_update )
			{
				return;
			}

			if ( Keyboard.current[pauseButton].wasPressedThisFrame )
			{
				gameState.pause();
			}
		}
		#endregion Unity

		#region private
		#endregion private
	}
}
