using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Mathematics;
using static Unity.Mathematics.math;
using Sirenix.OdinInspector;
using DG.Tweening;
using tg;
using static tg.tg;

namespace devember2021
{
	public class Game_Loader : MonoBehaviour
	{
		[NonSerialized,ShowInInspector,HideInEditorMode] public Map mapToLoad;
		public Camera outOfGameCamera;

		public
		void
		Load( Game_State target_game_state )
		{
			log( $"Load {mapToLoad.name}" );
			outOfGameCamera.gameObject.SetActive( false );

			Map instantiated_map = Instantiate( mapToLoad );
			target_game_state.set_map( instantiated_map );

			instantiated_map.gameCamera.gameObject.SetActive( true );

			Spawner spawner = new Spawner( instantiated_map );

			var cs = target_game_state.characters;
			for ( int i = 0; i != cs.Count; ++i )
			{
				Spawn_Point sp = spawner.take_spawn_point( cs[i].team );
				cs[i].instantiate( target_game_state, instantiated_map.transform, sp.transform.position, instantiated_map.gameCamera );
			}
		}
		public
		void
		Unload( Game_State target_game_state )
		{
			log( $"Unload {target_game_state.currentMap?.name}" );
			if ( target_game_state.currentMap != null )
			{
				Destroy( target_game_state.currentMap.gameObject );
				target_game_state.set_map( null );

				// NOTE(theGiallo): characters is filled up by the characters selection screen, there's no need to clean it
				var cs = target_game_state.characters;
				for ( int i = 0; i != cs.Count; ++i )
				{
					cs[i].destroy();
				}
				//target_game_state.characters.Clear();
			}
			outOfGameCamera.gameObject.SetActive( true );
		}

		#region Unity
		#endregion Unity

		#region private
		private class Spawner
		{
			public Spawner( Map map )
			{
				spawnPointsPerTeam = new List<Spawn_Point>[map.spawnPointsPerTeam.Length];
				for ( int i = 0; i != spawnPointsPerTeam.Length; ++i )
				{
					spawnPointsPerTeam[i] = new List<Spawn_Point>();
					spawnPointsPerTeam[i].Add( map.spawnPointsPerTeam[i].arr );
				}
			}
			public Spawn_Point take_spawn_point( Game_State.Team team )
			{
				Spawn_Point ret;
				if ( ! take_from( team, out ret ) )
					take_from( Game_State.Team.Any, out ret );
				return ret;
			}
			private bool take_from( Game_State.Team team, out Spawn_Point sp )
			{
				bool ret = false;
				if ( spawnPointsPerTeam[(int)team].Count > 0 )
				{
					int c = spawnPointsPerTeam[(int)team].Count;
					int index = UnityEngine.Random.Range( 0, c );
					sp = spawnPointsPerTeam[(int)team][ index ];
					spawnPointsPerTeam[(int)team].RemoveAt( index );
					ret = true;
				} else
				{
					sp = null;
				}
				return ret;
			}
			private List<Spawn_Point>[] spawnPointsPerTeam;
		}
		#endregion private
	}
}
