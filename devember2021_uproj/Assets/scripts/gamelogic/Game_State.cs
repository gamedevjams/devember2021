using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Mathematics;
using static Unity.Mathematics.math;
using Sirenix.OdinInspector;
using DG.Tweening;
using tg;
using static tg.tg;

namespace devember2021
{
	public class Game_State : MonoBehaviour
	{
		public enum Team
		{
			Any,
			None,
			A,
			B
		}
		[Serializable]
		public struct Character_ID : IEquatable<Character_ID>
		{
			[ReadOnly]
			public int id;

			#region csharp
			public override bool Equals( object obj )
			{
				return obj is Character_ID iD && Equals( iD );
			}

			public bool Equals( Character_ID other )
			{
				return id == other.id;
			}

			public override int GetHashCode()
			{
				return HashCode.Combine( id );
			}

			public static bool operator ==( Character_ID left, Character_ID right )
			{
				return left.Equals( right );
			}

			public static bool operator !=( Character_ID left, Character_ID right )
			{
				return !( left == right );
			}
			#endregion csharp
		}
		public class
		Character
		{
			public Game_Character_SO             character_so     ;
			public Team                          team             ;
			public int                           gamepad_device_id;
			public Character_ID                  character_id = new Character_ID(){ id = -1 };
			public string name => character_so.name;

			public GameObject                    character_go                {get; private set;}
			public Transform                     character_tr                {get; private set;}
			public Character_Movement_Controller character_mov_controller    {get; private set;}
			public Character_Aim_Controller      character_aim_controller    {get; private set;}
			public Character_Fire_Controller     character_fire_controller   {get; private set;}
			public Gamepad_Character_Commander   gamepad_character_commander {get; private set;}
			public Gamepad_Picker_Commander      gamepad_picker_commander    {get; private set;}
			public Damageable                    damageable                  {get; private set;}
			public Pushable                      pushable                    {get; private set;}
			public Game_State_Provider           game_state_provider         {get; private set;}
			public Game_State                    game_state                  {get; private set;}
			public float3                        spawn_position              {get; private set;}

			public
			void
			instantiate( Game_State game_state, Transform parent, float3 w_pos, Camera camera )
			{
				character_go = Instantiate( character_so.characterPrefab );

				// TODO(theGiallo, 2021-12-16): add check for gameState.playing to all gameplay scripts

				character_mov_controller    = character_go.GetComponent<Character_Movement_Controller>();
				character_aim_controller    = character_go.GetComponent<Character_Aim_Controller     >();
				character_fire_controller   = character_go.GetComponent<Character_Fire_Controller    >();
				gamepad_character_commander = character_go.GetComponent<Gamepad_Character_Commander  >();
				gamepad_picker_commander    = character_go.GetComponent<Gamepad_Picker_Commander     >();
				damageable                  = character_go.GetComponent<Damageable                   >();
				pushable                    = character_go.GetComponent<Pushable                     >();
				game_state_provider         = character_go.GetComponentInChildren<Game_State_Provider>();
				this.game_state = game_state;
				game_state_provider.set( game_state );

				gamepad_character_commander.controllerID = gamepad_device_id;
				gamepad_picker_commander   .controllerID = gamepad_device_id;
				gamepad_character_commander.camera       = camera;


				character_mov_controller    .character_id = character_id;
				character_aim_controller    .character_id = character_id;
				character_fire_controller   .character_id = character_id;
				gamepad_character_commander .character_id = character_id;
				gamepad_picker_commander    .character_id = character_id;

				character_tr = character_go.transform;
				character_tr.SetParent( parent, worldPositionStays: false );
				character_tr.position = spawn_position = w_pos;
			}

			public
			void
			respawn()
			{
				if ( ! character_go.activeSelf )
				{
					character_go.SetActive( true );
				}
				damageable               .reset();
				character_mov_controller .reset();
				character_aim_controller .reset();
				character_fire_controller.reset();
				character_tr.position = spawn_position;
			}

			public
			void
			disable()
			{
				character_go.SetActive( false );
				//damageable               .enabled = false;
				//character_mov_controller .enabled = false;
				//character_aim_controller .enabled = false;
				//character_fire_controller.enabled = false;
			}

			public
			void
			destroy()
			{
				if ( character_go != null )
				{
					Destroy( character_go );
				}

				character_go                = null;
				character_tr                = null;
				character_mov_controller    = null;
				gamepad_character_commander = null;
				game_state_provider         = null;
				character_aim_controller    = null;
				character_fire_controller   = null;
				gamepad_picker_commander    = null;
				damageable                  = null;
				pushable                    = null;
				game_state_provider         = null;
				game_state                  = null;
				spawn_position              = float3(0,0,0);


			}

			public
			void
			clear()
			{
				character_so                = null;
				team                        = Team.None;
				gamepad_device_id           = -1;

				destroy();
			}
		}

		public Map             currentMap { get; private set; }
		public Match_Settings_SO  matchSettings;
		public List<Character> characters { get; private set; } = new List<Character>();
		public Character character( Character_ID id ) => id.id >= 0 && id.id < characters.Count ? characters[id.id] : null;

		public Projectile_Managers   projectile_managers;
		public Sound_System          soundSystem;
		public Physic_Materials_SFXs physicMaterialsSFXs;
		public FSMApp                appFSM;
		[NonSerialized,ShowInInspector,HideInEditorMode,ReadOnly] public I_In_Game_UI  inGameUI;
		[NonSerialized,ShowInInspector,HideInEditorMode,ReadOnly] public I_End_Game_UI endGameUI;

		[ShowInInspector,HideInEditorMode] public bool restarting  { get; private set; }
		[ShowInInspector,HideInEditorMode] public bool playing     { get; private set; }
		[ShowInInspector,HideInEditorMode] public bool paused      { get; private set; }
		[ShowInInspector,HideInEditorMode] public bool in_end_game { get; private set; }
		[ShowInInspector,HideInEditorMode] public bool can_update => playing && ! paused;

		public event Action         onStart;
		public event Action         onPaused;
		public event Action         onUnpaused;
		public event Action         onEndGame;
		public event Action<Action> onExit
		{
			add    => on_exit.add   ( value );
			remove => on_exit.remove( value );
		}
		public event Action onExitDone
		{
			add    => on_exit.onDone += value;
			remove => on_exit.onDone -= value;
		}
		private Waiting_Event on_exit = new Waiting_Event();

		public event Action<Action> onRestart
		{
			add    => on_restart.add   ( value );
			remove => on_restart.remove( value );
		}
		public event Action onRestartDone
		{
			add    => on_restart.onDone += value;
			remove => on_restart.onDone -= value;
		}
		private Waiting_Event on_restart = new Waiting_Event();

		public float gameTime => game_time;

		public
		void
		start()
		{
			if ( ! playing && ! restarting )
			{
				playing   = true;
				paused    = false;
				in_end_game = false;
				Physics.autoSimulation = true;

				game_time = 0;

				match_settings_instance = Instantiate( matchSettings );
				match_settings_instance.setup( this );

				onStart?.Invoke();
			}
		}

		public
		void
		restart()
		{
			playing     = false;
			paused      = false;
			in_end_game = false;
			Physics.autoSimulation = false;

			restarting = true;

			on_restart.onDone += handle_restart_done;
			on_restart?.invoke();
		}

		public
		void
		pause()
		{
			if ( playing && ! paused && ! in_end_game )
			{
				paused = true;
				Physics.autoSimulation = false;
				onPaused?.Invoke();
			}
		}
		public
		void
		unpause()
		{
			if ( playing && paused )
			{
				paused = false;
				Physics.autoSimulation = true;
				onUnpaused?.Invoke();
			}
		}
		public
		void
		exit()
		{
			if ( playing || in_end_game )
			{
				playing     = false;
				paused      = false;
				in_end_game = false;
				Physics.autoSimulation = true;

				on_exit.invoke();
			}
		}
		public
		void
		end_game()
		{
			if ( playing && ! in_end_game )
			{
				if ( paused )
				{
					unpause();
				}
				Physics.autoSimulation = false;

				playing = false;
				in_end_game = true;

				onEndGame?.Invoke();
			}
		}

		public
		void
		set_map( Map map )
		{
			currentMap = map;
			if ( map != null )
			{
				map.gameStateProvider  .set( this );
				map.soundSystemProvider.set( soundSystem );
			}
		}
		public
		void
		set_characters( IEnumerable<Character> characters )
		{
			this.characters.Clear();
			this.characters.Add( characters );
			for ( int i = 0; i != this.characters.Count; ++i )
			{
				this.characters[i].character_id.id = i;
			}
		}

		#region Unity
		private
		void
		Awake()
		{
			on_exit.onDone += handle_on_exit_done;
		}
		private
		void
		Update()
		{
			if ( ! can_update )
			{
				return;
			}
			game_time += Time.deltaTime;

			match_settings_instance.update();
		}
		#endregion Unity

		#region private
		[ShowInInspector,ReadOnly,HideInEditorMode] private Match_Settings_SO  match_settings_instance;
		[ShowInInspector,ReadOnly,HideInEditorMode] private float game_time;

		private
		void
		handle_on_exit_done()
		{
			if ( match_settings_instance != null )
			{
				Destroy( match_settings_instance );
			}
		}
		private
		void
		handle_restart_done()
		{
			on_restart.onDone -= handle_restart_done;
			restarting = false;
			start();
		}
		#endregion private
	}
}
