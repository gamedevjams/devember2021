using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Mathematics;
using static Unity.Mathematics.math;
using Sirenix.OdinInspector;
using DG.Tweening;
using tg;
using static tg.tg;

namespace devember2021
{
	[CreateAssetMenu(fileName = "New_Burst_SO_Magnum", menuName = "ScriptableObjects/tg/Burst/Magnum", order = 1)]
	public class Burst_SO_Magnum : Burst_SO
	{
		public bursts.Burst_Magnum magnumBurst;
		public override Fireable.Burst burst => magnumBurst;

		#region Unity
		#endregion Unity

		#region private
		#endregion private
	}
	namespace bursts
	{
		[Serializable]
		public class Burst_Magnum : Fireable.Burst
		{
			public Projectile_Manager_SO projectileManagerSO;
			[Min(0)]
			public float postPauseSecs;

			public override void init( Game_State game_state, string shootSFX, Game_State.Character_ID character_id )
			{
				projectile_manager = game_state.projectile_managers.get_manager( projectileManagerSO );
				sound_system       = game_state.soundSystem;
				this.shootSFX      = shootSFX;
				this.character_id  = character_id;
			}

			protected override IEnumerator<Time_Eatable> burst()
			{
				log( "Magnum burst: spawn" );
				yield return spawn( projectile_manager, fireable_interpolator );
				log( "Magnum burst: wait" );
				yield return wait( postPauseSecs, Interrupt.If_Abort );
				log( "Magnum burst: done" );
			}

			private Projectile.Manager projectile_manager;
		}
	}
}
