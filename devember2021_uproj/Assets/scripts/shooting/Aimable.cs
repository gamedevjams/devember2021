using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Mathematics;
using static Unity.Mathematics.math;
using Sirenix.OdinInspector;
using DG.Tweening;
using tg;
using static tg.tg;

namespace devember2021
{
	public class Aimable : MonoBehaviour
	{
		public Transform          rootTransform;
		public BoxCollider        boxCollider;
		public Collisions_Tracker collisionTracker;

		#region Unity
		#endregion Unity

		#region private
		#endregion private
	}
}
