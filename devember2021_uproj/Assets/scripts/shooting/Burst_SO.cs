using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Mathematics;
using static Unity.Mathematics.math;
using Sirenix.OdinInspector;
using DG.Tweening;
using tg;
using static tg.tg;

namespace devember2021
{
	//[CreateAssetMenu(fileName = "New_Burst_SO", menuName = "ScriptableObjects/tg/Burst", order = 1)]
	public class Burst_SO : ScriptableObject
	{
		public virtual Fireable.Burst burst { get; }
		public void init( Game_State game_state, string shootSFX, Game_State.Character_ID character_id )
		{
			burst.init( game_state, shootSFX, character_id );
		}

		#region Unity
		#endregion Unity

		#region private
		#endregion private
	}
}
