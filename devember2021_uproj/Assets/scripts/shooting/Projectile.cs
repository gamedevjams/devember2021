using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Mathematics;
using static Unity.Mathematics.math;
using Sirenix.OdinInspector;
using DG.Tweening;
using tg;
using static tg.tg;

namespace devember2021
{

	using static Fireable;

	public class Projectile : MonoBehaviour
	{
		public class Manager
		{
			public PoolPrefab<Projectile> pool;
			public List<Projectile> active_projectiles     = new List<Projectile>();
			public List<int       > active_projectiles_ids = new List<int       >();
			public int next_id = 0;
			public Sound_System          soundSystem;
			public Physic_Materials_SFXs physicMaterialsSFXs;
			public float projectileRadius => pool?.prefab.radius ?? 0f;

			public Manager( Projectile prefab )
			{
				pool = new PoolPrefab<Projectile>( prefab );
			}

			public
			Projectile
			spawn( Fireable_Point fp, float seconds_to_be_simulated, float3 root_velocity, Game_State.Character_ID character_id )
			{
				Projectile ret = pool.take();
				ret.manager = this;
				ret.reset();

				int id = ret.id = next_id++;
				log( $"spawn projectile {id}" );
				#if UNITY_EDITOR
				ret.name = $"{pool.prefab.name} {id}";
				#endif

				Transform tr = ret.tr = ret.transform;
				ret.record_pos( fp.pos, Time.fixedTime - seconds_to_be_simulated );
				tr.position = fp.pos;
				tr.forward  = fp.fw;
				ret.curr_velocity = ret.startVelocity + dot( root_velocity, fp.fw );

				ret.character_id = character_id;

				// NOTE(theGiallo): trying to avoid collision with weapon. I ended up moving the fire point to 5cm away from the weapon bounding box, but I'll leave this here.
				ret.pre_seconds_to_be_simulated = seconds_to_be_simulated + Time.fixedDeltaTime;

				active_projectiles    .Add( ret );
				active_projectiles_ids.Add( id  );

				return ret;
			}
			public
			void
			despawn_all()
			{
				for ( int i = active_projectiles.Count - 1; i >= 0; --i )
				{
					despawn( i );
				}
			}
			public
			void
			despawn( Projectile p )
			{
				int index = find_index( p );
				if ( index >= 0 )
				{
					debug_assert( active_projectiles[index].id == p.id );
				}
				despawn( index );
			}
			public
			void
			despawn( int index )
			{
				if ( index >= 0 )
				{
					if ( index >= active_projectiles.Count )
					{
						log_err( $"projectile despawn {index} index >= active_projectiles.Count {active_projectiles.Count}" );
						return;
					}
					var p = active_projectiles[index];
					debug_assert( active_projectiles[index].id == p.id );
					debug_assert( active_projectiles[index].id == active_projectiles_ids[index] );
					active_projectiles    .RemoveAt( index );
					active_projectiles_ids.RemoveAt( index );
					pool.put_back( p );
				}
			}
			public
			int
			find_index( Projectile p )
			{
				int ret = -1;
				int id = p.id;
				for ( int i = 0; i != active_projectiles_ids.Count; ++i )
				{
					if ( active_projectiles_ids[i] == id )
					{
						ret = i;
						break;
					}
				}
				return ret;
			}
			public
			void
			simulate( float dt = 0 )
			{
				for ( int pi = 0; pi < active_projectiles.Count; ++pi )
				{
					Projectile p = active_projectiles[pi];
					float s = p.pre_seconds_to_be_simulated + dt;

					// TODO(theGiallo, 2021-12-22): use integrator to compute acceleration
					// TODO(theGiallo, 2021-12-22): simulate at steps of predefined distance and do sphere cast tests

					Transform tr = p.tr;
					float3 start_pos = tr.position;
					float3 end_pos   = start_pos;
					float3 dir = tr.forward;
					bool despawn = false;
					float dist = p.curr_velocity * s;
					float original_dist = dist;
					float time_fraction_secs = 0;

					// TODO(theGiallo, 2022-12-27): avoid collision with gun while running in the shooting direction

					for ( int iters = 0; iters < 10; ++iters )
					{
						int collisions_count;
						if ( p.radius > 0 )
						{
							collisions_count = Physics.SphereCastNonAlloc( start_pos, p.radius, dir, spherecast_results, maxDistance: dist, p.layerMask, QueryTriggerInteraction.Collide );
						} else
						{
							collisions_count = Physics.RaycastNonAlloc( start_pos, dir, spherecast_results, maxDistance: dist, p.layerMask, QueryTriggerInteraction.Collide );
						}

						if ( collisions_count > 0 )
						{
							int first_ic = -1;
							for ( int ic = 0; ic != collisions_count; ++ic )
							{
								var rh = spherecast_results[ic];
								if ( rh.distance <= dist )
								{
									time_fraction_secs += ( s - time_fraction_secs ) * ( rh.distance / dist );
									dist = rh.distance;
									first_ic = ic;
								}
							}

							var frh = spherecast_results[first_ic];
							var rb = frh.collider.attachedRigidbody;
							Component component = rb != null ? (Component)rb : (Component)frh.collider;
							Pushable   pushable   = component.GetComponent<Pushable  >();
							Damageable damageable = component.GetComponent<Damageable>();

							if ( pushable == null && damageable == null && rb == null )
							{
								pushable   = component.GetComponentInParent<Pushable  >();
								damageable = component.GetComponentInParent<Damageable>();
							}

							if ( damageable != null )
							{
								damageable.damage( p.curr_damage );
							}

							end_pos = start_pos + dist * dir;
							p.record_pos( end_pos, Time.fixedTime - ( s - time_fraction_secs ) );

							if ( pushable != null )
							{
								pushable.push( dir: dir, force_module: p.curr_pushForce, pos: end_pos, normal: frh.normal );
							} else
							if ( rb != null )
							{
								rb.AddForceAtPosition( dir * p.curr_pushForce, position: end_pos, mode: ForceMode.VelocityChange );
							}

							PhysicMaterial pm = frh.collider.sharedMaterial;

							play_sound( p, pm, end_pos, time_fraction_secs );

							if ( material_is_deflector( pm ) )
							{
								start_pos = end_pos;
								dir = Vector3.Reflect( dir, frh.normal ).normalized;
								p.tr.rotation = Quaternion.LookRotation( dir, p.tr.up );
								dist = ( original_dist - dist ) * p.reflectionSpeedDampening;
								original_dist = dist;
								p.deflected( pm );
								if ( p.curr_velocity < 5 )
								{
									despawn = true;
									log( $"projectile deflected by {frh.collider.name} {component.name} too slow, despawning" );
								} else
								{
									log( $"projectile deflected by {frh.collider.name} {component.name}" );
									continue;
								}
							} else
							{
								log( $"projectile stopped by {frh.collider.name} {component.name}" );
								despawn = true;
							}

							break;
						} else
						{
							time_fraction_secs = s;
							end_pos = start_pos + dist * dir;
							p.record_pos( end_pos, Time.fixedTime - ( s - time_fraction_secs ) );
							break;
						}
					}

					if ( despawn )
					{
						indexes_to_despawn.Add( pi );
					} else
					{
						tr.position = end_pos;
						p.pre_seconds_to_be_simulated = 0;
					}
				}

				for ( int pi = indexes_to_despawn.Count - 1; pi >= 0; --pi )
				{
					despawn( indexes_to_despawn[pi] );
				}
				indexes_to_despawn.Clear();
			}

			public
			bool
			material_is_deflector( PhysicMaterial material )
			{
				bool ret = material != null && material.name.StartsWith( "deflector" );
				return ret;
			}

			public
			void
			play_sound( Projectile p, PhysicMaterial pm, float3 w_pos, float delay_secs )
			{
				string sfx_name = physicMaterialsSFXs.SFX( pm );
				if ( ! string.IsNullOrWhiteSpace( sfx_name ) )
				{
					soundSystem.playSFX( sfx_name, w_pos, skip_secs: 0, delay_secs: delay_secs ).autoPutBackInPool = true;
				}
			}

			private RaycastHit[] spherecast_results = new RaycastHit[1024];
			private List<int> indexes_to_despawn = new List<int>(1024);

			private
			static
			void
			log( string msg )
			{
				tg.tg.log( $"[Proj.Mgr] {msg}" );
			}
		}
		public struct ToSpawn
		{
			public Manager manager;
			public float   t;
		}

		public Manager manager;
		public LineRenderer trail;
		[MinValue(0)]
		public float trailTime;
		[MinValue(0)]
		public float trailWidth = 0.05f;

		[NonSerialized] public Transform tr;
		public int id { get; private set; }
		[NonSerialized] public float pre_seconds_to_be_simulated;
		[MinValue(0.1f)]
		public float startVelocity = 10f;
		[NonSerialized, ShowInInspector] public float curr_velocity;
		//public float3 gravity;
		[MinValue(0),Tooltip("Zero means raycast instead of spherecast")]
		public float radius = 0.02f;
		[ReadOnly] public int layerMask;
		[Range(0,1)] public float reflectionSpeedDampening;
		public float pushForce;
		public float curr_pushForce => _curr_push_force;
		public Damageable.Damage damage;
		public  Game_State.Character_ID character_id
		{
			get => _character_id;
			set
			{
				_character_id             = value;
				_curr_damage.character_id = value;
			}
		}
		[ShowInInspector,ReadOnly,HideInEditorMode]
		private Game_State.Character_ID _character_id;

		#region Unity
		private
		void
		Awake()
		{
			layerMask = ComputeLayerMaskOfLayer( gameObject.layer );
		}
		private
		void
		OnEnable()
		{
			//log( $"projectile {id:D3} enable" );
			reset();
			start_time = Time.fixedTime;
		}
		private
		void
		OnDisable()
		{
			//log( $"projectile {id:D3} disable" );
			reset();
		}
		#endregion Unity

		#region private
		public  Damageable.Damage curr_damage => _curr_damage;
		private Damageable.Damage _curr_damage;
		private float _curr_push_force;
		private List<Vector3> trail_positions = new List<Vector3>();
		private List<float  > trail_times     = new List<float  >();
		private Vector3[] trail_positions_array = new Vector3[65536];
		private float start_time;

		private
		void
		reset()
		{
			_curr_damage     = damage;
			_curr_push_force = pushForce;
			trail.SetPositions( new Vector3[2]{Vector3.zero, Vector3.zero} );
			trail_positions.Clear();
			trail_times    .Clear();
		}

		private
		void
		record_pos( float3 w_pos, float time = 0 )
		{
			if ( time == 0 )
			{
				time = Time.fixedTime;
			}
			time = time - start_time;

			//log( $"projectile {id:D3} record pos {w_pos} {time}" );

			int remove_up_to = -1;
			for ( int i = 0; i != trail_times.Count; ++i )
			{
				if ( time - trail_times[i] <= trailTime )
				{
					break;
				}
				remove_up_to = i;
			}
			if ( remove_up_to >= 0 )
			{
				//log( $"projectile {id:D3} remove up to {remove_up_to}" );
				trail_times.RemoveRange( 0, remove_up_to + 1 );
			}

			trail_positions.Add( w_pos );
			trail_times    .Add( time  );
			float duration = time - trail_times[0];
			float start_width = trailWidth;
			float end_width   = ( 1 - duration / trailTime ) * start_width;
			if ( trail != null )
			{
				//log( $"projectils {id:D3} {trail_positions.Count} trail positions" );
				// NOTE(theGiallo): startWidth is the oldest point, endWidth is this point
				// for some reason it doesn't work if we se start < end ... so we invert the array
				const bool invertWidth = false;
				if ( invertWidth )
				{
					#pragma warning disable CS0162 // Unreachable code detected
					trail.startWidth      = end_width;
					trail.endWidth        = start_width;
					#pragma warning restore CS0162 // Unreachable code detected
				} else
				{
					trail.startWidth      = start_width;
					trail.endWidth        = end_width;
				}

				trail.widthMultiplier = 1;
				trail.positionCount = trail_positions.Count;

				for ( int i = trail_positions.Count - 1, pi = 0; i >= 0 && pi < trail_positions_array.Length; --i, ++pi )
				{
					trail_positions_array[pi] = trail_positions[i];
				}

				trail.SetPositions( trail_positions_array );
			}
		}
		private
		void
		deflected( PhysicMaterial m )
		{
			_curr_damage.damage *= reflectionSpeedDampening;
			curr_velocity       *= reflectionSpeedDampening;
			_curr_push_force    *= reflectionSpeedDampening;
		}
		#endregion private
	}
}
