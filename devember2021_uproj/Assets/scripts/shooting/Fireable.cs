using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Mathematics;
using static Unity.Mathematics.math;
using Sirenix.OdinInspector;
using DG.Tweening;
using tg;
using static tg.tg;


namespace devember2021
{
	public class Fireable : MonoBehaviour
	{
		public Game_State_Provider gameStateProvider;
		[Tooltip("Forward is fire direction")]
		public Transform firePoint;
		public Burst_SO burstSO;
		private Burst_SO burst_SO_instanced;

		public string shootSFX;

		public event Action onTriggerOn;
		public event Action onTriggerOff;

		public struct Fireable_Point
		{
			public float3 pos, fw;

			public override string ToString() => $"(pos:{pos} fw:{fw})";
		}
		public class
		Fireable_Interpolator
		{
			Fireable_Point prev;
			Fireable_Point curr;
			Quaternion     tot_rot;
			public Fireable_Interpolator()
			{
				prev    = default;
				curr    = default;
				tot_rot = default;
			}
			public Fireable_Interpolator( Fireable_Point prev, Fireable_Point curr )
			{
				init( prev, curr );
			}
			public void init( Fireable_Point prev, Fireable_Point curr )
			{
				this.prev = prev;
				this.curr = curr;
				tot_rot = Quaternion.FromToRotation( prev.fw, curr.fw );
			}
			public void copy_from( Fireable_Interpolator fi )
			{
				prev    = fi.prev;
				curr    = fi.curr;
				tot_rot = fi.tot_rot;
			}
			public void interpolate( float t, ref Fireable_Point fp )
			{
				Quaternion partial_rot = Quaternion.SlerpUnclamped( Quaternion.identity, tot_rot, t );
				float3 partial_pos = lerp( prev.pos, curr.pos, t );
				float3 partial_fw  = partial_rot * prev.fw;
				fp.pos = partial_pos;
				fp.fw  = partial_fw;
			}
		}
		public class
		Root_Velocity
		{
			public float3 vel;
		}

		public
		void
		reset()
		{
			burst.abort();
		}

		public
		void
		set_root_velocity( float3 root_velocity )
		{
			this.root_velocity = root_velocity;
		}

		public
		void
		set_character_id( Game_State.Character_ID character_id )
		{
			this.character_id = character_id;
		}

		public
		bool
		is_trigger_on() => trigger.on;
		public
		void
		set_trigger( bool on )
		{
			if ( trigger.on == on )
			{
				return;
			}

			trigger.on = on;

			if ( on )
			{
				trigger.just_on = true;
				onTriggerOn?.Invoke();
			} else
			{
				trigger.just_off = true;
				onTriggerOff?.Invoke();
			}
		}

		#region Unity
		private
		void
		Awake()
		{
			gameStateProvider.onSet += handle_new_game_state;
			handle_new_game_state( gameStateProvider.get() );
		}
		private
		void
		Start()
		{
			register_prev_fire_point();
		}
		private
		void
		OnDestroy()
		{
			gameStateProvider.onSet += handle_new_game_state;
		}
		private
		void
		FixedUpdate()
		{
			if ( game_state == null || ! game_state.can_update )
			{
				return;
			}

			fire( Time.fixedDeltaTime, root_velocity );
			root_velocity = float3(0,0,0);

			register_prev_fire_point();

			trigger.reset_fronts();
		}
		#endregion Unity

		#region private
		private Game_State   game_state;
		private Sound_System sound_system;
		private Fireable_Point prev_fp;
		private Fireable_Point curr_fp;
		private Fireable_Interpolator fi = new Fireable_Interpolator( new Fireable_Point(){pos=float3(0,0,0), fw=float3(0,0,1)}, new Fireable_Point(){pos=float3(0,0,0), fw=float3(0,0,1)} );
		private Trigger trigger;
		private Burst burst = null;
		private bool have_burst;
		private float3 root_velocity;
		private Game_State.Character_ID character_id;

		public struct Trigger
		{
			public bool on;
			public bool off => ! on;
			public bool just_on;
			public bool just_off;

			public void reset_fronts()
			{
				just_on = just_off = false;
			}

			public override string ToString() => $"(on:{on} just on:{just_on} just off:{just_off})";
		}
		[Serializable]
		public abstract class Burst
		{
			public
			abstract
			void
			init( Game_State game_state, string shootSFX, Game_State.Character_ID character_id );

			[Min(-1)]
			public int loop;
			public int loops_done { get; protected set; }
			protected string                  shootSFX;
			protected Sound_System            sound_system;
			protected Game_State.Character_ID character_id;

			//private List<Projectile.ToSpawn> current_projectiles = new List<Projectile.ToSpawn>();
			private Time_Eater current_burst = null;
			protected Fireable_Interpolator fireable_interpolator = new Fireable_Interpolator();
			protected Root_Velocity root_velocity = new Root_Velocity();
			//private Fireable_Point amidst_fp = new Fireable_Point();
			private bool _abort;

			[Flags]
			public enum Interrupt
			{
				Never          = 0,
				If_Trigger_Off = 1 << 0,
				If_Abort       = 1 << 1,
			}

			public class Time_Eatable : IEnumerator<Time_Eatable>
			{
				public float  step_secs_to_eat; // NOTE(theGiallo): this step has a total of these secs
				public float  step_offset_secs => step_secs_to_eat - secs_to_eat; // NOTE(theGiallo): before this these secs have been eaten
				public float  secs_to_eat;
				public float  eaten_secs { get; protected set; }
				public bool   ended { get; protected set; }

				public Interrupt interrupt;
				public struct In_Data
				{
					public In_Data( Trigger trigger, bool abort )
					{
						this.trigger   = trigger  ;
						this.abort     = abort    ;
					}
					public Trigger   trigger;
					public bool      abort;

					public override string ToString() => $"{trigger} abort:{abort}";
				}
				public In_Data in_data;
				protected bool has_to_abort()
				{
					bool ret = ( has_flags( interrupt, Interrupt.If_Abort       ) && in_data.abort       )
					        || ( has_flags( interrupt, Interrupt.If_Trigger_Off ) && in_data.trigger.off );
					return ret;
				}

				public Time_Eatable() { }
				public Time_Eatable( Time_Eatable other )
				{
					this.step_secs_to_eat = other.step_secs_to_eat;
					this.secs_to_eat      = other.secs_to_eat     ;
					this.eaten_secs       = other.eaten_secs      ;
					this.ended            = other.ended           ;
					this.interrupt        = other.interrupt       ;
					this.in_data          = other.in_data         ;
				}

				public Time_Eatable Current => this;
				object IEnumerator.Current => this;

				public virtual bool MoveNext()
				{
					log( $"Time_Eatable MoveNext ended = {ended}" );
					if ( has_to_abort() )
					{
						ended = true;
					}

					ended = true;
					return ! ended;
				}
				public virtual void Reset()
				{
					log( $"Time_Eatable Reset" );
					step_secs_to_eat = 0;
					secs_to_eat = 0;
					eaten_secs = 0;
					ended = false;
				}

				public void Dispose()
				{
					log( $"Time_Eatable Dispose" );
					//throw new NotImplementedException();
				}

				public virtual Time_Eatable clone()
				{
					Time_Eatable ret = new Time_Eatable( this );
					return ret;
				}
			}
			public class Time_Eater : Time_Eatable
			{
				public Time_Eater()
				{
					log( "Time_Eater constructor" );
					ended = true;
					eatable = null;
				}
				public Time_Eater( IEnumerator<Time_Eatable> eatable )
				{
					init( eatable );
				}
				public void init( IEnumerator<Time_Eatable> eatable )
				{
					ended = false;
					this.eatable = eatable;
					push( eatable );
				}
				public Time_Eater( Time_Eater other ) : base( other )
				{
					init( other.eatable );
				}
				public override Time_Eatable clone() => new Time_Eater( this );
				public void Eat( float secs, In_Data in_data )
				{
					//log( $"Time_Eater Eat( {secs}, {in_data} )" );
					secs_to_eat = step_secs_to_eat = secs;
					this.in_data = in_data;
					MoveNext();
				}
				public override bool MoveNext()
				{
					bool ret = false;

					//log( $"Time_Eater MoveNext ended = {ended}" );

					if ( ended )
					{
						return ret;
					}

					if ( stack.Count == 0 )
					{
						ended = true;
						return ret;
					}

					float last_secs_to_eat = secs_to_eat;
					while ( last_secs_to_eat > 0 && ! ended )
					{
						IEnumerator<Time_Eatable> curr;
						bool do_break = false;
						while ( ( curr = peek() ) == null )
						{
							log( $"peek null count {stack.Count}" );
							pop();
							if ( stack.Count == 0 )
							{
								ended = true;
								ret = false;
								do_break = true;
								break;
							}
						}

						if ( do_break ) break;

						var cc = curr.Current;

						// NOTE(theGiallo): curr == curr.Current iff curr is Time_Eadible
						bool cc_is_time_edible = cc != null && cc == curr;
						if ( cc_is_time_edible )
						{
							cc.secs_to_eat      = last_secs_to_eat;
							cc.step_secs_to_eat = step_secs_to_eat;
							cc.in_data          = in_data;
						}

						bool res = curr.MoveNext();

						if ( cc_is_time_edible )
						{
							float eaten_secs = last_secs_to_eat - cc.secs_to_eat;
							last_secs_to_eat -= eaten_secs;
							this.eaten_secs += eaten_secs;
						}

						cc = curr.Current;

						if ( res && cc != null && cc != curr && ! cc.ended )
						{
							push( cc );
						}

						if ( ! res )
						{
							pop();
						}
					}

					secs_to_eat      = last_secs_to_eat;
					ret = ! ended;
					// TODO(theGiallo, 2021-12-24): value ended
					//secs_to_eat      = cc.secs_to_eat     ;
					//step_secs_to_eat = cc.step_secs_to_eat;
					//eaten_secs       = cc.eaten_secs;
					//ended            = cc.ended;

					return ret;
				}
				public override void Reset()
				{
					log( "Time_Eater Reset" );
					base.Reset();
					try {
					eatable?.Reset();
					} catch ( NotSupportedException e )
					{
						log_err( $"eatable.Reset() gave {e}" );
					}

					// TODO(theGiallo, 2021-12-24): do we have to reset all the stack?
					stack.Clear();

					push( eatable );
				}

				private IEnumerator<Time_Eatable> eatable;
				private List<IEnumerator<Time_Eatable>> stack = new List<IEnumerator<Time_Eatable>>();

				private void push( IEnumerator<Time_Eatable> te ) => stack.Add( te );
				private IEnumerator<Time_Eatable> peek() => stack.Count > 0 ? stack[ stack.Count - 1 ] : null;
				private IEnumerator<Time_Eatable> pop()
				{
					IEnumerator<Time_Eatable> ret = null;
					if ( stack.Count > 0 )
					{
						int i = stack.Count - 1 ;
						ret = stack[i];
						stack.RemoveAt( i );
					}
					return ret;
				}
			}

			// TODO(theGiallo, 2021-12-23): use pools for Time_Eatable functions
			// TODO(theGiallo, 2021-12-23): measure if it's faster to use To_Spawn list, instead of Manager.spawn

			public class Parallel : Time_Eatable
			{
				private IEnumerator<Time_Eatable>[] sequences;
				private Time_Eater[]                sequences_eaters;
				public Parallel( Interrupt interrupt, params IEnumerator<Time_Eatable>[] sequences )
				{
					init( interrupt, sequences );
					log( "parallel constructor" );
				}
				private void init( Interrupt interrupt, params IEnumerator<Time_Eatable>[] sequences )
				{
					this.sequences = sequences;
					sequences_eaters = new Time_Eater[sequences.Length];
					for ( int i = 0; i != sequences.Length; ++i )
					{
						sequences_eaters[i] = new Time_Eater( sequences[i] );
					}
					this.interrupt = interrupt;
				}

				public Parallel( Parallel other ) : base( other )
				{
					init( other.interrupt, other.sequences );
				}
				public override Time_Eatable clone() => new Parallel( this );
				public override bool MoveNext()
				{
					log( $"parallel MoveNext ended = {ended}" );
					if ( has_to_abort() )
					{
						ended = true;
					}

					if ( ended )
					{
						return false;
					}

					int count = 0;
					float start_secs_to_eat = secs_to_eat;
					for ( int i = 0; i != sequences_eaters.Length; ++i )
					{
						var se = sequences_eaters[i];
						se.step_secs_to_eat = step_secs_to_eat;
						se.secs_to_eat      = start_secs_to_eat;
						se.in_data          = in_data;
						if ( se.MoveNext() )
						{
							++count;
						}
						secs_to_eat = Mathf.Min( secs_to_eat, se.secs_to_eat );
					}

					float just_eaten_secs = start_secs_to_eat - secs_to_eat;
					eaten_secs += just_eaten_secs;

					bool ret = count > 0;
					ended = ! ret;

					log( $"parallel MoveNext ended = {ended} just_eaten_secs:{just_eaten_secs} eaten_secs:{eaten_secs} secs_to_eat:{secs_to_eat}" );

					return ret;
				}
				public override void Reset()
				{
					log( "Parallel Reset" );
					base.Reset();

					for ( int i = 0; i != sequences.Length; ++i )
					{
						sequences[i].Current?.Reset();
						sequences_eaters[i].Reset();
					}
				}
			}
			public Time_Eatable parallel( Interrupt interrupt, params Time_Eatable[] sequences ) => new Parallel( interrupt, sequences );
			public class Wait : Time_Eatable
			{
				protected float secs_to_wait;
				public Wait( float secs, Interrupt interrupt )
				{
					init( secs, interrupt );
					log( $"wait constructor" );
				}
				private void init( float secs, Interrupt interrupt )
				{
					secs_to_wait   = secs;
					this.interrupt = interrupt;
				}
				public Wait( Wait other ) : base( other )
				{
					init( other.secs_to_eat, other.interrupt );
				}
				public override bool MoveNext()
				{
					log( $"wait MoveNext ended = {ended} secs_to_eat:{secs_to_eat}" );
					if ( has_to_abort() )
					{
						ended = true;
					}

					if ( ended )
					{
						return false;
					}
					float start_secs_to_eat = secs_to_eat;

					float remaining_secs_to_wait = secs_to_wait - eaten_secs - secs_to_eat;
					ended = remaining_secs_to_wait <= 0;
					secs_to_eat = ended ? -remaining_secs_to_wait : 0;

					float just_eaten_secs = start_secs_to_eat - secs_to_eat;
					eaten_secs += just_eaten_secs;

					log( $"wait MoveNext ended = {ended} just_eaten_secs:{just_eaten_secs} eaten_secs:{eaten_secs} secs_to_wait:{secs_to_wait} secs_to_eat:{secs_to_eat}" );

					return ! ended;
				}
			}
			public Time_Eatable wait( float secs, Interrupt interrupt ) => new Wait( secs, interrupt );
			public Time_Eatable wait_and( float secs, Time_Eatable te, Interrupt interrupt ) => new Time_Eater( _wait_and( secs, te, interrupt ) );
			IEnumerator<Time_Eatable> _wait_and( float secs, Time_Eatable te, Interrupt interrupt )
			{
				log( $"wait and {secs} wait" );
				yield return new Wait( secs, interrupt );
				log( $"wait and {secs} te" );
				yield return te;
				log( $"wait and {secs} done" );
			}

			public class Spawn : Time_Eatable
			{
				Projectile.Manager projectiles_manager;
				string             shootSFX;
				Sound_System       sound_system;

				private Fireable_Interpolator   fireable_interpolator;
				private Fireable_Point          amidst_fp;
				private Root_Velocity           root_velocity;
				private Game_State.Character_ID character_id;

				public Projectile spawned_projectile { get; protected set; }

				public Spawn( Projectile.Manager projectiles_manager, Fireable_Interpolator fireable_interpolator, Root_Velocity root_velocity, string shootSFX, Sound_System sound_system, Game_State.Character_ID character_id )
				{
					this.projectiles_manager   = projectiles_manager;
					this.fireable_interpolator = fireable_interpolator;
					this.shootSFX              = shootSFX;
					this.sound_system          = sound_system;
					this.root_velocity         = root_velocity;
					this.character_id          = character_id;
					log( "spawn constructor" );
				}
				public Spawn( Spawn other ) : base( other )
				{
					projectiles_manager   = other.projectiles_manager;
					fireable_interpolator = other.fireable_interpolator;
					shootSFX              = other.shootSFX;
					sound_system          = other.sound_system;
					root_velocity         = other.root_velocity;
					character_id          = other.character_id;
				}
				public override Time_Eatable clone() => new Spawn( this );
				public override bool MoveNext()
				{
					log( $"spawn MoveNext ended = {ended}" );
					if ( has_to_abort() )
					{
						ended = true;
					}

					if ( ! ended )
					{
						ended = true;

						float secs_to_simulate = secs_to_eat;//step_offset_secs;
						float t = secs_to_simulate / step_secs_to_eat;

						fireable_interpolator.interpolate( t, ref amidst_fp );

						// NOTE(theGiallo): preventing projectile to collide with weapon
						amidst_fp.pos += ( projectiles_manager.projectileRadius + 0.01f ) * amidst_fp.fw;

						log( $"spawn MoveNext spawning {amidst_fp} sim:{secs_to_simulate}s t:{t} step:{step_secs_to_eat}s fixedTime:{Time.fixedTime:0000.000000}s" );

						spawned_projectile = projectiles_manager.spawn( amidst_fp, secs_to_simulate, root_velocity.vel, character_id );

						sound_system.playSFX( shootSFX, amidst_fp.pos, skip_secs:0, delay_secs: step_offset_secs ).autoPutBackInPool = true;
					}
					return false;
				}
				public override void Reset()
				{
					log( "Parallel Reset" );
					base.Reset();
					spawned_projectile = null;
				}
			}
			public Time_Eatable spawn( Projectile.Manager projectiles_manager, Fireable_Interpolator fireable_interpolator )
			   => new Spawn( projectiles_manager, fireable_interpolator, root_velocity, shootSFX, sound_system, character_id );
			public Time_Eatable frequency( Time_Eatable e, float Hz, float period_offset = 0f, float duration = -1f, int repetitions = -1, Interrupt interrupt = Interrupt.If_Abort )
			{
				return new Time_Eater( _frequency( e, Hz, period_offset, duration, repetitions, interrupt ) );
			}
			private IEnumerator<Time_Eatable> _frequency( Time_Eatable e, float Hz, float period_offset = 0f, float duration = -1f, int repetitions = -1, Interrupt interrupt = Interrupt.If_Abort )
			{
				float period = 1.0f / Hz;
				float offset_secs = period * period_offset;
				if ( repetitions != -1 )
				{
					duration = offset_secs + period * repetitions;
				} else
				if ( duration != -1 )
				{
					repetitions = 1 + Mathf.FloorToInt( ( duration - offset_secs ) / period );
				}

				float remainder_secs = duration - offset_secs - repetitions * period;

				log( $"frequency offset wait" );
				yield return wait( offset_secs, interrupt );

				Time_Eatable[] parallels = new Time_Eatable[repetitions];
				for ( int i = 0; i != repetitions; ++i )
				{
					parallels[i] = wait_and( period * i, e.clone(), interrupt );
				}
				log( $"frequency parallel" );
				yield return parallel( interrupt, parallels );
				log( $"frequency remainder wait" );

				yield return wait( remainder_secs, interrupt );
				log( $"frequency done" );
			}

			protected virtual IEnumerator<Time_Eatable> burst(){yield return wait(0,Interrupt.If_Abort|Interrupt.If_Trigger_Off);}

			#if UNITY_EDITOR
			protected IEnumerator<Time_Eatable> burst_example()
			{
				Projectile.Manager small_manager = null;
				Projectile.Manager big_manager = null;
				yield return parallel( Interrupt.If_Abort,
				                       frequency( spawn( big_manager  , fireable_interpolator ), Hz: 5, period_offset: 0.5f, duration: 1.1f, interrupt: Interrupt.If_Trigger_Off ),
				                       frequency( spawn( small_manager, fireable_interpolator ), Hz:12, repetitions: 3, interrupt: Interrupt.If_Abort ) );
				yield return wait( 0.2f, Interrupt.If_Abort | Interrupt.If_Trigger_Off );
			}
			#endif

			public void abort()
			{
				// mandatory stop, e.g. if weapon expodes
				_abort = true;
			}
			public void fire( float secs, Fireable_Interpolator fi, float3 root_velocity, Trigger trigger )
			{
				this.root_velocity.vel = root_velocity;

				if ( trigger.just_on )
				{
					_abort = false;
				}
				if ( trigger.on && current_burst == null )
				{
					current_burst = new Time_Eater( burst() );
				}
				if ( trigger.just_on && current_burst.ended )
				{
					current_burst.init( burst() );
					loops_done = 0;
				}

				fireable_interpolator.copy_from( fi );

				if ( current_burst != null )
				{
					current_burst.Eat( secs, new Time_Eatable.In_Data( trigger, _abort ) );

					if ( current_burst.ended && loop != loops_done )
					{
						current_burst.Reset();
						++loops_done;
					}
				}
			}
		}

		private
		void
		fire( float secs, float3 root_velocity )
		{
			curr_fp.pos = firePoint.position;
			curr_fp.fw  = firePoint.forward;
			fi.init( prev_fp, curr_fp );

			if ( have_burst )
			{
				burst.fire( secs, fi, root_velocity, trigger );
			}
		}
		private
		void
		register_prev_fire_point()
		{
			prev_fp.pos = firePoint.position;
			prev_fp.fw  = firePoint.forward;
		}

		private
		void
		handle_new_game_state( Game_State gs )
		{
			game_state = gs;
			if ( gs != null )
			{
				burst_SO_instanced = Instantiate( burstSO );
				burst_SO_instanced.init( gs, shootSFX, character_id );
				burst = burst_SO_instanced.burst;
				have_burst = true;
			}
		}

		private static
		void
		log( string msg )
		{
			//tg.tg.log( msg );
		}
		#endregion private
	}
}
