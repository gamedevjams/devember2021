using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Mathematics;
using static Unity.Mathematics.math;
using Sirenix.OdinInspector;
using DG.Tweening;
using tg;
using static tg.tg;

namespace devember2021
{
	public class Projectile_Managers : MonoBehaviour
	{
		public Game_State gameState;
		public
		Projectile.Manager
		get_manager( Projectile_Manager_SO projectile_manager_so )
		{
			Projectile.Manager ret;
			if ( ! managers_dict.TryGetValue( projectile_manager_so.name, out Projectile_Manager_SO instanced ) )
			{
				instanced = ScriptableObject.Instantiate( projectile_manager_so );
				instanced.name = projectile_manager_so.name;
				managers.Add( instanced.manager );
				instanced.manager.pool.holder_tr.SetParent( tr );
				instanced.manager.soundSystem         = gameState.soundSystem;
				instanced.manager.physicMaterialsSFXs = gameState.physicMaterialsSFXs;
			}
			ret = instanced.manager;
			return ret;
		}

		#region Unity
		private
		void
		Awake()
		{
			tr = transform;
			gameState.onExit    += handle_exit;
			gameState.onRestart += handle_exit;
		}
		private
		void
		FixedUpdate()
		{
			if ( ! gameState.can_update )
			{
				return;
			}

			for ( int i = 0; i != managers.Count; ++i )
			{
				managers[i].simulate( Time.fixedDeltaTime );
			}
		}
		private void OnDestroy()
		{
			foreach ( var m in managers_dict.Values )
			{
				Destroy( m );
			}
			managers_dict.Clear();
			managers     .Clear();
		}
		#endregion Unity

		#region private
		private Dictionary<string,Projectile_Manager_SO> managers_dict = new Dictionary<string, Projectile_Manager_SO>();
		private List<Projectile.Manager> managers = new List<Projectile.Manager>();
		private Transform tr;

		private
		void
		handle_exit( Action done )
		{
			for ( int i = 0; i != managers.Count; ++i )
			{
				managers[i].despawn_all();
			}

			done();
		}
		#endregion private
	}
}
