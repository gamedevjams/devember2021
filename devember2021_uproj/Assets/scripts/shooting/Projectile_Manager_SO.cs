using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Mathematics;
using static Unity.Mathematics.math;
using Sirenix.OdinInspector;
using DG.Tweening;
using tg;
using static tg.tg;

namespace devember2021
{
	[CreateAssetMenu(fileName = "New_Projectile_Manager_SO", menuName = "ScriptableObjects/tg/Projectile Manager", order = 1)]
	public class Projectile_Manager_SO : ScriptableObject
	{
		public Projectile prefab;
		public Projectile.Manager manager;

		#region Unity
		private
		void
		Awake()
		{
			#if UNITY_EDITOR
			if ( Application.isPlaying )
			#endif
			manager = new Projectile.Manager( prefab );
		}
		#endregion Unity

		#region private
		#endregion private
	}
}
