using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Mathematics;
using static Unity.Mathematics.math;
using Sirenix.OdinInspector;
using DG.Tweening;
using tg;
using static tg.tg;

namespace tg
{
	[ExecuteAlways]
	public class MaterialPropertyBlockApplier : MonoBehaviour
	{
		public int materialIndex;

		public bool doUVTiling;
		[ShowIf("doUVTiling")]
		public float2 UVTiling;

		public bool doUVOffset;
		[ShowIf("doUVOffset")]
		public float2 UVOffset;


		/*
			NOTE(theGiallo): https://docs.unity3d.com/Manual/SL-PropertiesInPrograms.html

			_MainTex_ST:
			x contains X tiling value
			y contains Y tiling value
			z contains X offset value
			w contains Y offset value
		*/

		#region Unity
		void
		Start()
		{
			uv_scale_offset_id = Shader.PropertyToID( "_MainTex_ST" );

			mpb = new MaterialPropertyBlock();
			r = GetComponent<Renderer>();

			apply();
		}

		#if UNITY_EDITOR
		private
		void
		OnWillRenderObject()
		{
			try {
			Start();
			} catch ( Exception e )
			{
				log_err( $"Exception while applying MPB: {e}" );
			}
		}
		#endif
		#endregion Unity

		#region private
		Renderer r;
		MaterialPropertyBlock mpb;
		int uv_scale_offset_id;

		private
		void
		apply()
		{
			if ( r == null )
			{
				log_err( "renderer is null" );
				return;
			}

			if ( doUVTiling && doUVOffset )
			{
				mpb.SetVector( uv_scale_offset_id, float4(UVTiling,UVOffset) );
			} else
			if ( doUVTiling || doUVOffset )
			{
				Material m;
				m = r.sharedMaterials[materialIndex];

				if ( m == null )
				{
					log_err( $"could not find material with index {materialIndex}" );
					return;
				}

				float4 scale_offset = m.GetVector( uv_scale_offset_id );
				float2 materialUVTiling = scale_offset.xy;
				float2 materialUVOffset = scale_offset.zw;
				float2 _UVTiling = doUVTiling ? UVTiling : materialUVTiling;
				float2 _UVOffset = doUVOffset ? UVOffset : materialUVOffset;

				mpb.SetVector( uv_scale_offset_id, float4(materialUVTiling, materialUVOffset) );
				mpb.SetVector( uv_scale_offset_id, float4(_UVTiling, _UVOffset) );
			}

			r.SetPropertyBlock( mpb, materialIndex );
		}
		#endregion private
	}
}
