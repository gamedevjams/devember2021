using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Mathematics;
using static Unity.Mathematics.math;
using Sirenix.OdinInspector;
using DG.Tweening;
using tg;
using static tg.tg;

namespace devember2021
{
	public interface
	I_End_Game_UI
	{
		void set_match_settings( Match_Settings_SO match_settings_SO );
		void enable( bool enabled );
		GameObject  gameObject { get; }
		CanvasGroup canvasGroup { get; }
	}
}
