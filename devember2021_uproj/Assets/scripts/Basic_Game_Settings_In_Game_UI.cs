using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Mathematics;
using static Unity.Mathematics.math;
using Sirenix.OdinInspector;
using DG.Tweening;
using tg;
using static tg.tg;
using TMPro;

namespace devember2021
{
	public class Basic_Game_Settings_In_Game_UI : MonoBehaviour, I_In_Game_UI
	{
		public TMP_Text     timeText;
		public GameObject   teamGO;
		[SerializeField] private CanvasGroup _canvasGroup;

		public GameObject[] charactersGOs             = new GameObject[4];
		public TMP_Text[]   teamScoresTexts           = new TMP_Text[2];
		public TMP_Text[]   teamLossVictoryTexts      = new TMP_Text[2];
		public TMP_Text[]   characterScoresTexts      = new TMP_Text[4];
		public TMP_Text[]   characterLossVictoryTexts = new TMP_Text[4];
		public TMP_Text[]   characterNamesTexts       = new TMP_Text[4];

		public
		void
		set_match_settings( Match_Settings_SO match_settings )
		{
			this.match_settings = match_settings;

			teamGO.SetActive( match_settings.teamBased );

			var cs = match_settings.game_state.characters;
			for ( int i = 0; i != charactersGOs.Length; ++i )
			{
				charactersGOs[i].SetActive( i < cs.Count );
			}
			for ( int i = 0; i != cs.Count; ++i )
			{
				characterNamesTexts[i].text = cs[i].name;
			}

			timeText.enabled = ! match_settings.duration.infinite;
		}
		GameObject I_In_Game_UI.gameObject => gameObject;
		public CanvasGroup canvasGroup => _canvasGroup;

		#region Unity
		private
		void
		Update()
		{
			var css  = match_settings.characters_scores;
			var cslv = match_settings.characters_loss_victory;
			for ( int i = 0; i != css.Length; ++i )
			{
				characterScoresTexts     [i].text = $"{css[i]}";
				characterLossVictoryTexts[i].text = loss_victory_str( cslv[i] );
			}

			var tss = match_settings.teams_scores;
			var tlv = match_settings.teams_loss_victory;
			for ( int i = 0; i != tss.Length; ++i )
			{
				teamScoresTexts     [i].text = $"{tss[i]}";
				teamLossVictoryTexts[i].text = loss_victory_str( tlv[i] );
			}

			if ( ! match_settings.duration.infinite )
			{
				tmp_duration.totalSeconds = Mathf.Min( match_settings.duration.totalSeconds, match_settings.game_state.gameTime );
				string past_hh = $"{tmp_duration.hours:D2}h";
				string past_mm = $"{tmp_duration.mins :D2}m";
				string past_ss = $"{tmp_duration.secs :D2}s";
				tmp_duration.totalSeconds = Mathf.Max( 0, match_settings.duration.totalSeconds - match_settings.game_state.gameTime );
				string rem_hh = $"{tmp_duration.hours:D2}h";
				string rem_mm = $"{tmp_duration.mins :D2}m";
				string rem_ss = $"{tmp_duration.secs :D2}s";
				timeText.text = $"{past_hh} {past_mm} {past_ss} - {rem_hh} {rem_mm} {rem_ss}";
			}
		}
		#endregion Unity

		#region private
		private Match_Settings_SO match_settings;
		private Match_Settings_SO.Duration tmp_duration = new Match_Settings_SO.Duration();

		private
		string
		loss_victory_str( Match_Settings_SO.Loss_Victory lv )
		{
			string ret = "";
			switch ( lv )
			{
				case Match_Settings_SO.Loss_Victory.None:
					ret = "";
					break;
				case Match_Settings_SO.Loss_Victory.Loss:
					ret = "Lost";
					break;
				case Match_Settings_SO.Loss_Victory.Victory:
					ret = "Won";
					break;
			}
			return ret;
		}
		#endregion private
	}
}
