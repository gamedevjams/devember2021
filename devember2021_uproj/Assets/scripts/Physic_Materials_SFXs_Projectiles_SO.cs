using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using tg;
using static tg.tg;
using Sirenix.OdinInspector;
using Unity.Mathematics;
using static Unity.Mathematics.math;

namespace devember2021
{
	[CreateAssetMenu(fileName = "New_Physic_Materials_SFXs_Projectiles_SO", menuName = "ScriptableObjects/tg/sound/Physic Material SFXs Projectiles", order = 1)]
	public class Physic_Materials_SFXs_Projectiles_SO : ScriptableObject
	{
		[Serializable]
		public class Physic_Materials_SFX
		{
			public PhysicMaterial[] physicMaterials;
			public string SFX;
		}

		public Physic_Materials_SFX[] physicMaterialsSFXs;

		public
		string
		SFX( PhysicMaterial pm )
		{
			string ret = "";
			if ( pm != null )
			{
				dict.TryGetValue( pm, out ret );
				//if ( string.IsNullOrWhiteSpace( ret ) )
				//{
				//	log( $"no SFX found for {pm.name}" );
				//}
			}
			return ret;
		}

		public
		void
		init()
		{
			dict = new Dictionary<PhysicMaterial, string>();
			for ( int i = 0; i != physicMaterialsSFXs.Length; ++i )
			{
				var m = physicMaterialsSFXs[i];
				for (int j = 0; j != m.physicMaterials.Length; ++j)
				{
					dict.Add(m.physicMaterials[j], m.SFX );
				}
			}
		}

		private
		void
		Awake()
		{
			init();
		}

		private Dictionary<PhysicMaterial, string> dict;
	}
}
