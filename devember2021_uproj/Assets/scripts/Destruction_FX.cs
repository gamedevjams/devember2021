using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Mathematics;
using static Unity.Mathematics.math;
using Sirenix.OdinInspector;
using DG.Tweening;
using tg;
using static tg.tg;

namespace devember2021
{
	[DisallowMultipleComponent]
	public class Destruction_FX : MonoBehaviour
	{
		public Destructible destructible;
		public Play_SFX playSFX;

		#region Unity
		private
		void
		Awake()
		{
			destructible.on_destructed += handle_destruction;
		}
		#endregion Unity

		#region private
		private
		void
		handle_destruction()
		{
			playSFX.play();
		}
		#endregion private
	}
}
