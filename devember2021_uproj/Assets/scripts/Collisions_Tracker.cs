using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Mathematics;
using static Unity.Mathematics.math;
using Sirenix.OdinInspector;
using DG.Tweening;
using tg;
using static tg.tg;

namespace devember2021
{
	public class Collisions_Tracker : MonoBehaviour
	{
		public List<ContactPoint> contactPointsFixedUpdate = new List<ContactPoint>( 64 );

		#region Unity
		//private int c = 0;
		private
		void
		OnCollisionEnter( Collision collision_info )
		{
			//log( $"{name} [{c:D4}] enter {collision_info.gameObject.name}" );
			var cs = collision_info.contacts;
			contactPointsFixedUpdate.Add( cs );
		}

		private
		void
		OnCollisionStay( Collision collision_info )
		{
			//log( $"{name} [{c:D4}] stay {collision_info.gameObject.name}" );
			var cs = collision_info.contacts;
			contactPointsFixedUpdate.Add( cs );
		}
		private
		void
		FixedUpdate()
		{
			//log( $"{name} [{c:D4}] clearing {contactPointsFixedUpdate.Count}" );
			//++c;
			contactPointsFixedUpdate.Clear();
		}
		#endregion Unity

		#region private
		#endregion private
	}
}
