using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Mathematics;
using static Unity.Mathematics.math;
using Sirenix.OdinInspector;
using DG.Tweening;
using tg;
using static tg.tg;

namespace devember2021
{
	public class MapExteriorsSetupper : MonoBehaviour
	{
		public BoxCollider[] exteriorColliders = new BoxCollider[4];
		[MinValue(0)] public float heightBelow  = 128;
		[MinValue(0)] public float heightAbove  = 1024;
		[MinValue(0)] public float depth        = 128;

		#if UNITY_EDITOR
		[Header("Editor")]
		[ShowInInspector] private Transform setup_center;
		[ShowInInspector] private float2    setup_size;
		[Button("Setup")]
		private void setup() => setup( setup_center.position, setup_size );
		#endif

		public
		void
		setup( float3 center_world_space, float2 size )
		{
			float total_height = heightAbove + heightBelow;
			float3 hsize = float3( ( size.x + depth ) / 2, total_height / 2 - heightBelow, ( size.y + depth ) / 2 );

			for ( int i = 0; i != 4; ++i )
			{
				float3 side_signs = float3(0,1,0);
				float X = 0, Z = 0;
				switch ( i )
				{
					case 0: side_signs = float3(  1, 1,  0 ); X = depth;  Z = size.y + depth * 2; break;
					case 1: side_signs = float3(  0, 1,  1 ); X = size.x; Z = depth;  break;
					case 2: side_signs = float3( -1, 1,  0 ); X = depth;  Z = size.y + depth * 2; break;
					case 3: side_signs = float3(  0, 1, -1 ); X = size.x; Z = depth;  break;
				}
				var c = exteriorColliders[i];
				c.size   = float3(X, total_height, Z);
				c.center = (float3)c.transform.InverseTransformPoint( center_world_space ) + hsize * side_signs;
			}
		}

		#region Unity
		#endregion Unity

		#region private
		#endregion private
	}
}
