using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Mathematics;
using static Unity.Mathematics.math;
using Sirenix.OdinInspector;
using DG.Tweening;
using tg;
using static tg.tg;

namespace devember2021
{
	public class Map : MonoBehaviour
	{
		public string mapName = "Unnamed Map";

		[ValidateInput("@gameCamera != null", "Camera is mandatory", InfoMessageType.Error)]
		public Camera                gameCamera;
		public Game_State_Provider   gameStateProvider;
		public Sound_System_Provider soundSystemProvider;

		[ReadOnly]
		public List<Spawn_Point> spawnPoints = new List<Spawn_Point>();
		[Serializable]
		public class
		Team_Spawn_Points
		{
			public Team_Spawn_Points( Game_State.Team name, Spawn_Point[] arr )
			{
				this.name = name;
				this.arr = arr;
			}
			readonly public Game_State.Team name;
			[LabelText("@name")]
			public Spawn_Point[] arr;
		}
		[ReadOnly]
		public Team_Spawn_Points[] spawnPointsPerTeam;

		#region Unity
		private
		void
		Awake()
		{
			gather_spawn_points();
		}

		#if UNITY_EDITOR
		private void OnValidate()
		{
			gather_spawn_points();
		}
		#endif
		#endregion Unity

		#region private
		[Button("Gather Spawn Points")]
		private
		void
		gather_spawn_points()
		{
			Spawn_Point[] sps = transform.GetComponentsInChildren<Spawn_Point>( includeInactive: false );

			spawnPoints.Clear();
			spawnPoints.Add( sps );

			int teams_count = Enum.GetValues(typeof(Game_State.Team)).Length;

			List<Spawn_Point>[] _spawnPointsPerTeam = new List<Spawn_Point>[teams_count];
			for ( int i = 0; i != _spawnPointsPerTeam.Length; ++i )
			{
				_spawnPointsPerTeam[i] = new List<Spawn_Point>();
			}

			if ( spawnPointsPerTeam == null || spawnPointsPerTeam.Length != teams_count )
			{
				spawnPointsPerTeam = new Team_Spawn_Points[teams_count];
			}

			for ( int i = 0; i != spawnPoints.Count; ++i )
			{
				var sp = spawnPoints[i];
				_spawnPointsPerTeam[(int)sp.team].Add( sp );
			}

			for ( int i = 0; i != spawnPointsPerTeam.Length; ++i )
			{
				spawnPointsPerTeam[i] = new Team_Spawn_Points((Game_State.Team)i, _spawnPointsPerTeam[i].ToArray() );
			}
		}
		#if UNITY_EDITOR
		[Button("Assign Providers")]
		private
		void
		editor_assign_providers()
		{
			Game_State_Provider  [] game_state_providers   = GetComponentsInChildren<Game_State_Provider  >( includeInactive: true );
			Sound_System_Provider[] sound_system_providers = GetComponentsInChildren<Sound_System_Provider>( includeInactive: true );

			for ( int i = 0; i != game_state_providers.Length; ++i )
			{
				if ( game_state_providers[i] != gameStateProvider )
				{
					game_state_providers[i].chainedProvider = gameStateProvider;
					log( $"assigned game state provider {game_state_providers[i].name}", game_state_providers[i].gameObject );
				}
			}

			for ( int i = 0; i != sound_system_providers.Length; ++i )
			{
				if ( sound_system_providers[i] != soundSystemProvider )
				{
					sound_system_providers[i].chainedProvider = soundSystemProvider;
					log( $"assigned sound system provider {sound_system_providers[i].name}", sound_system_providers[i].gameObject );
				}
			}
		}
		#endif
		#endregion private
	}
}
