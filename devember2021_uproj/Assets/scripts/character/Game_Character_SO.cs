using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Mathematics;
using static Unity.Mathematics.math;
using Sirenix.OdinInspector;
using DG.Tweening;
using tg;
using static tg.tg;

namespace devember2021
{
	[CreateAssetMenu(fileName = "New_Game_Character_SO", menuName = "ScriptableObjects/tg/Game Character", order = 1)]
	public class Game_Character_SO : ScriptableObject
	{
		public new string name;
		public Texture2D  thumbnail;
		public GameObject characterPrefab;

		#region Unity
		#endregion Unity

		#region private
		#endregion private
	}
}
