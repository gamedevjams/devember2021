using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Mathematics;
using static Unity.Mathematics.math;
using Sirenix.OdinInspector;
using DG.Tweening;
using tg;
using static tg.tg;

namespace devember2021
{
	public class Gamepad_Character_Commander : MonoBehaviour
	{
		[NonSerialized,ShowInInspector,HideInEditorMode] public int                     controllerID;
		[NonSerialized,ShowInInspector,HideInEditorMode] public Game_State.Character_ID character_id;

		public Game_State_Provider gameStateProvider;

		public Character_Movement_Controller characterMovementController;
		public Character_Aim_Controller      characterAimController;
		public Character_Fire_Controller     characterFireController;
		public new Camera camera;
		public AnimationCurve stickToVelPercentage;

		public Game_Input.Gamepad_Button fireButton;

		public float deadZoneMov = 0.1f;
		public float deadZoneAim = 0.1f;
		public float normalVel;
		public float sprintVel;

		public ZeroAimStickBehaviour zeroAimStickBehaviour;
		public float zeroAimRadius = 0.1f;
		public enum ZeroAimStickBehaviour
		{
			Keep_Aim_Direction_World,
			Forward_Direction
		}

		#region Unity
		private
		void
		Start()
		{
			camera_tr = camera.transform;
			prev_desired_aim_direction = transform.forward;

			gameStateProvider.onSet += handle_new_game_state;
			game_state = gameStateProvider.get();

			Update();
		}
		private
		void
		Update()
		{
			if ( ! game_state.can_update )
			{
				return;
			}
			var gamepad = Game_Input.instance.gamepads[controllerID];
			const int left_index  = 0;
			const int right_index = 1;
			float3 vel;
			float2 mov_stick = gamepad.sticks_value[left_index];
			float2 aim_stick = gamepad.sticks_value[right_index];

			float2 apply_dead_zone( float2 stick, float dead_zone )
			{
				float l = length( stick );
				if ( l == 0 )
				{
					return stick;
				}
				float new_l = max( 0, l - dead_zone ) / ( 1 - dead_zone );
				float2 ret = stick * ( new_l / l );
				return ret;
			}
			mov_stick = apply_dead_zone( mov_stick, deadZoneMov );
			aim_stick = apply_dead_zone( aim_stick, deadZoneAim );

			float3 mov_stick3d = stick_to_world( mov_stick );
			float3 aim_stick3d = stick_to_world( aim_stick );
			float2 mov_stick_game_plane = mov_stick3d.xz;
			float l = length( mov_stick );
			float vt = stickToVelPercentage.Evaluate( l );

			void
			run_amount()
			{
				float maxtr = 0.98f;
				float vel_pc = Mathf.Clamp( gamepad.triggers_value[left_index], 0, maxtr ) / maxtr;
				characterMovementController.currentControlsMaxVel = normalVel + vel_pc * ( sprintVel - normalVel );
			}

			void
			movement()
			{
				float max_vel = characterMovementController.currentControlsMaxVel;

				float2 vel2 = mov_stick_game_plane * ( l == 0 ? 1 : ( max_vel * vt / l ) );

				vel = float3( vel2.x, 0, vel2.y );

				characterMovementController.desiredVelocity = vel;
			}

			void
			aim()
			{
				float3 aim;
				float aim_l = length( aim_stick );
				if ( aim_l < zeroAimRadius )
				{
					switch ( zeroAimStickBehaviour )
					{
						case ZeroAimStickBehaviour.Keep_Aim_Direction_World:
							aim = prev_desired_aim_direction;
							break;
						case ZeroAimStickBehaviour.Forward_Direction:
						default:
							aim = transform.forward;
							break;
					}
				} else
				{
					aim = aim_stick3d;
					//log( $"aiming to {aim}" );
				}

				#if DEBUG
				if ( all( aim == float3(0,0,0) ) ) log_err( $"{Time.frameCount} aim is zero prev_desired_aim_direction: ({prev_desired_aim_direction.x:E}, {prev_desired_aim_direction.y:E}, {prev_desired_aim_direction.z:E})" );
				#endif

				prev_desired_aim_direction                 = aim;
				characterAimController.desiredAimDirection = aim;
			}

			void
			fire()
			{
				characterFireController.trigger_on = gamepad.buttons_active[(int)fireButton] == 1;
			}

			run_amount();
			movement();
			aim();
			fire();
		}
		#endregion Unity

		#region private
		private Transform  camera_tr;
		private float3     prev_desired_aim_direction;
		private Game_State game_state;

		private
		float3
		stick_to_world( float2 stick )
		{
			float3 ret;

			//float3 stick_camera_space_world = camera_tr.TransformVector( float3( stick.x, stick.y, 0 ) );
			float3 wp = transform.position;
			float3 sp = camera.WorldToScreenPoint( wp );
			sp.z = 0;
			float3 sp_dir_p = sp + float3( stick.x, stick.y, 0 );
			Ray dir_p_ray = camera.ScreenPointToRay( sp_dir_p );
			//float3 stick_dir_p_fw = dir_p_ray.direction;

			float3 plane_normal = transform.parent.up;
			float3 plane_x      = cross( plane_normal, camera_tr.forward );
			float3 plane_z      = cross( plane_x, plane_normal );

			Plane plane = new Plane( inNormal: plane_normal, inPoint: wp );
			bool intersect = plane.Raycast( dir_p_ray, out float t );
			if ( ! intersect )
			{
				ret = plane_x * stick.x + plane_z * stick.y;
			} else
			{
				float3 dir_p_ws = dir_p_ray.GetPoint( t );
				float3 dir_ws = normalize( dir_p_ws - wp ) * length( stick );
				ret = dir_ws;
			}

			return ret;
		}

		private
		void
		handle_new_game_state( Game_State gs )
		{
			game_state = gs;
		}
		#endregion private
	}
}
