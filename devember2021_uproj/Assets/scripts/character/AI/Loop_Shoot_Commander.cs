using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Mathematics;
using static Unity.Mathematics.math;
using Sirenix.OdinInspector;
using DG.Tweening;
using tg;
using static tg.tg;

namespace devember2021
{
	public class
	Loop_Shoot_Commander : MonoBehaviour
	{
		public Character_Fire_Controller fire_controller;

		[Min(2f / 240f)]
		public float period;

		#region Unity
		#if UNITY_EDITOR
		private
		void
		OnValidate()
		{
			clamp_period();
		}
		#endif //UNITY_EDITOR

		private
		void
		Awake()
		{
			clamp_period();
			timer = new Timer( period );
			timer.use_fixed_time = true;
		}
		private
		void
		FixedUpdate()
		{
			#if UNITY_EDITOR
			clamp_period();
			#endif

			log( $"timer.active: {timer.active} timer.passed_time: {timer.passed_time()} timer.passed_percentage: {timer.passed_percentage()*100:00.00}%" );

			timer.active = timer.active && timer.passed_percentage() < 1;

			if ( ! timer.active )
			{
				trigger_on();

				timer.restart( period );
			} else
			{
				trigger_off();
			}
		}
		#endregion Unity

		#region private
		private Timer timer;

		private
		void
		clamp_period()
		{
			period = Mathf.Max( Time.fixedDeltaTime * 2, period );
		}

		private
		void
		trigger_on()
		{
			log( $"trigger_on" );
			fire_controller.trigger_on = true;
		}

		private
		void
		trigger_off()
		{
			log( $"trigger_off" );
			fire_controller.trigger_on = false;
		}
		private
		void
		log( string msg, UnityEngine.Object o = null )
		{
			tg.tg.log( $"[Loop Shoot Commander][{gameObject.name}] {msg}", o == null ? this : o );
		}
		#endregion private
	}
}
