using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Mathematics;
using static Unity.Mathematics.math;
using Sirenix.OdinInspector;
using DG.Tweening;
using tg;
using static tg.tg;

namespace devember2021
{
	public class Character_Movement_Controller : MonoBehaviour
	{
		[Serializable]
		public class Structure
		{
			public Rigidbody rb;
			public List<Collisions_Tracker> collisionsTrackers;
			public List<Pushable> pushables;
		}

		public Structure structure;

		public Game_State_Provider gameStateProvider;

		public float3 desiredVelocity;
		public float  maxVelocity = 12.0f;
		public float  maxAcceleration = 3;
		public float  maxAngularVelocityDegs = 180;

		public bool useCurveForMaxAngVel;
		public AnimationCurve maxAngularVelocityDegPerVelocity;

		public float currentControlsMaxVel = 6;
		public float maxGroundDegs = 30;
		public float groundedForgivenessMs;

		public bool doClampMaxVel;
		public bool doClampMaxAngVel;
		public bool doClampMaxAccModule;
		public bool doClampMaxAcc;
		public bool doRotateTowards;
		public bool doSlideCollisions;
		public bool doForceAngularVelToZero;
		public bool doKeepVerticalVel;
		public bool doKeepPhysEngineDeltaVel;
		public bool doCustomGravity;
		public bool doMoveAlongGround;
		public enum GroundNormalMode
		{
			MEAN, FLATTEST, STEEPEST
		}
		public GroundNormalMode groundNormalMode;
		public bool doDecayExternalVelocityChanges;
		[Range(0,1)]
		public float externalVelocityDecayFactor;

		public Game_State.Character_ID character_id;

		public
		void
		reset()
		{
			desiredVelocity      = float3(0,0,0);
			old_desired_velocity = float3(0,0,0);
		}

		#region Unity
		private
		void
		Awake()
		{
			tr = transform;

			gameStateProvider.onSet += handle_new_game_state;
			game_state = gameStateProvider.get();

			var ps = structure.pushables;
			for ( int i = 0; i != ps.Count; ++i )
			{
				ps[i].onPushed += handle_push;
			}
		}
		private
		void
		OnDestroy()
		{
			gameStateProvider.onSet -= handle_new_game_state;
		}

		#if UNITY_EDITOR
		private float3 vel_pre_colls;
		private float3 vel_post_colls_1;
		private float3 vel_post_colls_2;
		private float3 vel_post_colls_21;

		public bool    do_vel_colls1;
		public bool    do_vel_colls2;

		public Color   pre_colls_color;
		public Color   colls_1_color;
		public Color   colls_2_color;
		public Color   colls_21_color;
		public Color   assigned_vel_color;

		private
		void
		OnDrawGizmosSelected()
		{
			for ( int i = 0; i != structure.collisionsTrackers.Count; ++i )
			{
				var cps = structure.collisionsTrackers[i].contactPointsFixedUpdate;
				for ( int j = 0; j != cps.Count; ++j )
				{
					float length = 0.2f;
					Gizmos.DrawLine( cps[j].point, cps[j].point + cps[j].normal * length );
				}
			}

			var c = Gizmos.color;
			float3 p = transform.position;

			Gizmos.color = pre_colls_color;
			Gizmos.DrawLine( p, p + vel_pre_colls );

			Gizmos.color = colls_1_color;
			Gizmos.DrawLine( p, p + vel_post_colls_1 );

			Gizmos.color = colls_2_color;
			Gizmos.DrawLine( p, p + vel_post_colls_2 );

			Gizmos.color = colls_21_color;
			Gizmos.DrawLine( p, p + vel_post_colls_21 );

			Gizmos.color = assigned_vel_color;
			Gizmos.DrawLine( p, p + prev_step_vel );

			Gizmos.color = c;
		}
		#endif // UNITY_EDITOR

		private
		void
		FixedUpdate()
		{
			if ( ! game_state.can_update )
			{
				return;
			}

			g_up = normalize( - Physics.gravity );
			min_ground_cos = Mathf.Cos( Mathf.Deg2Rad * maxGroundDegs );

			grounded = false;
			int ground_normals_count = 0;
			float3 mean_ground_normal = float3(0,0,0);
			float3 flattest_ground_normal = float3(0,0,0);
			float3 steepest_ground_normal = float3(0,0,0);
			float  flattest_ground_normal_cos = 1;
			float  steepest_ground_normal_cos = 0;


			List<float3> collisions_normals = new List<float3>();
			for ( int i = 0; i != structure.collisionsTrackers.Count; ++i )
			{
				var cps = structure.collisionsTrackers[i].contactPointsFixedUpdate;
				collisions_normals.Reserve( cps.Count );
				for ( int j = 0; j != cps.Count; ++j )
				{
					float3 n = cps[j].normal;
					collisions_normals.Add( n );
					float c = dot( n, g_up );
					if ( c > min_ground_cos )
					{
						grounded = true;
						mean_ground_normal += n;
						++ground_normals_count;

						if ( c <= flattest_ground_normal_cos )
						{
							flattest_ground_normal_cos = c;
							flattest_ground_normal     = n;
						}

						if ( c >= steepest_ground_normal_cos )
						{
							steepest_ground_normal_cos = c;
							steepest_ground_normal     = n;
						}
					}
				}
			}

			if ( grounded )
			{
				not_grounded_fsteps_count = 0;
				grounded_fsteps_count     += 1;

				mean_ground_normal = normalize( mean_ground_normal / (float)ground_normals_count );
			}
			else
			{
				not_grounded_fsteps_count += 1;
				grounded_fsteps_count     = 0;

				mean_ground_normal     = last_mean_ground_normal    ;
				flattest_ground_normal = last_flattest_ground_normal;
				steepest_ground_normal = last_steepest_ground_normal;
			}

			grounded_or_forgiven = grounded || not_grounded_fsteps_count * Time.fixedDeltaTime <= groundedForgivenessMs * 1e-3f;


			#if false
			if ( collisions_normals.Count > 0 )
			{
				log( "--- --- --- --- --- --- --- --- --- ---" );
				for ( int i = 0; i != collisions_normals.Count; ++i )
				{
					log( $"{collisions_normals[i]}" );
				}
				log( "--- --- --- --- --- --- --- --- --- ---" );
			}
			#endif

			float3 rb_vel = structure.rb.velocity;
			//float  rb_vel_mag = length( rb_vel );
			//float3 rb_vel_dir = rb_vel / rb_vel_mag;

			float3 desired_velocity = desiredVelocity;

			if ( doMoveAlongGround )
			if ( grounded_or_forgiven && length( desired_velocity ) > 1e-4f )
			{
				float3 gn = mean_ground_normal;
				float  gc = 0;
				switch ( groundNormalMode )
				{
					case GroundNormalMode.MEAN    :
						gn =     mean_ground_normal;
						gc = dot( g_up, mean_ground_normal );
						break;
					case GroundNormalMode.FLATTEST:
						gn = flattest_ground_normal;
						gc = flattest_ground_normal_cos;
						break;
					case GroundNormalMode.STEEPEST:
						gn = steepest_ground_normal;
						gc = steepest_ground_normal_cos;
						break;
				}
				float3 left = cross( desired_velocity, gn );
				float3 fw   = cross( gn, left );

				bool going_down = dot( g_up, fw ) < 0;
				if ( going_down )
				{
					float v = length( desired_velocity );
					desired_velocity = normalize( fw ) * v;
				} else
				{
					float3 nfw = normalize( fw );
					desired_velocity = dot( nfw, desired_velocity ) * nfw;
				}
			}

			if ( doClampMaxVel )
			desired_velocity = clamp_max_vel( desired_velocity );

			if ( doClampMaxAngVel )
			desired_velocity = clamp_ang_vel( desired_velocity, old_desired_velocity );

			if ( doClampMaxAccModule )
			desired_velocity = clamp_acc_module( desired_velocity, old_desired_velocity );

			if ( doClampMaxAcc )
			desired_velocity = clamp_acc( desired_velocity, old_desired_velocity );

			float3 new_old_desired_velocity  = desired_velocity;

			if ( doRotateTowards )
			rotate_towards( desired_velocity );

			if ( ! grounded_or_forgiven )
			desired_velocity = rb_vel;

			desired_velocity += external_velocity_changes;

			if ( doKeepVerticalVel )
			desired_velocity = keep_vertical_vel( rb_vel, desired_velocity );

			if ( doKeepPhysEngineDeltaVel )
			desired_velocity = keep_phys_engine_delta_vel( prev_step_vel, rb_vel, desired_velocity );

			if ( doCustomGravity )
			desired_velocity = apply_custom_gravity( desired_velocity, Physics.gravity, Time.fixedDeltaTime );
			structure.rb.useGravity = ! doCustomGravity;

			if ( doSlideCollisions )
			desired_velocity = apply_collisions_to_velocity( desired_velocity, collisions_normals );

			if ( doForceAngularVelToZero )
			structure.rb.angularVelocity = float3(0,0,0);

			if ( float.IsNaN( desired_velocity.x )
			  || float.IsNaN( desired_velocity.y )
			  || float.IsNaN( desired_velocity.z ) )
			{
				log( $"desired velocity has nan members: {desired_velocity.x:E6}, {desired_velocity.y:E6}, {desired_velocity.z:E6}" );
				desired_velocity = prev_step_vel;
			}
			structure.rb.velocity = desired_velocity;
			log( $"assigned velocity: {desired_velocity.x:E6}, {desired_velocity.y:E6}, {desired_velocity.z:E6}" );
			prev_step_vel         = desired_velocity;
			old_desired_velocity  = new_old_desired_velocity;
			if ( doDecayExternalVelocityChanges )
			{
				external_velocity_changes *= externalVelocityDecayFactor;
			} else
			{
				external_velocity_changes = float3(0,0,0);
			}


			if ( grounded )
			{
				last_mean_ground_normal     = mean_ground_normal    ;
				last_flattest_ground_normal = flattest_ground_normal;
				last_steepest_ground_normal = steepest_ground_normal;
			}
		}
		#endregion Unity

		#region private
		private float3     old_desired_velocity = float3(0,0,0);
		private float3     prev_step_vel        = float3(0,0,0);
		private Transform  tr;
		private Game_State game_state;
		private float3     external_velocity_changes;
		private bool       grounded;
		private bool       grounded_or_forgiven;
		private int        not_grounded_fsteps_count;
		private int        grounded_fsteps_count;

		private float3 last_mean_ground_normal     = float3(0,1,0);
		private float3 last_flattest_ground_normal = float3(0,1,0);
		private float3 last_steepest_ground_normal = float3(0,1,0);

		private float3 g_up;
		private float min_ground_cos; // Mathf.Cos( Mathf.Deg2Rad * maxGroundDegs );

		private
		void
		handle_push( Pushable.Push push )
		{
			float3 dv = push.normal * ( dot( push.dir, push.normal ) * push.velocity_change );
			external_velocity_changes += dv;
		}

		private
		float3
		clamp_acc_module( float3 desired_vel, float3 curr_vel )
		{
			curr_vel.y = desired_vel.y;

			float dl = length( desired_vel );

			if ( dl == 0 ) return desired_vel;

			float vl = length( curr_vel );
			float requested_dv = max( 0, dl - vl );
			float max_dv = maxAcceleration * Time.fixedDeltaTime;
			//log( $"vl: {vl:##0.00000} dl: {dl:##0.00000} dv: {requested_dv:##0.00000} max_dv: {max_dv:##0.00000} too much: {requested_dv > max_dv}" );
			if ( requested_dv > max_dv )
			{
				desired_vel = desired_vel * ( ( vl + max_dv ) / dl );
			}
			return desired_vel;
		}
		private
		float3
		clamp_acc( float3 desired_vel, float3 curr_vel )
		// NOTE(theGiallo): this interacts with walls. Velocity is sliding so desired_velocity gets rotated towards rb velocity.
		{
			curr_vel.y = desired_vel.y;

			//float parallel_component = dot( rb_vel_dir, desired_velocity );
			float3 requested_dv = desired_vel - curr_vel;
			float requested_dv_mag = length( requested_dv );
			float requested_acc = requested_dv_mag / Time.fixedDeltaTime;
			float max_dv = maxAcceleration * Time.fixedDeltaTime;
			if ( requested_dv_mag > max_dv )
			{
				float3 clamped_dv = requested_dv * ( max_dv / requested_dv_mag );
				desired_vel = clamped_dv + curr_vel;
			}
			return desired_vel;
		}
		private
		float3
		clamp_max_vel( float3 vel )
		{
			float v = length( vel );
			if ( v > maxVelocity )
			{
				vel *= ( maxVelocity / v );
			}
			return vel;
		}

		private
		float3
		clamp_ang_vel( float3 vel, float3 curr_vel )
		{
			curr_vel.y = vel.y;

			float degs = Vector3.SignedAngle( curr_vel, vel, Vector3.up );
			float requested_ang_vel = degs / Time.fixedDeltaTime;
			float max_degs_per_fu = maxAngularVelocityDegs * Time.fixedDeltaTime;
			float maxangdeg = useCurveForMaxAngVel ? maxAngularVelocityDegPerVelocity.Evaluate( length( curr_vel ) ) : maxAngularVelocityDegs;
			if ( abs( requested_ang_vel ) > maxangdeg )
			{
				vel = Quaternion.AngleAxis( - sign( degs ) * ( abs( degs ) -max_degs_per_fu ), Vector3.up ) * vel;
				float new_degs = Vector3.SignedAngle( curr_vel, vel, Vector3.up );
				//log( $"abs( {requested_ang_vel:00000.000} ) > {maxangdeg:00.000} ( degs: {degs} max_degs_per_fu: {max_degs_per_fu} old vel: {length( curr_vel )} new degs:{new_degs})" );
			}
			return vel;
		}

		private
		float3
		apply_collisions_to_velocity( float3 v, List<float3> collision_normals )
		{
			float3 v_colls1  = v;
			float3 v_colls2  = v;
			float3 v_colls21 = v;

			for ( int i = 0; i != collision_normals.Count; ++i )
			{
				float3 n = collision_normals[i];

				float c = dot( n, g_up );
				bool is_ground = c > min_ground_cos;

				if ( ! is_ground )
				{
					v_colls1 = v_colls1 - n * min( 0, dot( v_colls1, n ) );
				}
			}

			{
				int ncount = 0;
				float3 normal = float3(0,0,0);
				for ( int i = 0; i != collision_normals.Count; ++i )
				{
					float3 n = collision_normals[i];
					float d = dot( v, n );
					if ( d < 0 )
					{
						normal += n;
						++ncount;
					}
				}
				if ( ncount > 0 )
				{
					normal /= (float)ncount;
					v_colls2 = v - normal * min( 0, dot( v, normal ) );
				}
			}

			{
				int ncount = 0;
				float3 normal = float3(0,0,0);
				for ( int i = collision_normals.Count - 1; i >= 0; --i )
				{
					float3 n = collision_normals[i];
					float d = dot( v, n );
					if ( d < 0 )
					{
						normal += n;
						++ncount;
					}
				}
				if ( ncount > 0 )
				{
					normal /= (float)ncount;
					v_colls21 = v - normal * min( 0, dot( v, normal ) );
				}
			}

			vel_pre_colls    = v;
			vel_post_colls_1 = v_colls1;
			vel_post_colls_2  = v_colls2;
			vel_post_colls_21 = v_colls21;

			if ( collision_normals.Count > 0 )
			{
				log( $"v pre col: {v.x:E}, {v.y:E}, {v.z:E}" );
				log( $"v colls 1 : {v_colls1 .x:E}, {v_colls1 .y:E}, {v_colls1 .z:E}" );
				log( $"v colls 2 : {v_colls2 .x:E}, {v_colls2 .y:E}, {v_colls2 .z:E}" );
				log( $"v colls 21: {v_colls21.x:E}, {v_colls21.y:E}, {v_colls21.z:E}" );
			}

			if ( do_vel_colls1 )
			{
				v = v_colls1;
			} else
			if ( do_vel_colls2 )
			{
				v = v_colls2;
			}

			return v;
		}

		private
		void
		rotate_towards( float3 vel )
		{
			if ( vel.x != 0 && vel.z != 0 )
			{
				tr.forward = normalize( float3( vel.x, 0, vel.z ) );
			}
		}

		private
		float3

		keep_vertical_vel( float3 rb_vel, float3 desired_velocity )
		{
			float3 ret = desired_velocity + float3( 0, rb_vel.y, 0 );
			return ret;
		}

		private
		float3
		keep_phys_engine_delta_vel( float3 prev_step_vel, float3 rb_vel, float3 desired_velocity )
		{
			float3 ret = ( rb_vel - prev_step_vel ) + desired_velocity;
			return ret;
		}
		private
		float3
		apply_custom_gravity( float3 vel, float3 acc, float dt )
		{
			float3 ret = vel + dt * acc;
			return ret;
		}

		private
		void
		handle_new_game_state( Game_State gs )
		{
			game_state = gs;
		}
		#endregion private
	}
}
