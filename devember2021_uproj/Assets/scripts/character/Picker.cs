using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Mathematics;
using static Unity.Mathematics.math;
using Sirenix.OdinInspector;
using DG.Tweening;
using tg;
using static tg.tg;

namespace devember2021
{
	public class Picker : MonoBehaviour
	{
		[Layer]
		public int         pickablesLayer;
		[Layer]
		public int         blockersLayer;
		public Transform   pickerPoint;
		[Min(0)]
		public float       maxDistance;

		public Character_Aim_Controller  aimController;
		public Character_Fire_Controller fireController;

		[SerializeField] Pickup picked_up;

		#if UNITY_EDITOR
		[Button("Pickup Picked Up")]
		private
		void
		editor_pickup_picked_up()
		{
			pickup( picked_up );
		}
		#endif

		public
		void
		pickup( Pickup pickup )
		{
			if ( picked_up != null )
			{
				drop();
			}

			if ( pickup.aimable != null && aimController != null )
			{
				aimController.add_aimable( pickup.aimable );
			}

			if ( pickup.fireable != null && fireController != null )
			{
				fireController.fireable = pickup.fireable;
			}

			pickup.pickable = false;

			picked_up = pickup;
		}

		public
		void
		drop()
		{
			if ( picked_up == null )
			{
				return;
			}

			if ( picked_up.aimable != null && aimController != null )
			{
				aimController.remove_aimable();
			}

			if ( picked_up.fireable != null && fireController != null )
			{
				fireController.fireable = null;
			}

			picked_up.pickable = true;
			picked_up = null;
		}

		public
		Pickup
		candidate_for_pickup()
		{
			Pickup ret;

			float3 picker_pos = pickerPoint.position;
			int layer_mask = 1 << pickablesLayer;

			int colliders_count = Physics.OverlapSphereNonAlloc( picker_pos, maxDistance, results, layer_mask, QueryTriggerInteraction.Collide );

			sort_results( colliders_count );

			ret = get_closest_non_occluded_pickup( colliders_count );

			return ret;
		}

		#region Unity
		private
		void
		Start()
		{
		}
		#endregion Unity

		#region private
		private const int MAX_COLLIDERS = 1024;
		private Collider[] results        = new Collider[MAX_COLLIDERS];
		private float3  [] closest_points = new float3  [MAX_COLLIDERS];
		private float   [] sq_distances   = new float   [MAX_COLLIDERS];
		private int     [] indexes        = new int     [MAX_COLLIDERS];

		private
		void
		sort_results( int results_count )
		{
			float3 picker_pos = pickerPoint.position;

			for ( int i = 0; i != results_count; ++i )
			{
				var c = results[i];
				float3 cp = c.ClosestPoint( picker_pos );
				float3 dv = cp - picker_pos;
				float d2 = dot( dv, dv );

				closest_points[i] = cp;
				sq_distances  [i] = d2;
				indexes       [i] = i;
			}

			Array.Sort( keys: sq_distances, items: indexes, index: 0, length: results_count );
		}
		private
		Pickup
		get_closest_non_occluded_pickup( int results_count )
		{
			Pickup ret = null;

			float3 picker_pos = pickerPoint.position;
			int layer_mask = 1 << blockersLayer;

			for ( int i = 0; i != results_count; ++i )
			{
				int index = indexes[i];
				var c = results[index];

				var rb = c.attachedRigidbody;
				Pickup pickup;
				if ( rb != null )
				{
					pickup = rb.GetComponent<Pickup>();
				} else
				{
					pickup = c.GetComponent<Pickup>();
				}
				bool valid = false;
				if ( pickup != null )
				{
					valid = pickup.pickable;
				}

				bool occluded = true;
				if ( valid )
				{
					float distance = sqrt( sq_distances[i] );
					float3 closest_point = closest_points[i];
					occluded = Physics.Raycast( origin: picker_pos, direction: normalize( picker_pos - closest_point ), layerMask: layer_mask, maxDistance: distance );
				}

				if ( valid && ! occluded )
				{
					ret = pickup;
					break;
				}
			}

			return ret;
		}
		#endregion private
	}
}
