using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Mathematics;
using static Unity.Mathematics.math;
using Sirenix.OdinInspector;
using DG.Tweening;
using tg;
using static tg.tg;

namespace devember2021
{
	public class Gamepad_Picker_Commander : MonoBehaviour
	{
		[NonSerialized,ShowInInspector,HideInEditorMode] public int controllerID;
		[NonSerialized,ShowInInspector,HideInEditorMode] public Game_State.Character_ID character_id;

		public Picker picker;
		public Game_Input.Gamepad_Button pickupButton;

		#region Unity
		private
		void
		Update()
		{
			var gamepad = Game_Input.instance.gamepads[controllerID];

			bool pressed = gamepad.buttons_active[(int)pickupButton] == 1;

			if ( ! pressed )
			{
				pression_used = false;
			}

			if ( pressed && ! pression_used )
			{
				Pickup pickup = picker.candidate_for_pickup();
				if ( pickup != null )
				{
					log( $"pickup {pickup.name}" );
					picker.pickup( pickup );
					pression_used = true;
				}
			}
		}
		#endregion Unity

		#region private
		private bool pression_used;
		#endregion private
	}
}
