using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Mathematics;
using static Unity.Mathematics.math;
using Sirenix.OdinInspector;
using DG.Tweening;
using tg;
using static tg.tg;

namespace devember2021
{
	public class Character_Aim_Controller : MonoBehaviour
	{
		#if UNITY_EDITOR
		[ShowInInspector] private bool drawGizmo;
		#endif

		public Game_State_Provider gameStateProvider;

		[Header( "Structure" )]
		public Transform   aimableContainerTr;
		public FixedJoint  joint;
		public Transform   worldRoot;

		[Header( "Aimable" )]
		public float3      aimablePosition = float3(0,0,0.5f);

		public bool hasAimable { get => _has_aimable; private set { _has_aimable = value; } }
		[SerializeField,ReadOnly,Space] private bool _has_aimable;

		[Space]

		public BoxCollider aimableBoxCollider;
		public Transform   aimableTr;
		[SerializeField] private Collisions_Tracker collisions_tracker;

		[Header( "Config" )]
		public float maxAngularVelocityDegs = 360 * 10;
		public bool  checkCollisions;

		[Header( "Control" )]
		public float3 desiredAimDirection;

		public Game_State.Character_ID character_id;

		public
		void
		add_aimable( Aimable aimable )
		{
			add_aimable( aimable.rootTransform, aimable.boxCollider, ref aimable.collisionTracker );
		}

		public
		void
		add_aimable( Transform tr, BoxCollider coll, ref Collisions_Tracker ct )
		{
			if ( hasAimable ) return;

			hasAimable = true;

			var rb = coll.attachedRigidbody;

			if ( ct == null )
			{
				if ( rb != null )
				{
					ct = rb.gameObject.AddComponent<Collisions_Tracker>();
				}
			}

			ct.enabled = true;

			aimableBoxCollider  = coll;
			aimableTr           = tr;

			aimableTr.SetParent( aimableContainerTr, worldPositionStays: false );
			aimableTr.localPosition = aimablePosition;
			aimableTr.localRotation = Quaternion.identity;
			//aimableTr.localScale    = float3(1,1,1);

			collisions_tracker  = ct;
			joint.connectedBody = rb;
		}
		public
		void
		remove_aimable()
		{
			if ( ! hasAimable )
			{
				return;
			}

			//Destroy( collisions_tracker );
			if ( collisions_tracker != null )
			{
				collisions_tracker.enabled = false;
			}

			if ( aimableTr != null )
			{
				Transform tr = aimableBoxCollider.transform;
				float3 p = tr.TransformPoint( aimableBoxCollider.center );
				float3 s = tr.TransformVector( aimableBoxCollider.size / 2 );
				float3 dir = Vector3.down;
				float max_distance = 1.5f;
				int layer_mask = -1;
				int count = Physics.BoxCastNonAlloc( p, s, dir, raycast_hits, Quaternion.identity, max_distance, layer_mask );
				float min_distance = float.MaxValue;
				int min_i = -1;
				for ( int i = 0; i != count; ++i )
				{
					var hit = raycast_hits[i];
					bool is_self = hit.collider.attachedRigidbody == aimableBoxCollider.attachedRigidbody;
					float d = hit.distance;
					if ( ! is_self && d < min_distance )
					{
						min_distance = d;
						min_i = i;
					}
				}

				float distance = max_distance;

				if ( min_i != -1 )
				{
					var hit = raycast_hits[min_i];
					distance = hit.distance;
				}

				float3 dp = dir * distance;
				float3 new_pos = p + dp;
				aimableTr.position = new_pos;
				aimableTr.rotation = Quaternion.identity;

				aimableTr.SetParent( worldRoot, worldPositionStays: true );
			}

			collisions_tracker  = null;
			aimableBoxCollider  = null;
			aimableTr           = null;
			joint.connectedBody = null;

			hasAimable = false;
		}

		public
		void
		reset()
		{
		}

		#if UNITY_EDITOR
		[NonSerialized, ShowInInspector] public Aimable debug_add_aimable;

		[Button("Add")]
		private
		void
		debug_add()
		{
			add_aimable( debug_add_aimable );
		}
		[Button("Remove")]
		private
		void
		debug_remove()
		{
			remove_aimable();
		}
		#endif

		#region Unity
		private
		void
		Awake()
		{
			//if ( aimableTr != null && aimableBoxCollider != null )
			//{
			//	add_aimable( aimableTr, aimableBoxCollider, ref collisions_tracker );
			//}

			gameStateProvider.onSet += handle_new_game_state;
			game_state = gameStateProvider.get();
		}
		private
		void
		FixedUpdate()
		{
			if ( ! game_state.can_update )
			{
				return;
			}

			float3 current_aim_dir = aimableContainerTr.forward;

			float3 original_desiredAimDirection = desiredAimDirection;
			float3 desired_aim_direction = desiredAimDirection;

			if ( all( desired_aim_direction == float3(0,0,0) ) )
			{
				log_err( $"{Time.frameCount} desiredAimDirection is zero. Was ({original_desiredAimDirection.x:E}, {original_desiredAimDirection.y:E}, {original_desiredAimDirection.z:E})", gameObject );
				desired_aim_direction = transform.forward;
			}

			if ( checkCollisions && aimableBoxCollider != null )
			{
				current_aim_dir = check_collisions( current_aim_dir );
				desired_aim_direction = clamp_ang_vel_and_check_collisions( desired_aim_direction, current_aim_dir );
			} else
			{
				desired_aim_direction = clamp_ang_vel( desired_aim_direction, current_aim_dir );
			}

			if ( all( desired_aim_direction == float3(0,0,0) ) )
			{
				log_err( $"{Time.frameCount} desired_aim_direction is zero. Was ({original_desiredAimDirection.x:E}, {original_desiredAimDirection.y:E}, {original_desiredAimDirection.z:E})" );
				desired_aim_direction = transform.forward;
			}

			aimableContainerTr.forward = desired_aim_direction;
		}

		#if UNITY_EDITOR
		float3     center;
		float3     size;
		Quaternion rot;
		[ShowInInspector]
		private float debugAngle = 0;
		private
		void
		OnDrawGizmos()
		{
			if ( ! drawGizmo )
			{
				return;
			}

			if ( ! Application.isPlaying )
			{
				float testing_degs = debugAngle;

				float3 rot_center = aimableContainerTr.position;
				Transform a_tr = aimableBoxCollider.transform;
				Quaternion pre_rot = a_tr.rotation;//Quaternion.LookRotation( a_tr.forward, a_tr.up );
				//LayerMask  layerMask = ComputeLayerMaskOfLayer( aimableBoxCollider.gameObject.layer );
				float3 starting_center = (float3) a_tr.TransformPoint( aimableBoxCollider.center ) - rot_center;

				float3 up = aimableContainerTr.transform.up;

				Quaternion qrot = Quaternion.AngleAxis( testing_degs, up );
				float3 center = (float3)( qrot * starting_center ) + rot_center;
				//halfExtents = a_tr.TransformVector( aimableBoxCollider.size ) / 2;
				float3 halfExtents = (float3)a_tr.lossyScale * aimableBoxCollider.size / 2;
				Quaternion orientation = qrot * pre_rot;


				this.center = center;
				this.size   = 2 * halfExtents;
				this.rot    = orientation;
			}

			var old_mx = Gizmos.matrix;
			var old_c = Gizmos.color;

			Gizmos.matrix = Matrix4x4.TRS( center, rot, float3(1,1,1) );
			Gizmos.color = Color.blue;

			Gizmos.DrawCube( float3(0,0,0), size );

			Gizmos.matrix = old_mx;
			Gizmos.color  = old_c;
		}
		#endif
		#endregion Unity

		#region private
		private Game_State         game_state;
		private RaycastHit[]       raycast_hits = new RaycastHit[128];

		private
		float3
		clamp_ang_vel( float3 desired_dir, float3 curr_dir )
		{
			float3 up = aimableContainerTr.transform.up;
			float a_degs = Vector3.SignedAngle( curr_dir, desired_dir, up );
			if ( abs( a_degs ) > maxAngularVelocityDegs )
			{
				desired_dir = Quaternion.AngleAxis( sign( a_degs ) * maxAngularVelocityDegs, up ) * curr_dir;
			}
			return desired_dir;
		}

		private Collider[] results = new Collider[1024];
		private
		float3
		clamp_ang_vel_and_check_collisions( float3 desired_dir, float3 curr_dir )
		{
			float3 rot_center = aimableContainerTr.position;
			Transform a_tr = aimableBoxCollider.transform;
			float3 up = aimableContainerTr.up;

			float a_degs = Vector3.SignedAngle( curr_dir, desired_dir, up );
			float delta_degs = sign( a_degs ) * min( abs( a_degs ), maxAngularVelocityDegs );
			//halfExtents = a_tr.TransformVector( aimableBoxCollider.size ) / 2;
			float3 halfExtents = (float3)a_tr.lossyScale * aimableBoxCollider.size / 2;
			float abs_degs_step = 1;
			//float degs_step = abs_degs_step * sign( a_degs );
			float testing_degs;
			float tested_degs  = 0;

			Quaternion pre_rot = Quaternion.LookRotation( curr_dir/*a_tr.forward*/, up );
			LayerMask  layerMask = ComputeLayerMaskOfLayer( aimableBoxCollider.gameObject.layer );
			float3 starting_center = (float3) a_tr.TransformPoint( aimableBoxCollider.center ) - rot_center;

			for ( testing_degs = 0;
			      abs( testing_degs ) <= abs( delta_degs );
			      testing_degs += sign( a_degs ) * min( abs_degs_step, abs( delta_degs - testing_degs ) )
			    )
			{
				Quaternion qrot = Quaternion.AngleAxis( testing_degs, up );
				float3 center = (float3)( qrot * starting_center ) + rot_center;
				Quaternion orientation = qrot * pre_rot;

				#if UNITY_EDITOR
				this.center = center;
				this.size   = 2 * halfExtents;
				this.rot    = orientation;
				#endif

				bool collision = false;
				#if false
				collision = Physics.CheckBox( center, halfExtents, orientation, layerMask );
				#else
				int count = Physics.OverlapBoxNonAlloc( center, halfExtents, results, orientation, (int)layerMask );
				for ( int i = 0; i != count; ++i )
				{
					if ( results[i] != aimableBoxCollider )
					{
						collision = true;
						break;
					}
				}
				#endif

				if ( collision )
				{
					break;
				} else
				{
					tested_degs = testing_degs;
				}

				if ( testing_degs == delta_degs )
				{
					break;
				}
			}

			desired_dir = Quaternion.AngleAxis( tested_degs, up ) * curr_dir;
			return desired_dir;
		}
		private
		float3
		check_collisions( float3 fw )
		{
			float3 ret = fw;

			float3 rot_center = aimableContainerTr.position;
			Transform a_tr = aimableBoxCollider.transform;
			float3 up = a_tr.up;
			Quaternion pre_rot = Quaternion.LookRotation( fw/*a_tr.forward*/, up );
			LayerMask  layerMask = ComputeLayerMaskOfLayer( aimableBoxCollider.gameObject.layer );
			float3 starting_center = (float3) a_tr.TransformPoint( aimableBoxCollider.center ) - rot_center;
			float3 halfExtents = (float3)a_tr.lossyScale * aimableBoxCollider.size / 2;
			float testing_degs;
			float tested_degs  = 0;
			const float abs_degs_step = 1;
			const float max_degs = 360;
			float s = 0;

			var lcps = collisions_tracker.contactPointsFixedUpdate;
			for ( int i = 0; i != lcps.Count; ++i )
			{
				var lcp = lcps[i];
				float3 p = lcp.point;
				float3 rel_p = p - rot_center;
				rel_p.y = 0;
				float degs = Vector3.SignedAngle( fw, rel_p, axis: up );
				s = - sign( degs );
			}

			//log( $"sign = {s} count = {lcps.Count}" );
			if ( s == 0 )
			{
				return ret;
			}

			for ( testing_degs = 0;
			      abs( testing_degs ) <= max_degs;
			      testing_degs += s * min( abs_degs_step, abs( max_degs - testing_degs ) )
			    )
			{
				Quaternion qrot = Quaternion.AngleAxis( testing_degs, up );
				float3 center = (float3)( qrot * starting_center ) + rot_center;
				//halfExtents = a_tr.TransformVector( aimableBoxCollider.size ) / 2;
				Quaternion orientation = qrot * pre_rot;

				#if UNITY_EDITOR
				this.center = center;
				this.size   = 2 * halfExtents;
				this.rot    = orientation;
				#endif

				bool collision = false;
				#if false
				collision = Physics.CheckBox( center, halfExtents, orientation, layerMask );
				#else
				int count = Physics.OverlapBoxNonAlloc( center, halfExtents, results, orientation, (int)layerMask );
				for ( int i = 0; i != count; ++i )
				{
					if ( results[i] != aimableBoxCollider )
					{
						collision = true;
						break;
					}
				}
				#endif

				if ( ! collision )
				{
					tested_degs = testing_degs;
					break;
				}

				if ( testing_degs == max_degs )
				{
					tested_degs = 0;
					break;
				}
			}

			ret = Quaternion.AngleAxis( tested_degs, up ) * fw;

			return ret;
		}

		private
		void
		handle_new_game_state( Game_State gs )
		{
			game_state = gs;
		}
		#endregion private
	}
}
