using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Mathematics;
using static Unity.Mathematics.math;
using Sirenix.OdinInspector;
using DG.Tweening;
using tg;
using static tg.tg;

namespace devember2021
{
	public class Character_Fire_Controller : MonoBehaviour
	{
		public Character_Movement_Controller characterMovementController;

		public Game_State_Provider gameStateProvider;

		public Game_State.Character_ID character_id;

		[Header( "Aimable" )]
		public Fireable fireable;

		[Header( "Control" )]
		public bool trigger_on;

		public
		void
		reset()
		{
			if ( fireable != null )
			{
				fireable.reset();
			}
		}

		#region Unity
		private
		void
		Awake()
		{
			gameStateProvider.onSet += handle_new_game_state;
			game_state = gameStateProvider.get();
		}
		private
		void
		FixedUpdate()
		{
			if ( ! game_state.can_update )
			{
				return;
			}

			if ( fireable != null )
			{
				fireable.set_trigger( trigger_on );
				fireable.set_root_velocity( characterMovementController.desiredVelocity );
				fireable.set_character_id( character_id );
			}
		}
		#endregion Unity

		#region private
		private Game_State game_state;

		private
		void
		handle_new_game_state( Game_State gs )
		{
			game_state = gs;
		}
		#endregion private
	}
}
