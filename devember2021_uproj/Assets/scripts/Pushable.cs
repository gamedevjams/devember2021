using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Mathematics;
using static Unity.Mathematics.math;
using Sirenix.OdinInspector;
using DG.Tweening;
using tg;
using static tg.tg;

namespace devember2021
{
	public class Pushable : MonoBehaviour
	{
		public struct Push
		{
			public float3 dir;
			public float velocity_change;
			public float3 pos;
			public float3 normal;
			public Push( float3 dir, float force_module, float3 pos, float3 normal )
			{
				this.dir             = dir;
				this.velocity_change = force_module;
				this.pos             = pos;
				this.normal          = normal;
			}
		}
		public event Action<Push> onPushed;
		public
		void
		push( float3 dir, float force_module, float3 pos, float3 normal )
		{
			onPushed?.Invoke( new Push( dir, force_module, pos, normal ) );
		}

		#region Unity
		#endregion Unity

		#region private
		#endregion private
	}
}
