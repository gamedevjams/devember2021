using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Mathematics;
using static Unity.Mathematics.math;
using Sirenix.OdinInspector;
using DG.Tweening;
using tg;
using static tg.tg;

namespace devember2021
{
	public class Variant_Selector : MonoBehaviour
	{
		[ValueDropdown("dropdown_list"),OnValueChanged("apply_selection")]
		public int              selected;
		[HideInInlineEditors]
		public List<GameObject> variants;

		#region Unity
		private
		void
		Awake()
		{
			apply_selection();
		}
		#endregion Unity

		#region private
#if UNITY_EDITOR
		private
		ValueDropdownList<int>
		dropdown_list()
		{
			ValueDropdownList<int> ret = new ValueDropdownList<int>();
			for ( int i = 0; i != variants.Count; ++i )
			{
				ret.Add( variants[i].name, i );
			}
			return ret;
		}
		#endif

		private
		void
		apply_selection()
		{
			for ( int i = 0; i != variants.Count; ++i )
			{
				variants[i].SetActive( i == selected );
			}
		}
		#endregion private
	}
}
