using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Mathematics;
using static Unity.Mathematics.math;
using Sirenix.OdinInspector;
using DG.Tweening;
using tg;
using static tg.tg;

namespace tg
{
	public class Sound_System : MonoBehaviour
	{
		public Sound_Collection_SO soundCollection;
		public Sound_Source        soundSourcePrefab;

		private List<string> _all_sfx_names = new List<string>();
		public
		List<string>
		all_sfx_names()
		{
			_all_sfx_names.Clear();
			for ( int i = 0; i != soundCollection.SFXs.Length; ++i )
			{
				_all_sfx_names.Add( soundCollection.SFXs[i].name );
			}
			return _all_sfx_names;
		}

		public
		Sound_Source
		playSFX( string name, float3 w_pos, float skip_secs = 0, float delay_secs = 0 )
		{
			if ( soundCollection.SFXs_dict.TryGetValue( name, out Sound_Collection_SO.SFX sfx ) )
			{
				var ss = take_sound_source();
				ss.setup( sfx.get_clip() );
				ss.set_pos( w_pos );
				ss.play( skip_secs, delay_secs );
				return ss;
			} else
			{
				log_err( $"sfx not found '{name}' in collection {soundCollection.name}" );
				return null;
			}
		}

		public
		void
		despawn( Sound_Source ss )
		{
			int index = find_index( ss );
			despawn( index );
		}
		public
		void
		despawn( int index )
		{
			var ss = active_sound_sources[index];
			active_sound_sources    .RemoveAt( index );
			active_sound_sources_ids.RemoveAt( index );
			sound_sources_pool.put_back( ss );
		}

		#region Unity
		private
		void
		Awake()
		{
			sound_sources_pool = new PoolPrefab<Sound_Source>( soundSourcePrefab );
			soundCollection.init();
		}
		#endregion Unity

		#region private
		PoolPrefab<Sound_Source> sound_sources_pool;
		int next_id = 0;
		List<Sound_Source> active_sound_sources     = new List<Sound_Source>();
		List<int         > active_sound_sources_ids = new List<int         >();

		private
		Sound_Source
		take_sound_source()
		{
			Sound_Source ss = sound_sources_pool.take();
			ss.owner = this;
			ss.__set_id( ++next_id );
			active_sound_sources    .Add( ss );
			active_sound_sources_ids.Add( ss.id );
			return ss;
		}

		private
		int
		find_index( Sound_Source ss )
		{
			int ret = -1;
			int id = ss.id;
			for ( int i = 0; i != active_sound_sources_ids.Count; ++i )
			{
				if ( active_sound_sources_ids[i] == id )
				{
					ret = i;
					break;
				}
			}
			return ret;
		}
		#endregion private
	}
}
