using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Mathematics;
using static Unity.Mathematics.math;
using Sirenix.OdinInspector;
using DG.Tweening;
using tg;
using static tg.tg;

namespace tg
{
	[CreateAssetMenu(fileName = "New_Sound_Collection_SO", menuName = "ScriptableObjects/tg/sound/Sound Collection", order = 1)]
	public class Sound_Collection_SO : ScriptableObject
	{
		[Serializable]
		public class SFX
		{
			public enum Mode
			{
				Single,
				FromSet
			}

			public string name;
			public Mode mode;
			[InlineEditor,ShowIfGroup("Single",Condition="@mode==Mode.Single")]
			public Sound_SO sound;
			[InlineEditor,ShowIfGroup("FromSet",Condition="@mode==Mode.FromSet")]
			public Sounds_Set_SO set;

			private Sound_Source.Setup out_setup = new Sound_Source.Setup();
			public
			Sound_Source.Setup
			get_clip()
			{
				switch ( mode )
				{
					case Mode.Single:
						sound.setup( out_setup );
						break;
					case Mode.FromSet:
						set.get_clip( out_setup );
						break;
				}

				return out_setup;
			}
		}

		[LabelText("SFXs"),Searchable,DictionaryDrawerSettings(KeyLabel = "name")]
		public SFX[] SFXs;
		public Dictionary<string,SFX> SFXs_dict { get; private set; }

		public
		void
		init()
		{
			SFXs_dict = new Dictionary<string, SFX>( SFXs.Length );
			for ( int i = 0; i != SFXs.Length; ++i )
			{
				SFXs_dict.Add( SFXs[i].name, SFXs[i] );
			}
		}

		#region Unity
		private
		void
		Awake()
		{
			init();
		}
		#endregion Unity

		#region private
		#endregion private
	}
}
