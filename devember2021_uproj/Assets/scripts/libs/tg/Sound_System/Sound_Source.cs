using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Mathematics;
using static Unity.Mathematics.math;
using Sirenix.OdinInspector;
using DG.Tweening;
using tg;
using static tg.tg;

namespace tg
{
	public class Sound_Source : MonoBehaviour
	{
		public class Setup
		{
			public AudioClip clip;
			/// <summary>
			/// Speed multiplier
			/// </summary>
			public float     pitch;
			public float     max_distance;
			public float     min_distance;
			public float     volume;
			public bool      loop;
		}

		public Sound_System owner;
		public AudioSource audioSource;
		public Transform tr { get; private set; }
		public int id => _id;
		private int _id;
		public void __set_id( int id ) => _id = id;

		public event Action<Sound_Source> onPlayComplete;
		public event Action<Sound_Source> onStopped;
		public event Action<Sound_Source> onPlayEnded; // NOTE(theGiallo): either stopped or completed
		public event Action<Sound_Source> onPaused;
		public event Action<Sound_Source> onUnpaused;

		public bool autoPutBackInPool;

		public
		void
		setup( Setup data )
		{
			if ( play_started )
			{
				log_err( $"play started {audioSource.clip.name} but trying to setup with {data.clip.name}" );
			}

			audioSource.Stop();

			is_paused    = false;
			play_started = false;

			audioSource.clip        = data.clip;
			audioSource.pitch       = data.pitch;
			audioSource.minDistance = data.min_distance;
			audioSource.maxDistance = data.max_distance;
			audioSource.volume      = data.volume;
			audioSource.loop        = data.loop;
		}

		public
		void
		set_pos( float3 w_pos ) => tr.position = w_pos;

		public
		void
		play( float skip_secs = 0, float delay_secs = 0, bool spatialized = true, float spatialBlend = 1 )
		{
			is_paused = false;

			audioSource.time = skip_secs;

			play_started = true;

			audioSource.spatialize   = spatialized;
			audioSource.spatialBlend = spatialBlend;

			if ( delay_secs != 0 )
			{
				audioSource.PlayDelayed( delay_secs );
			} else
			{
				audioSource.Play();
			}
		}
		public
		bool
		pause()
		{

			bool ret = false;
			if ( audioSource.isPlaying && ! is_paused )
			{
				audioSource.Pause();
				onPaused?.Invoke( this );
				is_paused = true;
				ret = true;
			}
			return ret;
		}
		public
		bool
		unpause()
		{

			bool ret = false;
			if ( ! audioSource.isPlaying && is_paused )
			{
				audioSource.UnPause();
				onUnpaused?.Invoke( this );
				is_paused = false;
				ret = true;
			}
			return ret;
		}
		public
		bool
		stop()
		{
			bool ret = false;
			if ( audioSource.isPlaying )
			{
				audioSource.Stop();

				play_started = false;
				is_paused    = false;

				onStopped  ?.Invoke( this );
				onPlayEnded?.Invoke( this );

				if ( autoPutBackInPool )
				{
					put_back();
				}
				ret = true;
			}
			return ret;
		}

		public bool isPlaying => audioSource.isPlaying;

		#region Unity
		private
		void
		Awake()
		{
			tr = transform;
		}
		private
		void
		Update()
		{
			if ( play_started && ! is_paused && ! audioSource.isPlaying )
			{
				play_started = false;

				onPlayComplete?.Invoke( this );
				onPlayEnded   ?.Invoke( this );

				if ( autoPutBackInPool )
				{
					put_back();
				}
			}
		}
		#endregion Unity

		#region private
		private bool play_started;
		private bool is_paused;

		private
		void
		put_back()
		{
			owner.despawn( this );
		}
		#endregion private
	}
}
