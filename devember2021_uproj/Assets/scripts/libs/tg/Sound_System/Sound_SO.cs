using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Mathematics;
using static Unity.Mathematics.math;
using Sirenix.OdinInspector;
using DG.Tweening;
using tg;
using static tg.tg;

namespace tg
{
	[CreateAssetMenu(fileName = "New_Sound_SO", menuName = "ScriptableObjects/tg/sound/Sound", order = 1)]
	public class Sound_SO : ScriptableObject
	{
		public AudioClip clip;

		public enum Pitch_Units
		{
			SpeedMultiplier,
			SemitonesShift
		}

		public Pitch_Units pitchUnits;

		#region SemitonesShift

		[ShowIf("@pitchUnits == Pitch_Units.SemitonesShift")]
		public bool randomSemitones;

		[ShowIf("@pitchUnits == Pitch_Units.SemitonesShift && randomSemitones")]
		public bool allowSubSemitones;

		[MinMaxSlider(minValue: -88, maxValue: 88, showFields: true), ShowIf("@pitchUnits == Pitch_Units.SemitonesShift && randomSemitones")]
		public Vector2 randomSemitonesRange;

		[ShowIf("@pitchUnits == Pitch_Units.SemitonesShift && ! randomSemitones")]
		public float fixedSemitones = 0;

		#endregion SemitonesShift


		#region SpeedMultiplier

		[ShowIf("@pitchUnits == Pitch_Units.SpeedMultiplier")]
		public bool randomSpeed;

		[MinMaxSlider(minValue: -10, maxValue: 10, showFields: true), ShowIf("@pitchUnits == Pitch_Units.SpeedMultiplier && randomSpeed")]
		public Vector2 randomSpeedRange;

		[ShowIf("@pitchUnits == Pitch_Units.SpeedMultiplier && ! randomSpeed")]
		public float fixedSpeed = 1;

		#endregion SpeedMultiplier


		public bool randomVolume;

		[MinMaxSlider(minValue: 0, maxValue: 1, showFields: true),ShowIf("randomVolume")]
		public Vector2 randomVolumeRange;

		[HideIf("randomVolume"),Range(0,1)]
		public float fixedVolume = 1;


		public float minDistance = 0.1f;
		public float maxDistance = 100f;

		public bool loop;

		public
		void
		setup( Sound_Source.Setup out_setup )
		{
			float speed_multiplier = 1;
			switch ( pitchUnits )
			{
				case Pitch_Units.SpeedMultiplier:
					speed_multiplier = randomSpeed  ? random( randomSpeedRange .x, randomSpeedRange .y ) : fixedSpeed;
					break;
				case Pitch_Units.SemitonesShift:
				{
					float semitones_shift;
					if ( randomSemitones )
					{
						if ( allowSubSemitones )
						{
							semitones_shift = random( randomSemitonesRange.x, randomSemitonesRange.y );
						} else
						{
							int min = Mathf.CeilToInt(  randomSemitonesRange.x );
							int max = Mathf.FloorToInt( randomSemitonesRange.y );
							semitones_shift = random( min_incl: min, max_incl: max );
						}
					} else
					{
						semitones_shift = fixedSemitones;
					}
					const float TWELFT_ROOT_OF_TWO = 1.05946309436f; // NOTE(theGiallo): Mathf.Pow( 2f, 1f / 12f )
					speed_multiplier = Mathf.Pow( TWELFT_ROOT_OF_TWO, semitones_shift );
				}
					break;
			}
			out_setup.clip         = clip;
			out_setup.pitch        = speed_multiplier;
			out_setup.volume       = randomVolume ? random( randomVolumeRange.x, randomVolumeRange.y ) : fixedVolume;
			out_setup.min_distance = minDistance;
			out_setup.max_distance = maxDistance;
			out_setup.loop         = loop;
		}

		#region Unity
		#endregion Unity

		#region private
		private
		float
		random( float min, float max )
		{
			float ret = UnityEngine.Random.Range( min, max );
			return ret;
		}
		private
		int
		random( int min_incl, int max_incl )
		{
			int ret = UnityEngine.Random.Range( minInclusive: min_incl, maxExclusive: max_incl + 1 );
			return ret;
		}
		#endregion private
	}
}
