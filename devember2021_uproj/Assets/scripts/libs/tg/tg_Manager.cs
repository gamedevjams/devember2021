﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using static tg.tg;

namespace tg
{
	public class tg_Manager : MonoBehaviour
	{
		public static tg_Manager instance = null;

		public static
		void
		start_coroutine( IEnumerator co )
		{
			instance.StartCoroutine( co );
		}

		#region private
		[RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.AfterSceneLoad)]
		private static
		void
		instantiate()
		{
			if ( instance == null )
			{
				GameObject go = new GameObject( "tg Manager" );
				go.AddComponent<tg_Manager>();
			}
		}
		#endregion

		#region Unity
		private void Awake()
		{
			debug_assert( instance == null );
			instance = this;
			DontDestroyOnLoad( gameObject );
		}
		private void Start()
		{
		}
		private void Update()
		{
		}
		#endregion
	}
}
