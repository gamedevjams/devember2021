using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Mathematics;
using static Unity.Mathematics.math;
using Sirenix.OdinInspector;
using DG.Tweening;
using tg;
using static tg.tg;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace tg
{
	public class
	LayerAttribute : PropertyAttribute {}

	#if UNITY_EDITOR
	[CustomPropertyDrawer( typeof( LayerAttribute ) )]
	public class
	LayerAttributeDrawer : PropertyDrawer
	{
		public override void OnGUI( Rect position, SerializedProperty property, GUIContent label )
		{
			EditorGUI.BeginProperty( position, label, property );
			int index = property.intValue;
			if ( index > 31 )
			{
				log_warn( $"layer {property.propertyPath} is set to {index}, but max value is 31" );
				index = 31;
			} else
			if ( index < 0 )
			{
				log_warn( $"layer {property.propertyPath} is set to {index}, but min value is 0" );
				index = 0;
			}
			property.intValue = EditorGUI.LayerField( position, label, index );
			EditorGUI.EndProperty();
		}
	}
	#endif
}
