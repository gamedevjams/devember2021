﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using static tg.tg;
using System.Collections.Concurrent;
using System;

namespace tg
{
	public class Callback_Executor : MonoBehaviour
	{
		public static void enqueue( Action action )
		{
			instance.actions.Enqueue( action );
		}

		#region private
		[RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.AfterSceneLoad)]
		private static void instantiate()
		{
			if ( instance == null )
			{
				GameObject go = new GameObject( "Callback_Executor" );
				instance = go.AddComponent<Callback_Executor>();
			}
		}
		private ConcurrentQueue<Action> actions = new ConcurrentQueue<Action>();
		private static Callback_Executor instance;
		#endregion

		#region Unity
		private void Awake()
		{
			if ( instance != null )
			{
				log_err( "Callback_Executor instance already exists" );
				Destroy( this );
				return;
			}
			instance = this;
			DontDestroyOnLoad( this );
		}
		private void Start()
		{
		}
		private void Update()
		{
			while ( actions.TryDequeue( out Action action ) )
			{
				action();
			}
		}
		#endregion
	}
}
