﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using static tg.tg;

namespace tg
{
	[ExecuteInEditMode]
	[RequireComponent(typeof(BoxCollider))]
	public class FlatGrid : MonoBehaviour
	{
		public GameObject prefab;
		public Vector2    cell_size;

		#region private
		private BoxCollider box_collider;
		private List<GameObject> instanced = new List<GameObject>();

		[Button]
		private void spawn()
		{
			if ( cell_size.x == 0 || cell_size.y == 0 )
			{
				return;
			}
			gameObject.destroy_all_children();

			Vector3 c = box_collider.center;
			Vector3 s = box_collider.size;
			Vector3 origin = c - s / 2;
			Vector2Int grid_size = new Vector2Int();
			grid_size.x = (int)( s.x / cell_size.x );
			grid_size.y = (int)( s.z / cell_size.y );
			for ( int iy = 0; iy != grid_size.y; ++iy )
			for ( int ix = 0; ix != grid_size.x; ++ix )
			{
				GameObject go = GameObject.Instantiate( prefab, transform, false );
				instanced.Add( go );
				go.transform.localPosition = origin + new Vector3( ix * cell_size.x, 0, iy * cell_size.y );
				go.name = $"{prefab.name} [{ix,2}, {iy,2}]";
			}
		}
		#if UNITY_EDITOR
		private Vector3 old_grid_center;
		private Vector3 old_grid_size;
		#endif
		#endregion

		#region Unity
		private void Awake()
		{
			box_collider = GetComponent<BoxCollider>();
			box_collider.enabled = false;
		}
		private void Start()
		{
		}
		private void Update()
		{
			#if UNITY_EDITOR
			box_collider.enabled = false;
			if ( box_collider.center != old_grid_center
			  || box_collider.size   != old_grid_size   )
			{
				spawn();
				old_grid_center = box_collider.center;
				old_grid_size   = box_collider.size;
			}
			#endif
		}
		private void OnValidate()
		{
			box_collider = GetComponent<BoxCollider>();
			box_collider.enabled = false;
		}
		#endregion
	}
}
