﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

using static tg.tg;

namespace tg
{
	public class RandomRotate : MonoBehaviour
	{
		public Vector3 min_deg;
		public Vector3 max_deg;
		public Vector3 abs_deg;

		[Button]
		public void random_rotate()
		{
			for ( int i = 0, ic = transform.childCount; i != ic; ++i )
			{
				Transform c = transform.GetChild( i );
				Vector3 degs = random_V3( min_deg, max_deg );
				c.Rotate( degs );
			}
		}
		#region private
		#if UNITY_EDITOR
		private Vector3 old_abs_deg;
		private void OnValidate()
		{
			if ( old_abs_deg != abs_deg )
			{
				Vector3 h = abs_deg / 2;
				min_deg = -h;
				max_deg =  h;
				old_abs_deg = abs_deg;
			}
		}
		#endif
		#endregion

		#region Unity
		private void Awake()
		{
		}
		private void Start()
		{
		}
		private void Update()
		{
		}
		#endregion
	}
}
