﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using static tg.tg;
using System;
using System.IO;
using System.Collections.Concurrent;
using System.Threading;

namespace tg
{
	public class Save_Load_Manager : MonoBehaviour
	{
		public static Save_Load_Manager instance;

		public static
		void
		load( string file_name, string profile_name, Action<Opt<byte[]>> result_handler )
		{
			log( $"load {file_name} {profile_name}" );
			run_task( () => load_locking( file_name, profile_name, result_handler ) );
		}
		// NOTE(theGiallo): result_handler parameter is true if no error occurred. Another blob could have been written instead of the given one, if requested after
		public static
		void
		save( string file_name, string profile_name, byte[] data, Action<bool> result_handler = null, int offset = 0, int count = -1 )
		{
			log( $"save( file_name: {file_name}, profile_name:{profile_name}, offset:{offset}, count:{count}, ... )" );
			if ( count < 0 )
			{
				count = data.Length - offset;
			}
			if ( count < 0 || offset < 0 || offset + count > data.Length )
			{
				result_handler?.Invoke( false );
				return;
			}

			string file_path = get_file_path( file_name, profile_name );

			ensure_file_path_in_db( file_path );

			Will_Write ww = blobs_to_save[file_path];
			debug_assert( ww.path == file_path );
			Blob old = ww.swap_in( new Blob( data, offset, count ) );
			if ( old != null )
			{
				log( "skipping an obsolete write" );
			}

			run_task( () => save_locking( file_path, result_handler ) );
		}

		#region private
		[RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.AfterSceneLoad)]
		private static void instantiate()
		{
			if ( instance == null )
			{
				GameObject go = new GameObject( "Save_Load_Manager" );
				instance = go.AddComponent<Save_Load_Manager>();
			}
		}

		private class
		Blob
		{
			public byte[] data;
			public int    offset;
			public int    count;
			public Blob( byte[] data, int offset, int count )
			{
				debug_assert( data != null
				           && data.Length >= offset
				           && offset >= 0
				           && data.Length >= offset + count );
				this.data   = data;
				this.offset = offset;
				this.count  = count;
			}
		};
		private class
		Will_Write
		{
			public string         path { get; }
			public WaitHandle     got_one => _got_one;
			public object         file_lock = new object();

			private Blob          blob;
			private AutoResetEvent _got_one = new AutoResetEvent( false );

			public Will_Write( string path )
			{
				this.path = path;
			}

			public
			Blob
			swap_in( Blob new_blob )
			{
				Blob ret = Interlocked.Exchange<Blob>( ref blob, new_blob );
				bool did_set = _got_one.Set();
				if ( ! did_set )
				{
					log_err( $"handle broken {path}" );
				}
				return ret;
			}
			public
			bool
			swap_out( out Blob blob )
			{
				blob = null;
				blob = Interlocked.Exchange<Blob>( ref this.blob, blob );
				bool ret = blob != null;
				return ret;
			}
		}
		private static ConcurrentDictionary<string, Will_Write> blobs_to_save = new ConcurrentDictionary<string, Will_Write>();

		private static
		void
		ensure_file_path_in_db( string path )
		{
			if ( ! blobs_to_save.ContainsKey( path ) )
			{
				blobs_to_save.TryAdd( path, new Will_Write( path ) );
			}
		}

		private static
		void
		load_locking( string file_name, string profile_name, Action<Opt<byte[]>> result_handler )
		{
			string file_path = get_file_path( file_name, profile_name );

			string moved_file_path = get_moved_file_path( file_path );
			ensure_file_path_in_db( file_path );

			Opt<byte[]> res = Opt<byte[]>.NO_VALUE;

			lock ( blobs_to_save[file_path].file_lock )
			{
				if ( ! File.Exists( file_path )
				  && File.Exists( moved_file_path ) )
				{
					log( $"file not found, but an existing 'moved' file was found, moving it onto the file path {file_path}" );
					File.Move( moved_file_path, file_path );
				}
				bool ret = File.Exists( file_path );
				if ( ret )
				{
					try {
						res = File.ReadAllBytes( file_path );
						ret = true;
					} catch ( Exception e )
					{
						log_err( e.Message );
						ret = false;
					}
				}
			}
			Callback_Executor.enqueue( () => result_handler( res ) );
		}
		private static
		void
		save_locking( string file_path, Action<bool> result_handler = null )
		{
			bool res = false;
			bool found = blobs_to_save.TryGetValue( file_path, out Will_Write ww );
			if ( ! found )
			{
				result_handler?.Invoke( res );
				return;
			}

			res = true;
			lock ( blobs_to_save[file_path].file_lock )
			if ( ww.swap_out( out Blob blob ) )
			{
				log( $"going to save {file_path}" );
				string tmp_file_path = file_path + "___temp";
				string moved_file_path = get_moved_file_path( file_path );
				try {
					string dir = Path.GetDirectoryName( tmp_file_path );
					if ( ! Directory.Exists( dir ) )
					{
						Directory.CreateDirectory( dir );
					}
					FileStream fs = File.Open( tmp_file_path, FileMode.OpenOrCreate );
					fs.Write( blob.data, blob.offset, blob.count );
					fs.Close();
					if ( File.Exists( moved_file_path ) ) File.Delete( moved_file_path );
					if ( File.Exists( file_path ) ) File.Move( file_path, moved_file_path );
					File.Move( tmp_file_path, file_path );
					File.Delete( moved_file_path );
				} catch ( Exception e )
				{
					res = false;
					log_err( e.Message );
				}
			}

			if ( result_handler != null )
			{
				Callback_Executor.enqueue( () => result_handler( res ) );
			}
		}

		private
		static
		string
		get_moved_file_path( string file_path )
		{
			string ret = file_path + "___moved";
			return ret;
		}

		private
		static
		string
		get_file_path( string file_name, string profile_name )
		{
			string ret;
			if ( profile_name == null )
			{
				ret = file_name;
			} else
			{
				ret = Path.Combine( unity_cache.Application.persistentDataPath, profile_name, file_name );
			}
			log( $"path {file_name} {profile_name} => '{ret}'" );
			return ret;
		}
		#endregion

		#region Unity
		private void Awake()
		{
			if ( instance != null )
			{
				log_err( "Save_Load_Manager instance already exists" );
				Destroy( this );
				return;
			}
			instance = this;
			DontDestroyOnLoad( this );
		}
		private void Start()
		{
		}
		private void Update()
		{
		}
		#endregion
	}
}
