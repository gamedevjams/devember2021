using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Mathematics;
using static Unity.Mathematics.math;
using Sirenix.OdinInspector;
using DG.Tweening;
using tg;
using static tg.tg;

namespace tg
{
	[CreateAssetMenu(fileName = "New_VFX_Collection_SO", menuName = "ScriptableObjects/tg/vfx/VFX Collection", order = 1)]
	public class VFX_Collection_SO : ScriptableObject
	{
		[Serializable]
		public class VFX
		{
			public enum Mode
			{
				Single,
				FromSet
			}

			public string name;
			public Mode mode;
			[InlineEditor,ShowIfGroup("Single",Condition="@mode==Mode.Single")]
			public VFX_SO vfx;
			[InlineEditor,ShowIfGroup("FromSet",Condition="@mode==Mode.FromSet")]
			public VFXs_Set_SO set;

			private VFX_Source.Setup out_setup = new VFX_Source.Setup();
			public
			VFX_Source.Setup
			get_clip()
			{
				switch ( mode )
				{
					case Mode.Single:
						vfx.setup( out_setup );
						break;
					case Mode.FromSet:
						set.setup( out_setup );
						break;
				}

				return out_setup;
			}
		}

		[LabelText("VFXs"),Searchable,DictionaryDrawerSettings(KeyLabel = "name")]
		public VFX[] VFXs;
		public Dictionary<string,VFX> VFXs_dict { get; private set; }

		public
		void
		init()
		{
			VFXs_dict = new Dictionary<string, VFX>( VFXs.Length );
			for ( int i = 0; i != VFXs.Length; ++i )
			{
				VFXs_dict.Add( VFXs[i].name, VFXs[i] );
			}
		}

		#region Unity
		private
		void
		Awake()
		{
			init();
		}
		#endregion Unity

		#region private
		#endregion private
	}
}
