using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Mathematics;
using static Unity.Mathematics.math;
using Sirenix.OdinInspector;
using DG.Tweening;
using tg;
using static tg.tg;

namespace tg
{
	[CreateAssetMenu(fileName = "New_VFXs_Set_SO", menuName = "ScriptableObjects/tg/vfx/VFXs Set", order = 1)]
	public class VFXs_Set_SO : ScriptableObject
	{
		[Serializable]
		public class Element
		{
			[InlineEditor]
			public VFX_SO vfx;
			public float    weight = 1;
			[NonSerialized]
			public float    weight_accum = -1;
		}

		public enum Mode
		{
			Random,
			RandomNoRepetitions,
			RandomOrderAll,
			Cycle,
		}

		public Mode mode;
		[ShowIf("@mode == Mode.Random || mode == Mode.RandomNoRepetitions")]
		public bool useWeights;
		public Element[] elements;

		public
		void
		setup( VFX_Source.Setup out_setup )
		{
			int index = 0;
			switch ( mode )
			{
				case Mode.Random:
					index = useWeights ? random_index_weighted() : random( elements.Length );
					break;
				case Mode.RandomNoRepetitions:
					if ( useWeights )
					{
						index = random_index_weighted( random_last_index );
					} else
					{
						index = random( elements.Length - ( random_last_index < 0 ? 0 : 1 ) );
						index = index < random_last_index ? index : index + 1;
					}
					random_last_index = index;
					break;
				case Mode.RandomOrderAll:
					if ( random_ordered_indexes == null )
					{
						random_ordered_indexes = new int[elements.Length];
					}
					if ( random_ordered_index >= elements.Length || random_ordered_index < 0 )
					{
						random_ordered_index = 0;
						for ( int i = 0; i != elements.Length; ++i )
						{
							random_ordered_indexes[i] = i;
						}
						for ( int i = 0; i != random_ordered_indexes.Length; ++i )
						{
							int ii = random( i, random_ordered_indexes.Length );
							                   int tmp = random_ordered_indexes[i ];
							random_ordered_indexes[i ] = random_ordered_indexes[ii];
							random_ordered_indexes[ii] = tmp;
						}
					}
					index = random_ordered_indexes[random_ordered_index++];
					break;
				case Mode.Cycle:
					index = cycle_index;
					cycle_index = cycle_index == elements.Length - 1 ? 0 : cycle_index + 1;
					break;
			}
			var e = elements[index];
			e.vfx.setup( out_setup );
		}

		private int[] random_ordered_indexes = null;
		private int random_ordered_index = -1;
		private int cycle_index = 0;
		private int random_last_index = -1;
		private float tot_weights = -1;

		private
		void
		check_init_weights()
		{
			if ( tot_weights == -1 )
			{
				tot_weights = 0;
				for ( int i = 0; i != elements.Length; ++i )
				{
					tot_weights += elements[i].weight;
					elements[i].weight_accum = tot_weights;
				}
			}
		}
		private
		int
		random_index_weighted( int skip_index = -1 )
		{
			int ret = elements.Length - 1;

			float skip_value = 0;
			if ( skip_index != -1 )
			{
				skip_value = elements[skip_index].weight;
			}

			float v = random( 0, tot_weights - skip_value );

			for ( int i = 0; i != elements.Length; ++i )
			{
				float wa = elements[i].weight_accum - ( skip_index == -1 || i < skip_index ? 0 : skip_value );
				if ( v <= wa )
				{
					ret = i;
					break;
				}
			}

			return ret;
		}

		private
		float
		random( float min, float max )
		{
			float ret = UnityEngine.Random.Range( 0, max );
			return ret;
		}
		private
		int
		random( int max_excl ) => random( 0, max_excl );
		private
		int
		random( int min, int max_excl )
		{
			int ret = UnityEngine.Random.Range( min, max_excl );
			return ret;
		}

		#region Unity
		#endregion Unity

		#region private
		#endregion private
	}
}
