using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Mathematics;
using static Unity.Mathematics.math;
using Sirenix.OdinInspector;
using DG.Tweening;
using tg;
using static tg.tg;

namespace tg
{
	public class VFX_Source : MonoBehaviour
	{
		public class __OBSOLETE_STRUCT
		{
			public void foo()
			{
				ParticleSystem ps = null;
				float simulation_speed = 0;
				{
					var m = ps.main;
					m.simulationSpeed = simulation_speed;
				}
				ps.Play( withChildren: true );
				ps.Stop( withChildren: true, ParticleSystemStopBehavior.StopEmittingAndClear );
				ps.Stop( withChildren: true, ParticleSystemStopBehavior.StopEmitting );
				if ( ps.isStopped || ! ps.isPlaying )
				{
					// put_back
				}

				Transform parent_tr = null;
				Vector3 forward = Vector3.forward;;
				ps.transform.SetParent( parent_tr );
				ps.transform.forward = forward;
				float seconds_delay = 0;
				ps.Simulate( seconds_delay, withChildren:true, restart: true, fixedTimeStep: true );
			}
		}

		public VFX_System owner;
		public List<ParticleSystem> particleSystems;
		public Transform tr { get; private set; }
		public int id => _id;
		private int _id;
		public void __set_id( int id ) => _id = id;

		public event Action<VFX_Source> onPlayComplete;
		public event Action<VFX_Source> onStopped;
		public event Action<VFX_Source> onPlayEnded; // NOTE(theGiallo): either stopped or completed
		public event Action<VFX_Source> onPaused;
		public event Action<VFX_Source> onUnpaused;

		public bool autoPutBackInPool;

		public
		void
		set_pos( float3 w_pos ) => tr.position = w_pos;
		public
		void
		set_parent( Transform parent )
		{
			transform.SetParent( p, worldPositionStays: true );
		}
		public
		void
		set_fw( Vector3 fw )
		{
			transform.forward = fw;
		}
		public
		void
		set_up( Vector3 up )
		{
			transform.up = up;
		}

		public
		void
		play( float skip_secs = 0, float delay_secs = 0, bool spatialized = true, float spatialBlend = 1 )
		{
			is_paused = false;

			play_started = true;
			this.delay_secs = delay_secs;
			this.skip_secs  = skip_secs;
			already_waited_secs = 0;
			start_time = unpaused_time = Time.time;

			if ( delay_secs != 0 )
			{
				delay_coroutine = StartCoroutine( delay_co( skip_secs:skip_secs, delay_secs:delay_secs, restart:true ) );
			} else
			{
				_play( skip_secs:skip_secs, restart:true );
			}
		}
		public
		bool
		pause()
		{
			bool ret = false;
			if ( isPlaying && ! is_paused )
			{
				if ( delay_coroutine != null )
				{
					already_waited_secs += unpaused_time - Time.time;
					StopCoroutine( delay_coroutine );
					delay_coroutine = null;
				} else
				for ( int i = 0; i != particleSystems.Count; ++i )
				{
					var ps = particleSystems[i];
					ps.Pause( withChildren:true );
				}

				onPaused?.Invoke( this );
				is_paused = true;

				ret = true;
			}
			return ret;
		}
		public
		bool
		unpause()
		{
			bool ret = false;
			if ( ! isPlaying && is_paused )
			{
				if ( delay_secs > already_waited_secs )
				{
					delay_coroutine = StartCoroutine( delay_co( skip_secs:skip_secs, delay_secs - already_waited_secs, restart: true ) );
				} else
				{
					_play( skip_secs:0, restart:false );
				}

				onUnpaused?.Invoke( this );
				is_paused = false;
				ret = true;
			}
			return ret;
		}
		public
		bool
		stop( bool immediate )
		{
			bool ret = false;
			if ( isPlaying )
			{
				if ( delay_coroutine != null )
				{
					StopCoroutine( delay_coroutine );
					delay_coroutine = null;
				} else
				for ( int i = 0; i != particleSystems.Count; ++i )
				{
					var ps = particleSystems[i];
					ps.Stop( withChildren:true,
					         immediate
					       ? ParticleSystemStopBehavior.StopEmittingAndClear
					       : ParticleSystemStopBehavior.StopEmitting );
				}

				on_stop_detected( manual: true );

				ret = true;
			}
			return ret;
		}

		public bool isPlaying { get; private set; }

		#region Unity
		private
		void
		Awake()
		{
			tr = transform;
		}
		private
		void
		Update()
		{
			if ( play_started && ! is_paused && no_ps_is_playing() )
			{
				play_started = false;

				on_stop_detected( manual: false );
			}
		}
		#endregion Unity

		#region private
		private bool  play_started;
		private bool  is_paused;
		private float start_time;
		private float unpaused_time;
		private float already_waited_secs;
		private float skip_secs;
		private float delay_secs;

		private Coroutine delay_coroutine;

		private
		IEnumerator
		delay_co( float skip_secs, float delay_secs, bool restart )
		{
			yield return new WaitForSeconds( delay_secs );
			_play( skip_secs:skip_secs, restart );
			delay_coroutine = null;
		}
		private
		void
		_play( float skip_secs, bool restart )
		{
			for ( int i = 0; i != particleSystems.Count; ++i )
			{
				var ps = particleSystems[i];
				if ( skip_secs > 0 )
				{
					ps.Simulate( skip_secs, withChildren:true, restart:restart, fixedTimeStep: true );
				}
				ps.Play( withChildren:true );
			}
		}
		private
		void
		on_stop_detected( bool manual )
		{
			isPlaying    = false;
			play_started = false;
			is_paused    = false;

			if ( manual )
			{
				onStopped  ?.Invoke( this );
			} else
			{
				onPlayComplete?.Invoke( this );
			}
			onPlayEnded?.Invoke( this );

			if ( autoPutBackInPool )
			{
				put_back();
			}
		}
		private
		bool
		no_ps_is_playing()
		{
			bool ret = true;
			for ( int i = 0; i != particleSystems.Count && ret; ++i )
			{
				var ps = particleSystems[i];
				ret = ret && ! ps.isPlaying;
			}
			return ret;
		}

		private
		void
		put_back()
		{
			owner.despawn( this );
		}
		#endregion private
	}
}
