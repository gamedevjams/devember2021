using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Mathematics;
using static Unity.Mathematics.math;
using Sirenix.OdinInspector;
using DG.Tweening;
using tg;
using static tg.tg;

namespace tg
{
	[CreateAssetMenu(fileName = "New_VFX_SO", menuName = "ScriptableObjects/tg/vfx/VFX", order = 1)]
	public class VFX_SO : ScriptableObject
	{
		public VFX_Source source;

		#region SpeedMultiplier

		public bool randomSpeed;

		[MinMaxSlider(minValue: -10, maxValue: 10, showFields: true), ShowIf("randomSpeed")]
		public Vector2 randomSpeedRange;

		[ShowIf("@ ! randomSpeed")]
		public float fixedSpeed = 1;

		#endregion SpeedMultiplier

		public
		void
		setup( VFX_Source.Setup out_setup )
		{
			float speed_multiplier = randomSpeed  ? random( randomSpeedRange .x, randomSpeedRange .y ) : fixedSpeed;
			out_setup.source           = source;
			out_setup.simulation_speed = speed_multiplier;
		}

		#region Unity
		#endregion Unity

		#region private
		private
		float
		random( float min, float max )
		{
			float ret = UnityEngine.Random.Range( min, max );
			return ret;
		}
		private
		int
		random( int min_incl, int max_incl )
		{
			int ret = UnityEngine.Random.Range( minInclusive: min_incl, maxExclusive: max_incl + 1 );
			return ret;
		}
		#endregion private
	}
}
