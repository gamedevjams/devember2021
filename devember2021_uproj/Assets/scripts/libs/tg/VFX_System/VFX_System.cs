using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Mathematics;
using static Unity.Mathematics.math;
using Sirenix.OdinInspector;
using DG.Tweening;
using tg;
using static tg.tg;

namespace tg
{
	public class VFX_System : MonoBehaviour
	{
		public VFX_Collection_SO vfxCollection;
		public VFX_Source        vfxSourcePrefab;

		private List<string> _all_vfx_names = new List<string>();
		public
		List<string>
		all_vfx_names()
		{
			_all_vfx_names.Clear();
			for ( int i = 0; i != vfxCollection.VFXs.Length; ++i )
			{
				_all_vfx_names.Add( vfxCollection.VFXs[i].name );
			}
			return _all_vfx_names;
		}

		public
		VFX_Source
		playVFX( string name, float3 w_pos, float skip_secs = 0, float delay_secs = 0 )
		{
			if ( vfxCollection.VFXs_dict.TryGetValue( name, out VFX_Collection_SO.VFX vfx ) )
			{
				var ss = take_vfx_source();
				ss.setup( vfx.get_clip() );
				ss.set_pos( w_pos );
				ss.play( skip_secs, delay_secs );
				return ss;
			} else
			{
				log_err( $"vfx not found '{name}' in collection {vfxCollection.name}" );
				return null;
			}
		}

		public
		void
		despawn( VFX_Source ss )
		{
			int index = find_index( ss );
			despawn( index );
		}
		public
		void
		despawn( int index )
		{
			var ss = active_vfx_sources[index];
			active_vfx_sources    .RemoveAt( index );
			active_vfx_sources_ids.RemoveAt( index );
			vfx_sources_pool.put_back( ss );
		}

		#region Unity
		private
		void
		Awake()
		{
			vfx_sources_pool = new PoolPrefab<VFX_Source>( vfxSourcePrefab );
			vfxCollection.init();
		}
		#endregion Unity

		#region private
		PoolPrefab<VFX_Source> vfx_sources_pool;
		int next_id = 0;
		List<VFX_Source> active_vfx_sources     = new List<VFX_Source>();
		List<int       > active_vfx_sources_ids = new List<int       >();

		private
		VFX_Source
		take_vfx_source()
		{
			VFX_Source ss = vfx_sources_pool.take();
			ss.owner = this;
			ss.__set_id( ++next_id );
			active_vfx_sources    .Add( ss );
			active_vfx_sources_ids.Add( ss.id );
			return ss;
		}

		private
		int
		find_index( VFX_Source ss )
		{
			int ret = -1;
			int id = ss.id;
			for ( int i = 0; i != active_vfx_sources_ids.Count; ++i )
			{
				if ( active_vfx_sources_ids[i] == id )
				{
					ret = i;
					break;
				}
			}
			return ret;
		}
		#endregion private
	}
}
