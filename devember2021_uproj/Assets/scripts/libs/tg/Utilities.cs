﻿#if !BIT_VECTOR_BYTE_BLOCKS && !BIT_VECTOR_WORD_BLOCKS
#define BIT_VECTOR_WORD_BLOCKS
#endif
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using static tg.tg;

namespace tg
{

	public static partial class
	tg
	{
		public
		static
		unsafe
		uint
		u32_FNV1a( byte*  bytes, int length )
		{
			uint ret;

			const uint FNV_prime    = 16777619;
			const uint offset_basis = 2166136261;

			ret = offset_basis;
			for ( uint i = 0; i < length; ++i )
			{
				ret = ret ^ (uint)bytes[i];
				ret = ret * FNV_prime;
			}

			return ret;
		}
		public
		static
		uint
		u32_FNV1a( byte[] bytes )
		{
			uint ret;

			const uint FNV_prime    = 16777619;
			const uint offset_basis = 2166136261;

			ret = offset_basis;
			for ( uint i = 0; i < bytes.Length; ++i )
			{
				ret = ret ^ (uint)bytes[i];
				ret = ret * FNV_prime;
			}

			return ret;
		}
		public
		static
		uint
		u32_FNV1a( string s )
		{
			uint ret;

			byte[] bytes = System.Text.Encoding.UTF8.GetBytes( s );

			ret = u32_FNV1a( bytes );

			return ret;
		}
		public
		static
		int
		s32_FNV1a( byte[] bytes )
		{
			int ret;
			uint u = u32_FNV1a( bytes );
			ret = System.BitConverter.ToInt32( System.BitConverter.GetBytes( u ), 0 );
			return ret;
		}
		public
		static
		int
		s32_FNV1a( string s )
		{
			int ret;
			uint u = u32_FNV1a( s );
			ret = System.BitConverter.ToInt32( System.BitConverter.GetBytes( u ), 0 );
			return ret;
		}

		public
		static
		float
		deg_to_0_360( float deg )
		{
			float ret = deg - Mathf.Floor( deg / 360 ) * 360;
			ret = ret < 0 ? ret + 360 : ret;
			return ret;
		}

		public static float clamp_deg_0_360( float deg_0360, float min_deg_0360, float max_deg_0360 )
		{
			float ret;
			if ( min_deg_0360 > max_deg_0360 )
			{
				ret = deg_0360 < max_deg_0360
				    ? deg_0360
				    : ( deg_0360 > min_deg_0360
				      ? deg_0360
				      : ( deg_0360 <= 180
				        ? max_deg_0360
				        : min_deg_0360 ) );
			} else
			{
				ret = Mathf.Clamp( deg_0360, min_deg_0360, max_deg_0360 );
			}
			return ret;
		}

		public
		static
		float
		deg_to_center0( float deg )
		{
			float ret = deg_to_0_360( deg + 180 ) - 180;
			return ret;
		}

		public
		static
		float
		clamp_deg_center0( float deg_center0, float min_deg_center0, float max_deg_center0 )
		{
			float ret = Mathf.Clamp( deg_center0, min_deg_center0, max_deg_center0 );
			return ret;
		}

		public
		static
		float
		clamp_deg_to_center0( float deg )
		{
			float ret = Mathf.Clamp( deg, -180, 180 );
			return ret;
		}

		public
		static
		bool
		f_eps( float f0, float f1, float eps )
		{
			bool ret = Mathf.Abs( f0 - f1 ) < eps;
			return ret;
		}

		public static
		bool
		v3_eps( Vector3 v0, Vector3 v1, float eps )
		{
			bool ret = f_eps( v0.x, v0.x, eps ) && f_eps( v0.y, v1.y, eps ) && f_eps( v0.z, v1.z, eps );
			return ret;
		}

		public
		static
		bool
		same_sign( float a, float b )
		{
			bool ret = ( a > 0 && b > 0 ) || ( a < 0 && b < 0 ) || ( a == 0 && b == 0 );
			return ret;
		}
		public
		static
		bool
		a_greater_in_module_than_b_and_same_sign( float a, float b )
		{
			bool ret = same_sign( a, b ) && Mathf.Abs( a ) > Mathf.Abs( b );
			return ret;
		}

		public
		static
		float
		get_angle_deg_to_align_two_points_to_target( Vector2 target, Vector2 forward_point, Vector2 backward_point )
		{
			Vector2 T = target;
			Vector2 P = forward_point;
			Vector2 B = backward_point;

			Vector2 BOdir2   = -B.normalized;
			Vector2 BPdir2   = ( P - B ).normalized;
			//Vector3 BOdir3   = new Vector3( BOdir2.x, 0, BOdir2.y );
			//Vector3 BPdir3   = new Vector3( BPdir2.x, 0, BPdir2.y );
			//float sin_beta   = Vector3.Cross( BOdir3, BPdir3 ).y;
			float deg_beta   = Vector2.Angle( BOdir2, BPdir2 );
			float sin_beta   = Mathf.Sin( Mathf.Deg2Rad * deg_beta );
			//float sin_alpha  = Vector3.Cross( P.normalized, B.normalized ).magnitude;
			float sin_delta  = sin_beta * B.magnitude / T.magnitude;
			float deg_delta  = Mathf.Rad2Deg * Mathf.Asin( sin_delta );
			float deg_alpha  = 180 - deg_beta - deg_delta;
			float deg_alpha_ = Vector2.Angle( T, B );

			float y_rotation  = deg_alpha - deg_alpha_;
			return y_rotation;
		}

		public
		class
		Timer
		{
			public  bool  active = false;
			private float target_time;
			private float duration;
			public  bool use_fixed_time = false;

			public Timer( float duration, bool autostart = false, float delay = 0 )
			{
				this.duration = duration + delay;
				if ( autostart )
				{
					restart();
				}
				this.duration = duration;
			}
			public
			void
			restart( float duration )
			{
				this.duration = duration;
				target_time = time() + duration;
				active = true;
			}
			public
			void
			restart()
			{
				target_time = time() + duration;
				active = true;
			}
			public
			float
			remaining_time()
			{
				float ret = target_time - time();
				return ret;
			}
			public
			float
			passed_time()
			{
				float ret = time() - target_time + duration;
				return ret;
			}
			public
			float
			remaining_percentage()
			{
				float ret = duration == 0 ? 1 : remaining_time() / duration;
				return ret;
			}
			public
			float
			passed_percentage()
			{
				float ret = duration == 0 ? 1 : passed_time() / duration;
				return ret;
			}
			public
			bool
			loopback()
			{
				bool ret = false;
				float t = remaining_time();
				if ( t <= 0 )
				{
					ret = true;
					target_time = time() + duration + t;
				}
				return ret;
			}

			private
			float
			time()
			{
				float ret = use_fixed_time ? Time.fixedTime : Time.time;
				return ret;
			}
		}

		public
		struct
		Bit_Vector
		{
			#if BIT_VECTOR_BYTE_BLOCKS
			private byte[] array;
			#elif BIT_VECTOR_WORD_BLOCKS
			private uint[]  array;
			#endif
			public int bits_count { get; private set; }
			public int bits_real_capacity { get; private set; }

			public
			Bit_Vector( int bits_count, bool round_up_count = false )
			{
				#if BIT_VECTOR_BYTE_BLOCKS
				array = new byte[ ( bits_count + 7 ) / 8 ];
				#elif BIT_VECTOR_WORD_BLOCKS
				array = new uint[ ( bits_count + 31 ) / 32 ];
				#endif
				this.bits_real_capacity = real_capacity_of_array( array.Length );
				this.bits_count = round_up_count ? this.bits_real_capacity : bits_count;
			}
			public
			static
			int real_capacity_of_array( int array_length )
			{
				int ret;

				#if BIT_VECTOR_BYTE_BLOCKS
				ret = 8 * array_length;
				#elif BIT_VECTOR_WORD_BLOCKS
				ret = 32 * array_length;
				#endif

				return ret;
			}
			public
			int
			get_blocks_used_count()
			{
				int ret;
				int tmp;
				get_block_bit( bits_count - 1, out ret, out tmp );
				++ret;
				return ret;
			}
			public
			int this[int index]
			{
				get
				{
					int ret;
					int block_index, block_bit_index;
					get_block_bit( index, out block_index, out block_bit_index );
					ret = (int)( ( array[block_index] >> block_bit_index ) & 1u );
					return ret;
				}
				set
				{
					int block_index, block_bit_index;
					get_block_bit( index, out block_index, out block_bit_index );
					debug_assert( array != null );
					array[block_index] = value == 0 ? array[block_index] & ~( 1u << block_bit_index )
					                                : array[block_index] |  ( 1u << block_bit_index );
				}
			}
			public
			void
			set( int index, int value )
			{
				int block_index, block_bit_index;
				get_block_bit( index, out block_index, out block_bit_index );
				array[block_index] = value == 0 ? array[block_index] & ~( 1u << block_bit_index )
				                                : array[block_index] |  ( 1u << block_bit_index );
			}

			public
			void
			clear()
			{
				for ( int i = 0; i != array.Length; ++i )
				{
					array[i] = 0;
				}
			}

			public
			void
			all_to_1()
			{
				for ( int i = 0; i != array.Length; ++i )
				{
					array[i] =
					#if BIT_VECTOR_BYTE_BLOCKS
					0xff;
					#elif BIT_VECTOR_WORD_BLOCKS
					0xffffffff;
					#endif
				}
			}

			public
			void
			negate()
			{
				for ( int i = 0; i != array.Length; ++i )
				{
					array[i] = ~array[i];
				}
			}

			public
			void
			copy_from( ref Bit_Vector other )
			{
				int other_blocks_count = other.get_blocks_used_count();
				if ( array == null || array.Length < other_blocks_count )
				{
					array = new
					#if BIT_VECTOR_BYTE_BLOCKS
					byte
					#elif BIT_VECTOR_WORD_BLOCKS
					uint
					#endif
					[other_blocks_count];

					bits_real_capacity = real_capacity_of_array( array.Length );
				}
				for ( int i = 0; i != other_blocks_count; ++i )
				{
					array[i] = other.array[i];
				}
				bits_count = other.bits_count;
			}

			public
			Bit_Vector
			copy_whole( uint u )
			{
				#if BIT_VECTOR_BYTE_BLOCKS
				private byte[] array;
				array[0] = ( u >> 24 ) & 0xff;
				array[1] = ( u >> 16 ) & 0xff;
				array[2] = ( u >>  8 ) & 0xff;
				array[3] = ( u       ) & 0xff;
				#elif BIT_VECTOR_WORD_BLOCKS
				array[0] = u;
				#endif
				return this;
			}

			private
			void
			get_block_bit( int bit_index, out int block_index, out int block_bit_index )
			{
				#if BIT_VECTOR_BYTE_BLOCKS
				block_index = bit_index / 8;
				block_bit_index = 7 - ( bit_index - block_index * 8 );
				#elif BIT_VECTOR_WORD_BLOCKS
				block_index = bit_index / 32;
				block_bit_index = 31 - ( bit_index - block_index * 32 );
				#endif
			}

			public
			Bit_Vector
			clone()
			{
				Bit_Vector ret = new Bit_Vector( bits_count );
				ret.copy_from( ref this );
				return ret;
			}

			public
			bool
			any()
			{
				bool ret = false;
				for ( int i = 0; i != bits_count; ++i )
				{
					if ( this[i] == 1 )
					{
						ret = true;
						break;
					}
				}
				return ret;
			}

			public static
			Bit_Vector
			operator &( Bit_Vector v0, Bit_Vector v1 )
			{
				debug_assert( v0.bits_real_capacity == v1.bits_real_capacity );
				Bit_Vector ret = v0.clone();

				for ( int i = 0; i != ret.array.Length; ++i )
				{
					ret.array[i] = v0.array[i] & v1.array[i];
				}

				return ret;
			}
			public static
			Bit_Vector
			operator |( Bit_Vector v0, Bit_Vector v1 )
			{
				debug_assert( v0.bits_real_capacity == v1.bits_real_capacity );
				Bit_Vector ret = v0.clone();

				for ( int i = 0; i != ret.array.Length; ++i )
				{
					ret.array[i] = v0.array[i] | v1.array[i];
				}

				return ret;
			}
			public static
			Bit_Vector
			operator ~( Bit_Vector v0 )
			{
				Bit_Vector ret = v0.clone();
				for ( int i = 0; i != v0.array.Length; ++i )
				{
					ret.array[i] = ~v0.array[i];
				}

				return ret;
			}

			public override
			string
			ToString()
			{
				string ret;
				char[] chars = new char[bits_count];
				for ( int i = 0; i != bits_count; ++i )
				{
					chars[i] = (char) ( '0' + this[i] );
				}
				ret = new string( chars );
				return ret;
			}
		}

		public
		struct
		Flat_Hyper_Matrix<T>
		{
			public  int   dimensions_count { get { return _sizes.Length; } }
			public int[] _sizes;
			public
			int[]
			get_sizes_copy()
			{
				int[] ret = new int[_sizes.Length];
				_sizes.CopyTo( ret, 0 );
				return ret;
			}
			private int[] sizes_multiplied;
			public  T[]   array;

			public
			Flat_Hyper_Matrix( params int[] sizes )
			{
				this._sizes            = new int[sizes.Length];
				this.sizes_multiplied = new int[sizes.Length];
				array = new T[mult( sizes )];
				sizes_multiplied[0] = 1;
				this._sizes[0] = sizes[0];
				for ( int i = 1; i != sizes.Length; ++i )
				{
					this._sizes[i] = sizes[i];
					sizes_multiplied[i] = sizes[i - 1] * sizes_multiplied[i - 1];
				}
			}
			public
			ref T this[params int[] indexes]
			{
				get
				{
					ref T ret = ref array[index(indexes)];
					return ref ret;
				}
			}
			public
			ref T at( params int[] point )
			{
				ref T ret = ref array[ index( point ) ];
				return ref ret;
			}
			public
			int
			index( params int[] point )
			{
				int ret = 0;
				for ( int i = 0 ; i != dimensions_count; ++i )
				{
					ret += point[i] * sizes_multiplied[i];
				}
				return ret;
			}
			public
			int[]
			get_point_from_index( int index )
			{
				int[] ret = new int[dimensions_count];
				for ( int i = 0 ; i != dimensions_count; ++i )
				{
					int m = i != dimensions_count - 1 ? sizes_multiplied[ i + 1 ] : array.Length;
					ret[i] = ( index % m ) / sizes_multiplied[i];
				}
				return ret;
			}
		}

		public
		struct
		Flat_Bit_Hyper_Matrix
		{
			public  int   dimensions_count { get { return _sizes.Length; } }
			public int[] _sizes;
			public
			int[]
			get_sizes_copy()
			{
				int[] ret = new int[_sizes.Length];
				_sizes.CopyTo( ret, 0 );
				return ret;
			}
			private int[] sizes_multiplied;
			public Bit_Vector bit_vector;

			public
			Flat_Bit_Hyper_Matrix( params int[] sizes )
			{
				this._sizes            = new int[sizes.Length];
				this.sizes_multiplied = new int[sizes.Length];
				bit_vector = new Bit_Vector( mult( sizes ) );
				sizes_multiplied[0] = 1;
				for ( int i = 1; i != sizes.Length; ++i )
				{
					this._sizes[i] = sizes[i];
					sizes_multiplied[i] = sizes[i - 1] * sizes_multiplied[i - 1];
				}
			}
			public
			int this[params int[] indexes]
			{
				get
				{
					int ret = bit_vector[index(indexes)];
					return ret;
				}
				set
				{
					bit_vector[index(indexes)] = value;
				}
			}
			public
			void
			set_b( bool b, params int[] point )
			{
				bit_vector[ index( point ) ] = b ? 1 : 0;
			}
			public
			int
			at( params int[] point )
			{
				int ret = bit_vector[ index( point ) ];
				return ret;
			}
			public
			int
			index( params int[] point )
			{
				int ret = 0;
				for ( int i = 0 ; i != dimensions_count; ++i )
				{
					ret += point[i] * sizes_multiplied[i];
				}
				return ret;
			}
			public
			int[]
			get_point_from_index( int index )
			{
				int[] ret = new int[dimensions_count];
				for ( int i = 0 ; i != dimensions_count; ++i )
				{
					int m = i != dimensions_count - 1 ? sizes_multiplied[ i + 1 ] : bit_vector.bits_count;
					ret[i] = ( index % m ) / sizes_multiplied[i];
				}
				return ret;
			}
		}


		public
		struct
		Multi_Dictionary<K,V>
		{
			public Dictionary<K,List<V>> dictionary;
			public
			void
			init()
			{
				dictionary = new Dictionary<K, List<V>>();
			}
			public
			List<V> this[K key]
			{
				get
				{
					TryGetValues( key, out List<V> ret );
					return ret;
				}
				set
				{
					dictionary[key] = value;
				}
			}
			public
			void
			Add( K key, V value )
			{
				List<V> lv;
				if ( !dictionary.TryGetValue( key, out lv ) )
				{
					lv = new List<V>();
					dictionary[key] = lv;
				}
				lv.Add( value );
			}
			public
			bool
			TryGetValues( K key, out List<V> values )
			{
				bool ret = dictionary.TryGetValue( key, out values );
				return ret;
			}
			public
			bool
			TryGetFirst( K key, ref V value )
			{
				bool ret;
				List<V> l;
				ret = dictionary.TryGetValue( key, out l );
				if ( ret )
				{
					value = l[0];
				}
				return ret;
			}
		}

		public
		struct
		Stack<T>
		{
			Chunked_Array<T> array;
			public
			int
			size()
			{
				int ret = array.occupancy;
				return ret;
			}

			public
			void
			init()
			{
				array = new Chunked_Array<T>( -1 );
			}
			public
			void
			push( ref T e )
			{
				array.add( ref e );
			}
			public
			void
			push( T e )
			{
				array.add( e );
			}
			public
			bool
			pop( ref T e )
			{
				bool ret = array.occupancy != 0;
				if ( ret )
				{
					e = ref array.last();
					array.remove_last();
				}
				return ret;
			}
			public
			bool
			peek( ref T e )
			{
				bool ret = array.occupancy != 0;
				if ( ret )
				{
					e = ref array.last();
				}
				return ret;
			}
			public
			ref T
			peek()
			{
				ref T ret = ref array.last();
				return ref ret;
			}
			public
			ref T
			Peek()
			{
				if ( array.occupancy == 0 )
				{
					throw new System.InvalidOperationException();
				}
				ref T ret = ref array.last();
				return ref ret;
			}
		}
		[Serializable]
		public
		class
		Chunked_Array<T> : ISerializationCallbackReceiver
		{
			public List<T[]> chunks;
			//public List<int> chunks_occupancy;
			public int first_free_chunk { get; private set; }
			public int first_free_chunk_occupancy { get; private set; }
			public int occupancy { get; private set; }
			public int chunks_size { get => _chunks_size; set { _chunks_size = value; } }
			[SerializeField]
			private int _chunks_size;
			public
			Chunked_Array( int chunks_size = 1024, uint initial_chunks_count = 0 )
			{
				init( chunks_size, initial_chunks_count );
			}
			private
			void
			init( int chunks_size = 1024, uint initial_chunks_count = 0 )
			{
				this.chunks_size = chunks_size <= 0 ? 1024 : chunks_size;
				chunks = new List<T[]>();
				//chunks_occupancy = new List<int>();
				first_free_chunk = -1;
				first_free_chunk_occupancy = 0;
				occupancy = 0;
				for ( uint i = 0; i != initial_chunks_count; ++i )
				{
					add_chunk();
				}
			}
			public
			void
			add_chunk()
			{
				chunks.Add( new T[ chunks_size ] );
				//chunks_occupancy.Add( 0 );
				first_free_chunk = first_free_chunk < 0 ? chunks.Count - 1 : first_free_chunk;
			}
			public
			void
			add( ref T e )
			{
				if ( first_free_chunk < 0 )
				{
					add_chunk();
				}
				chunks[first_free_chunk][first_free_chunk_occupancy++] = e;
				++occupancy;
				if ( first_free_chunk_occupancy == chunks_size )
				{
					first_free_chunk =
					   first_free_chunk == chunks.Count - 1 ? -1 : first_free_chunk + 1;
					first_free_chunk_occupancy = 0;
				}
			}
			public
			void
			add( T e )
			{
				if ( first_free_chunk < 0 )
				{
					add_chunk();
				}
				chunks[first_free_chunk][first_free_chunk_occupancy++] = e;
				++occupancy;
				if ( first_free_chunk_occupancy == chunks_size )
				{
					first_free_chunk =
					   first_free_chunk == chunks.Count - 1 ? -1 : first_free_chunk + 1;
					first_free_chunk_occupancy = 0;
				}
			}
			public
			void
			add( T[] ca )
			{
				for ( int i = 0; i != ca.Length; ++i )
				{
					add( ca[i] );
				}
			}
			public
			void
			add( List<T> ca )
			{
				for ( int i = 0; i != ca.Count; ++i )
				{
					add( ca[i] );
				}
			}
			public
			void
			add( Chunked_Array<T> ca )
			{
				for ( int i = 0; i != ca.occupancy; ++i )
				{
					add( ca[i] );
				}
			}
			public
			bool
			remove_last()
			{
				bool ret = false;

				if ( occupancy != 0 )
				{
					if ( first_free_chunk == -1 )
					{
						first_free_chunk = chunks.Count - 1;
						first_free_chunk_occupancy = chunks_size - 1;
					} else
					if ( first_free_chunk_occupancy == 0 )
					{
						--first_free_chunk;
						first_free_chunk_occupancy = chunks_size - 1;
					} else
					{
						--first_free_chunk_occupancy;
					}
					--occupancy;
					ret = true;
				}

				return ret;
			}
			public
			ref T this[ int index ]
			{
				get
				{
					int ci = index / chunks_size;
					int i  = index - chunks_size * ci;
					debug_assert( ci <= first_free_chunk && ( ci != first_free_chunk || i < first_free_chunk_occupancy ) );
					ref T ret = ref chunks[ci][i];
					return ref ret;
				}
			}
			public
			ref T last()
			{
				int index = occupancy - 1;
				int ci = index / chunks_size;
				int i  = index - chunks_size * ci;
				ref T ret = ref chunks[ci][i];
				return ref ret;
			}

			public
			void
			clear()
			{
				first_free_chunk = 0;
				first_free_chunk_occupancy = 0;
				occupancy = 0;
			}

			public
			void
			copy_from( Chunked_Array<T> other )
			{
				clear();
				for ( int i = 0; i != other.occupancy; ++i )
				{
					add( ref other[i] );
				}
			}

			[SerializeField]
			private T[] __arr_for_unity;
			public void OnBeforeSerialize()
			{
				__arr_for_unity = new T[occupancy];
				for ( int i = 0; i != occupancy; ++i )
				{
					__arr_for_unity[i] = this[i];
				}
			}

			public void OnAfterDeserialize()
			{
				init( chunks_size, 1 );
				for ( int i = 0; i != __arr_for_unity.Length; ++i )
				{
					add( __arr_for_unity[i] );
				}
				__arr_for_unity = null;
			}
		}

		[Serializable]
		public
		struct
		Array_Slice<T>
		{
			public T[] array;
			public int offset;
			public int count;

			public
			Array_Slice( T[] array) : this( array, 0, array.Length ) { }
			public static implicit operator Array_Slice<T>( T[] array ) => new Array_Slice<T>( array );
			public
			Array_Slice( T[] array, int offset, int count )
			{
				if ( offset > array.Length )
				{
					offset = array.Length;
					count = 0;
				}
				count = offset + count <= array.Length ? count : array.Length - offset;
				this.array  = array;
				this.count  = count;
				this.offset = offset;
			}
		}
		public class Pool<T> where T : class, new()
		{
			protected virtual T spawn() => new T();
			protected virtual void handle_put_back( T e ){}
			protected virtual void handle_taken( T e ){}

			public
			void
			put_back( T e )
			{
				handle_put_back( e );
				pool.Add( e );
			}
			public
			T
			take()
			{
				T ret;
				if ( pool.Count == 0 )
				{
					ret = spawn();
				} else
				{
					ret = pool[pool.Count-1];
					pool.RemoveAt(pool.Count-1);
				}
				handle_taken( ret );
				return ret;
			}
			public void reserve( int count )
			{
				pool.Reserve( count );
				for ( int i = 0; i < count; ++i )
				{
					pool.Add( spawn() );
				}
			}

			private List<T> pool = new List<T>();
		}
		public class PoolPrefab<T> : Pool<T> where T : Component, new()
		{
			public PoolPrefab( T prefab )
			{
				init( prefab );
			}
			public
			void
			init( T prefab )
			{
				Destroy();

				holder = new GameObject( $"pool of {prefab.name}" );
				holder.SetActive( false );
				holder_tr = holder.transform;
				this.prefab = prefab;
			}
			public void DontDestroyOnLoad()
			{
				Component.DontDestroyOnLoad( holder );
			}
			public
			void
			Destroy()
			{
				if ( holder != null )
				{
					Component.Destroy( holder );
					holder    = null;
					holder_tr = null;
					prefab = null;
				}
			}
			~PoolPrefab()
			{
				Destroy();
			}
			protected override
			T
			spawn()
			{
				T ret = Component.Instantiate( prefab, holder_tr );
				ret.gameObject.SetActive( false );
				return ret;
			}
			protected override
			void
			handle_taken( T e )
			{
				e.transform.SetParent( null, worldPositionStays: false );
				e.gameObject.SetActive( true );
			}
			protected override
			void
			handle_put_back( T e )
			{
				e.gameObject.SetActive( false );
				e.transform.SetParent( holder_tr, worldPositionStays: false );
			}

			public T prefab             { get; protected set; }
			public GameObject holder    { get; protected set; }
			public Transform  holder_tr { get; protected set; }
		}

		public
		class
		FSM<T>
		{
			public
			abstract class
			State
			{
				public abstract T name { get; }

				public FSM<T> M { get; private set; }
				public void _AddedTo( FSM<T> m )
				{
					if ( M != null )
					{
						log_err( $"FSM<{typeof(T).Name}>.State added to multiple FSMs" );
						return;
					}

					M = m;
				}
				public bool isTransitioning => M.transitioning && ( M.transition.from == this || M.transition.to == this );
				public void _RemovedFromFSM() => M = null;

				public virtual void WillEnter( State From, Action done ) => done?.Invoke();
				public virtual void Enter( State from, Action done ) => done?.Invoke();
				public virtual void WillExit( State To, Action done ) => done?.Invoke();
				public virtual void Exit( State To, Action done ) => done?.Invoke();
				public virtual void Update() {}
			}
			public
			interface
			IStateMB
			{
				State state { get; }
			}

			public State curr_state { get; private set; }
			public bool transitioning => transition.transitioning;
			public State transitioning_from => transition.from;
			public State transitioning_to   => transition.to;

			public FSM()
			{
				transition = new Transition( this );
			}

			public virtual
			bool
			name_is_null( T name ) => false;

			public
			bool
			Add( State state )
			{
				bool ret = false;
				if ( states.TryAdd( state.name, state ) )
				{
					ret = true;
					state._AddedTo( this );
				}
				return ret;
			}

			public
			bool
			GoTo( State state, Action done = null )
			{
				bool ret = false;

				if ( transitioning )
				{
					return ret;
				}

				if ( curr_state == state && curr_state == null )
				{
					ret = true;
					done?.Invoke();
					return ret;
				}

				transition.GoTo( curr_state, state, done );
				//log( $"{(curr_state != null ? curr_state.name.ToString() : "null")} => {(state != null ? state.name.ToString() : "null")}" );
				ret = true;

				return ret;
			}

			public
			bool
			GoTo( T name, Action done = null )
			{
				bool ret = false;
				if ( states.TryGetValue( name, out State state ) )
				{
					ret = GoTo( state, done );
				} else
				if ( name_is_null( name ) )
				{
					ret = GoTo( null, done );
				} else
				{
					log_err( $"requeted state is not present in FSM ({name})" );
				}
				return ret;
			}

			public
			bool
			TryRemove( T name, out State state )
			{
				bool ret = false;
				if ( states.Remove( name, out state ) )
				{
					if ( transition.to == state )
					{
						 transition.nullify_to();
					} else
					if ( transition.from == state )
					{
						transition.nullify_from();
					} else
					if ( curr_state == state )
					{
						states.Add( name, state );
						GoTo( null, ()=> { states.Remove( name, out State s ); s._RemovedFromFSM(); } );
					} else
					{
						state._RemovedFromFSM();
					}
					ret = true;
				}
				return ret;
			}

			public
			bool
			TryGetState( T name, out State state ) => states.TryGetValue( name, out state );

			public
			List<T>
			StatesList( List<T> list = null )
			{
				if ( list == null )
				{
					list = new List<T>();
				}
				list.Add( states.Keys );
				return list;
			}

			public void Update()
			{
				curr_state?.Update();
			}

			#region private
			protected Dictionary<T, State> states = new Dictionary<T, State>();
			private Transition transition;

			private class Transition
			{
				public Transition( FSM<T> fsm )
				{
					this.fsm = fsm;
				}

				FSM<T> fsm;

				public State from { get; private set; }
				public State to   { get; private set; }

				Action done;
				public bool transitioning { get; private set; }

				bool will_enter_done;
				bool will_exit_done;
				bool enter_done;
				bool exit_done;

				public bool GoTo( State f, State t, Action done )
				{
					bool ret = false;
					if ( transitioning || ( f == null && t == null ) )
					{
						return ret;
					}

					from = f;
					to   = t;
					this.done = done;

					transitioning = true;

					will_enter_done = to   == null;
					enter_done      = to   == null;
					will_exit_done  = from == null;
					exit_done       = from == null;

					if ( from != null )
					{
						from.WillExit( to, after_will_exit );
					} else
					{
						after_will_exit();
					}

					if ( to != null )
					{
						to.WillEnter( from, after_will_enter );
					} else
					{
						after_will_enter();
					}

					ret = true;

					return ret;
				}

				private void after_will_exit()
				{
					will_exit_done = true;
					if ( will_enter_done )
					{
						enter_exit();
					}
				}
				private void after_will_enter()
				{
					will_enter_done = true;
					if ( will_exit_done )
					{
						enter_exit();
					}
				}
				private void enter_exit()
				{
					if ( from != null )
					{
						from.Exit( to, after_exit );
					} else
					{
						after_exit();
					}
					if ( to != null )
					{
						to  ?.Enter( from, after_enter );
					} else
					{
						after_enter();
					}
				}
				private void after_exit()
				{
					exit_done = true;
					if ( enter_done )
					{
						conclusion();
					}
				}
				private void after_enter()
				{
					enter_done = true;
					if ( exit_done )
					{
						conclusion();
					}
				}
				private void conclusion()
				{
					transitioning = false;
					fsm.curr_state = to;
					done?.Invoke();
				}
				public void nullify_to()
				{
					Action old_done = done;
					done = ()=> { fsm.TryRemove( to.name, out _ ); old_done?.Invoke(); };

					after_will_enter();
					after_enter();
				}
				public void nullify_from()
				{
					Action old_done = done;
					done = ()=> { fsm.TryRemove( from.name, out _ ); old_done?.Invoke(); };

					after_will_exit();
					after_exit();
				}
			}
			#endregion
		}
		public class FSM : FSM<string>
		{
			public override bool name_is_null( string name ) => string.IsNullOrEmpty( name ) || name == "null";
		}

		public class
		Waiting_Event
		{
			public event Action onDone;

			public
			void
			add( Action<Action> callback )
			{
				if ( ! contains( callback ) )
				{
					callbacks.Add( callback );
				}
			}

			public
			void
			remove( Action<Action> callback )
			{
				callbacks.Remove( callback );
			}

			public
			bool
			invoke()
			{
				if ( invoking )
				{
					return false;
				}

				invoking_callbacks.Clear();
				invoking_callbacks.Add( callbacks );

				waiting_count = invoking_callbacks.Count;
				for ( int i = 0; i < invoking_callbacks.Count; ++i )
				{
					invoking_callbacks[i].Invoke( done );
				}

				return true;
			}

			public
			bool
			contains( Action<Action> callback ) => callbacks.Contains( callback );

			public bool invoking { get; private set; }

			private List< Action<Action> > callbacks          = new List<Action<Action>>();
			private List< Action<Action> > invoking_callbacks = new List<Action<Action>>();
			private int waiting_count;

			private
			void
			done()
			{
				--waiting_count;
				if ( waiting_count == 0 )
				{
					invoking = false;
					onDone?.Invoke();
				}
			}
		}

		public static
		List<T>
		Add<T>( this List<T> list, IEnumerable<T> ie )
		{
			list.Reserve( ie.Count() );

			foreach ( T e in ie )
			{
				list.Add( e );
			}

			return list;
		}

		public static
		void
		Reserve<T>( this List<T> list, int reserve_count ) => list.Capacity = Math.Max( list.Capacity, list.Count + reserve_count );

		public
		static
		int
		mult( params short[] arr )
		{
			int ret = 1;
			for ( int i = 0; i != arr.Length; ++i )
			{
				ret *= arr[i];
			}
			return ret;
		}
		public
		static
		uint
		mult( params ushort[] arr )
		{
			uint ret = 1;
			for ( int i = 0; i != arr.Length; ++i )
			{
				ret *= arr[i];
			}
			return ret;
		}
		public
		static
		int
		mult( params int[] arr )
		{
			int ret = 1;
			for ( int i = 0; i != arr.Length; ++i )
			{
				ret *= arr[i];
			}
			return ret;
		}
		public
		static
		uint
		mult( params uint[] arr )
		{
			uint ret = 1;
			for ( int i = 0; i != arr.Length; ++i )
			{
				ret += arr[i];
			}
			return ret;
		}
		public
		static
		float
		mult( params float[] arr )
		{
			float ret = 1;
			for ( int i = 0; i != arr.Length; ++i )
			{
				ret *= arr[i];
			}
			return ret;
		}
		public
		static
		double
		mult( params double[] arr )
		{
			double ret = 1;
			for ( int i = 0; i != arr.Length; ++i )
			{
				ret *= arr[i];
			}
			return ret;
		}
		public
		static
		int
		sum( params short[] arr )
		{
			int ret = 0;
			for ( int i = 0; i != arr.Length; ++i )
			{
				ret += arr[i];
			}
			return ret;
		}
		public
		static
		uint
		sum( params ushort[] arr )
		{
			uint ret = 0;
			for ( int i = 0; i != arr.Length; ++i )
			{
				ret += arr[i];
			}
			return ret;
		}
		public
		static
		int
		sum( params int[] arr )
		{
			int ret = 0;
			for ( int i = 0; i != arr.Length; ++i )
			{
				ret += arr[i];
			}
			return ret;
		}
		public
		static
		uint
		sum( params uint[] arr )
		{
			uint ret = 0;
			for ( int i = 0; i != arr.Length; ++i )
			{
				ret += arr[i];
			}
			return ret;
		}
		public
		static
		float
		sum( params float[] arr )
		{
			float ret = 0;
			for ( int i = 0; i != arr.Length; ++i )
			{
				ret += arr[i];
			}
			return ret;
		}
		public
		static
		double
		sum( params double[] arr )
		{
			double ret = 0;
			for ( int i = 0; i != arr.Length; ++i )
			{
				ret += arr[i];
			}
			return ret;
		}
	}
}