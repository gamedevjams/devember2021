using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Mathematics;
using static Unity.Mathematics.math;
using Sirenix.OdinInspector;
using DG.Tweening;
using tg;
using static tg.tg;
using UnityEngine.InputSystem;

namespace devember2021
{
	public class HotkeyToggleEnableBehaviour : MonoBehaviour
	{
		public MonoBehaviour behaviour;
		public Key key;
		public bool useComboKey;
		[ShowIf("useComboKey")]
		public Key comboKey;

		private
		void
		Update()
		{
			if ( useComboKey
			    ?
			     ( Keyboard.current[key     ].wasPressedThisFrame && Keyboard.current[comboKey].isPressed )
			  || ( Keyboard.current[comboKey].wasPressedThisFrame && Keyboard.current[key     ].isPressed )
			    :
			     Keyboard.current[key].wasPressedThisFrame )
			{
				behaviour.enabled = ! behaviour.enabled;
			}
		}
	}
}
