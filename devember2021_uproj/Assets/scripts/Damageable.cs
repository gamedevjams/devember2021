using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Mathematics;
using static Unity.Mathematics.math;
using Sirenix.OdinInspector;
using DG.Tweening;
using tg;
using static tg.tg;

namespace devember2021
{
	[DisallowMultipleComponent]
	public class Damageable : MonoBehaviour
	{
		[SerializeField] private float HP;
		public float initialHP => HP;
		[ShowInInspector]
		public float currHP => curr_HP;
		public bool alive => currHP > 0;
		public Action<Damageable,Damage     > onDamaged;
		public Action<Damageable,Heal       > onHealed;
		public Action<Damageable,Resuscitate> onResuscitated;
		public Action<Damageable,Damage     > onDeath;

		[Serializable]
		public
		struct
		Resuscitate
		{
			[Min(1)]
			public float                   hp;
			[NonSerialized,ShowInInspector,ReadOnly,HideInEditorMode]
			public Game_State.Character_ID character_id;
		}
		[Serializable]
		public
		struct
		Heal
		{
			public float                   heal;
			[NonSerialized,ShowInInspector,ReadOnly,HideInEditorMode]
			public Game_State.Character_ID character_id;
		}
		[Serializable]
		public
		struct
		Damage
		{
			public float                   damage;
			[NonSerialized,ShowInInspector,ReadOnly,HideInEditorMode]
			public Game_State.Character_ID character_id;
		}

		public
		void
		damage( Damage damage )
		{
			if ( ! alive )
			{
				return;
			}
			if ( ! this.isActiveAndEnabled )
			{
				log_err( $"{name} is not active-and-enabled but damage has been called" );
				return;
			}
			curr_HP = Mathf.Max( 0, curr_HP - Mathf.Max( 0, damage.damage ) );
			onDamaged?.Invoke( this, damage );
			if ( ! alive )
			{
				onDeath?.Invoke( this, damage );
			}
		}

		public
		void
		heal( Heal heal )
		{
			if ( ! alive )
			{
				return;
			}
			if ( ! this.isActiveAndEnabled )
			{
				log_err( $"{name} is not active-and-enabled but heal has been called" );
				return;
			}
			curr_HP = Mathf.Min( HP, curr_HP + Mathf.Max( 0, heal.heal ) );
			onHealed?.Invoke( this, heal );
		}

		public
		void
		resuscitate( Resuscitate resuscitate )
		{
			if ( alive )
			{
				return;
			}
			curr_HP = Mathf.Clamp( resuscitate.hp, min:1, max: HP );
			onResuscitated?.Invoke( this, resuscitate );
		}

		public
		void
		reset()
		{
			curr_HP = HP;
		}

		#region Unity
		private
		void
		Awake()
		{
			reset();
		}
		#endregion Unity

		#region private
		private float curr_HP;

		#if UNITY_EDITOR
		[ShowInInspector, LabelText("Damage To Do"), HideInEditorMode]
		private float editor_damage_to_do = 0;
		[Button("Apply Damage"),HideInEditorMode]
		private
		void
		editor_do_damage()
		{
			damage( new Damage(){ character_id = new Game_State.Character_ID{ id = -1 }, damage = editor_damage_to_do } );
		}
		[ShowInInspector, LabelText("Healing To Do"), HideInEditorMode]
		private float editor_healing_to_do = 0;
		[Button("Apply Heal"),HideInEditorMode]
		private
		void
		editor_do_heal()
		{
			heal( new Heal(){ character_id = new Game_State.Character_ID{ id = -1 }, heal = editor_healing_to_do } );
		}
		[ShowInInspector, LabelText("Ress HP"), HideInEditorMode]
		private float editor_ress_hp = 0;
		[Button("Resuscitate"),HideInEditorMode]
		private
		void
		editor_do_ress()
		{
			resuscitate( new Resuscitate(){ character_id = new Game_State.Character_ID{ id = -1 }, hp = editor_ress_hp } );
		}
		#endif
		#endregion private
	}
}
