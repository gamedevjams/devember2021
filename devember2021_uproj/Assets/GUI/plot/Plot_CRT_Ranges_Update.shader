﻿Shader "tg/Pot CRT Ranges Update"
{
	Properties
	{
		//_Values01 ( "Values01", float[] ) = 0
		_Iteration ( "Iteration", Int ) = 0
		_ValuesCount ( "Values Count", Int ) = 0
		_BGColor ( "Background", Color ) = ( 0,0,0,0 )
		_FrontColor ( "Front", Color ) = ( 0,0,0,0 )
		_Interpolation( "Interpolation", Int ) = 0
	}
		SubShader
	{
		// No culling or depth
		//Cull Off ZWrite Off ZTest Always
		Lighting Off
		Blend One Zero

		Pass
		{
			CGPROGRAM
			// Upgrade NOTE: excluded shader from DX11, OpenGL ES 2.0 because it uses unsized arrays
			//#pragma exclude_renderers d3d11 gles

			#include "UnityCustomRenderTexture.cginc"
			#pragma vertex CustomRenderTextureVertexShader
			#pragma fragment frag
			#pragma target 3.0

			//#include "UnityCG.cginc"

			float4 _BGColor;
			float4 _FrontColor;
			uniform float _Values01[128];
			uniform float _RangesMax[128];
			uniform float4 _FGColors[128];
			int    _Iteration;
			int    _ValuesCount;
			int    _Interpolation;

			float4 frag( v2f_customrendertexture IN ) : COLOR
			{
				float4 old = tex2D( _SelfTexture2D, IN.globalTexcoord.xy );
				int px_x = (int)( IN.globalTexcoord.x * _CustomRenderTextureWidth );

				// NOTE(theGiallo): front bar
				{
					int rel_iter = ( _Iteration + _ValuesCount ) % _CustomRenderTextureWidth;
					if ( px_x == rel_iter )
					{
						return _FrontColor;
					}
				}

				// NOTE(theGiallo): render lines
				int px_y = (int)( IN.globalTexcoord.y * _CustomRenderTextureHeight );
				{
				for ( int i = 0; i != (int)_RangesMax.Length; ++i )
				{
					int r_max_px = (int)( _RangesMax[i] * _CustomRenderTextureHeight );
					if ( px_y == r_max_px )
					{
						return _FGColors[i];
					}
				}
				}

				int iter = -1;
				float value;
				{
				for ( int i = 0; i != (int)_Values01.Length && i != _ValuesCount; ++i )
				{
					value = _Values01[i];
					int rel_iter = ( _Iteration + i ) % _CustomRenderTextureWidth;
					if ( px_x == rel_iter && value >= 0 )
					{
						iter = i;
						break;
					}
				}
				}
				if ( iter == -1 )
				{
					return old;
				}

				float4 _FGColor = float4(1,0,1,1);
				{
				for ( int i = 0; i != (int)_RangesMax.Length; ++i )
				{
					int prev_i = i == 0 ? i : i - 1;
					if ( _RangesMax[prev_i] <= _RangesMax[i] )
					{
						_FGColor = _FGColors[i];
						if ( value < _RangesMax[i] )
						{
							if ( _Interpolation != 0 )
							{
								float4 prev_color = i == 0 ? _FGColor : _FGColors[i-1];
								float prev_max = i == 0 ? 0.0 : _RangesMax[i-1];
								float dv01 = ( value - prev_max ) / ( _RangesMax[i] - prev_max );
								if ( _Interpolation == 1 )
								{
									_FGColor = lerp( prev_color, _FGColor, dv01 );
								} else
								if ( _Interpolation == 2 )
								{
									_FGColor = lerp( prev_color, _FGColor, smoothstep( 0, 1, dv01 ) );
								}
							}
							break;
						}
					} else
					{
						break;
					}
				}
				}

				return IN.globalTexcoord.y > value ? _BGColor : _FGColor;
			}
			ENDCG
		}
	}
}