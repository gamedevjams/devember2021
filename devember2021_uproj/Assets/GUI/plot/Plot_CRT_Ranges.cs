﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Sirenix.OdinInspector;
using static tg.tg;

namespace tg
{
	[RequireComponent(typeof(RawImage))]
	public class Plot_CRT_Ranges : MonoBehaviour
	{
		public enum
		Interpolation_Mode
		{
			None,
			Lerp,
			Smoothstep
		}

		public const int MAX_RANGES_COUNT = 128;
		public Color[] fg_colors = new Color[MAX_RANGES_COUNT];
		public float[] ranges_max = new float[MAX_RANGES_COUNT];
		public Color bg_color;
		public Color front_color;
		[Range(2,8192)]
		public int width;
		[Range(2,8192)]
		public int height;
		public Interpolation_Mode interpolation_mode;
		public Shader shader;

		public
		bool
		plot01( float value01 )
		{
			bool ret = values_used_this_frame < MAX_VALUES_PER_FRAME;
			if ( ret )
			{
				values01[values_used_this_frame++] = Mathf.Clamp01( value01 );
			}
			return ret;
		}
		public const int MAX_VALUES_PER_FRAME = 128;

		#region private
		private int fg_colors_id;
		private int ranges_max_id;
		private int bg_color_id;
		private int front_color_id;
		private int values_id;
		private int iteration_id;
		private int values_count_id;
		private int interpolation_id;
		private int iteration;
		private float[] values01 = new float[MAX_VALUES_PER_FRAME];
		private int values_used_this_frame;
		private RawImage raw_image;
		private CustomRenderTexture crt;
		private Material material;
		#endregion

		#region Unity
		private void Awake()
		{
		}
		private void Start()
		{
			values_id = Shader.PropertyToID( "_Values01" );
			iteration_id = Shader.PropertyToID( "_Iteration" );
			values_count_id = Shader.PropertyToID( "_ValuesCount" );
			fg_colors_id = Shader.PropertyToID( "_FGColors" );
			ranges_max_id = Shader.PropertyToID( "_RangesMax" );
			bg_color_id = Shader.PropertyToID( "_BGColor" );
			front_color_id = Shader.PropertyToID( "_FrontColor" );
			interpolation_id = Shader.PropertyToID( "_Interpolation" );

			crt = new CustomRenderTexture( width, height, RenderTextureFormat.ARGB32 );
			material = new Material( shader );
			crt.material = material;
			material.SetColorArray( fg_colors_id, fg_colors );
			material.SetFloatArray( ranges_max_id, ranges_max );
			material.SetColor( front_color_id, front_color );
			material.SetColor( bg_color_id, bg_color );
			material.SetInt( interpolation_id, (int)interpolation_mode );

			raw_image = GetComponent<RawImage>();
			raw_image.texture = crt;

			crt.updateMode            = CustomRenderTextureUpdateMode.OnDemand;
			crt.initializationMode    = CustomRenderTextureUpdateMode.OnDemand;
			crt.initializationSource  = CustomRenderTextureInitializationSource.TextureAndColor;
			crt.initializationColor   = bg_color;
			crt.initializationTexture = null;
			crt.doubleBuffered        = true;
			crt.Initialize();
		}
		private void LateUpdate()
		{
			if ( values_used_this_frame > 0 )
			{
				iteration = iteration + values_used_this_frame;
				crt.material.SetInt( iteration_id, iteration );
				crt.material.SetInt( values_count_id, values_used_this_frame );
				crt.material.SetFloatArray( values_id, values01 );
				crt.Update();

				values_used_this_frame = 0;
				for ( int i = 0; i != values01.Length; ++i )
				{
					values01[i] = -1f;
				}
			}
		}
		#endregion
	}
}
