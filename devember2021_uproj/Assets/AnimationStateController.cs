using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace devember2021
{
    // // first version from the tutorial
    // public class AnimationStateController : MonoBehaviour
    // {
    //     Animator animator;
    //     int isWalkingHash, isRunningHash;
    //
    //     // Start is called before the first frame update
    //     void Start()
    //     {
    //         animator = GetComponent<Animator>();
    //         isWalkingHash = Animator.StringToHash("IsWalking");
    //         isRunningHash = Animator.StringToHash("IsRunning");
    //     }
    //
    //     // Update is called once per frame
    //     void Update()
    //     {
    //         bool isRunning = animator.GetBool(isRunningHash);
    //         bool isWalking = animator.GetBool(isWalkingHash);
    //         bool forwardPressed = Input.GetKey("w");
    //         bool runPressed = Input.GetKey("left shift");
    //
    //         if (!isWalking && forwardPressed)
    //         {
    //             animator.SetBool(isWalkingHash, true);
    //         }
    //
    //         if (isWalking && !forwardPressed)
    //         {
    //             animator.SetBool(isWalkingHash, false);
    //         }
    //
    //         if (!isRunning && forwardPressed && runPressed)
    //         {
    //             animator.SetBool(isRunningHash, true);
    //         }
    //
    //         if (isRunning && (!forwardPressed || !runPressed))
    //         {
    //             animator.SetBool(isRunningHash, false);
    //         }
    //
    //     }
    // }

    // first version from the tutorial
    public class AnimationStateController : MonoBehaviour
    {
        Animator animator;
        float velocity = 0.0f;
        public float acceleration = 0.1f;
        public float deceleration = 0.5f;
        int VelocityHash;

        // int isWalkingHash, isRunningHash;

        // Start is called before the first frame update
        void Start()
        {
            animator = GetComponent<Animator>();
            // isWalkingHash = Animator.StringToHash("IsWalking");
            // isRunningHash = Animator.StringToHash("IsRunning");
            VelocityHash = Animator.StringToHash("Velocity");
        }

        // Update is called once per frame
        void Update()
        {
            // bool isRunning = animator.GetBool(isRunningHash);
            // bool isWalking = animator.GetBool(isWalkingHash);
            bool forwardPressed = Input.GetKey("w");
            bool runPressed = Input.GetKey("left shift");

            if (forwardPressed)
            {
                if (velocity < 1.0f)
                {
                    velocity += Time.deltaTime * acceleration;
                }
            }
            else
            {
                if (velocity > 0.0f)
                {
                    velocity -= Time.deltaTime * deceleration;
                }
            }

            velocity = Math.Clamp(velocity, 0, 1);

            // if (!isWalking && forwardPressed)
            // {
            //     animator.SetBool(isWalkingHash, true);
            // }
            //
            // if (isWalking && !forwardPressed)
            // {
            //     animator.SetBool(isWalkingHash, false);
            // }
            //
            // if (!isRunning && forwardPressed && runPressed)
            // {
            //     animator.SetBool(isRunningHash, true);
            // }
            //
            // if (isRunning && (!forwardPressed || !runPressed))
            // {
            //     animator.SetBool(isRunningHash, false);
            // }

            animator.SetFloat(VelocityHash, velocity);
        }
    }
}

